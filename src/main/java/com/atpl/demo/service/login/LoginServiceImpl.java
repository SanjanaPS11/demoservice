package com.atpl.demo.service.login;

import com.atpl.demo.model.login.LoginModel;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;
import java.util.UUID;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.atpl.demo.constant.Constants;
import com.atpl.demo.dao.login.LoginDao;
import com.atpl.demo.dao.message.MessageDao;

import com.atpl.demo.domain.message.MessageDomain;

import com.atpl.demo.exception.MmgRestException.NOT_FOUND;
import com.atpl.demo.exception.MmgRestException.SAVE_FAILED;
import com.atpl.demo.mapper.login.LoginMapper;
import com.atpl.demo.mapper.message.MessageMapper;
import com.atpl.demo.model.message.MessageModel;
import com.atpl.demo.model.registration.RegistrationModel;
import com.atpl.demo.service.company.CompanyService;
import com.atpl.demo.service.message.MessageService;
import com.atpl.demo.service.message.MessageServiceImpl;
import com.atpl.demo.utils.DateUtility;
import com.atpl.demo.domain.message.RoleId;
@Service("loginService")
public class LoginServiceImpl implements LoginService, Constants{
	
	@Autowired
	LoginDao loginDao;

	@Autowired
	LoginMapper loginMapper;

private static final Logger logger = LoggerFactory.getLogger(LoginServiceImpl.class);

public LoginServiceImpl() {
	// constructor
}
	
	
	public Boolean login(LoginModel loginModel) throws Exception{
		String username= loginModel.getUserName();
		String password=loginDao.Login(username);
		if(password.equals(loginModel.getPassword()))
					return true;
		
		else
			return false;
		
	}
}
