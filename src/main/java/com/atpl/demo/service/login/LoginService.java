package com.atpl.demo.service.login;

import com.atpl.demo.model.login.LoginModel;
import com.atpl.demo.model.message.MessageModel;

public interface LoginService {
	
	public Boolean login(LoginModel loginModel) throws Exception;

}
