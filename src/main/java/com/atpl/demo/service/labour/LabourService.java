package com.atpl.demo.service.labour;
import java.util.List;

import org.springframework.web.bind.annotation.PathVariable;

import com.atpl.demo.exception.GenericRes;
import com.atpl.demo.model.labour.LabourModel;

public interface LabourService {
	public Integer getLabourCount(String requestedBy,String status,Integer stateId) throws Exception;
}
