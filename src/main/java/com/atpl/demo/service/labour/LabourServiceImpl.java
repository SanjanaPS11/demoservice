package com.atpl.demo.service.labour;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.atpl.demo.constant.Constants;
import com.atpl.demo.dao.driver.DriverDao;
import com.atpl.demo.dao.labour.LabourDao;
import com.atpl.demo.domain.labour.LabourDomain;
import com.atpl.demo.domain.labour.LabourDomain;
import com.atpl.demo.domain.labour.LabourDomain;
import com.atpl.demo.exception.MmgRestException.NOT_FOUND;
import com.atpl.demo.mapper.driver.DriverMapper;
import com.atpl.demo.mapper.labour.LabourMapper;
import com.atpl.demo.model.labour.LabourModel;
import com.atpl.demo.service.labour.LabourService;
import com.atpl.demo.service.labour.LabourServiceImpl;

@Service("labourService")
public class LabourServiceImpl implements LabourService, Constants{


	@Autowired
	LabourDao labourDao;

	@Autowired
	LabourMapper labourMapper;
	private static final Logger logger = LoggerFactory.getLogger(LabourServiceImpl.class);
	public LabourServiceImpl() {
		// constructor
	}
	@Override
	public Integer getLabourCount(String requestedBy,String status,Integer stateId) throws Exception {
//		DriverDomain driverDomain = new DriverDomain();
//		BeanUtils.copyProperties(driverModel, driverDomain);
		return labourDao.getLabourCount(requestedBy,status,stateId);
		
	}
}
