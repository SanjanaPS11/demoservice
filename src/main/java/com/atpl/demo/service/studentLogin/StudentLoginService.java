package com.atpl.demo.service.studentLogin;

import com.atpl.demo.model.studentLogin.StudentLoginModel;

public interface StudentLoginService {
    
	public String saveStudentLogin(StudentLoginModel studentLoginModel)throws Exception;
	
	public Boolean login(StudentLoginModel studentLoginModel) throws Exception;
	
	public String updateStudentLogin(StudentLoginModel studentLoginModel)throws Exception;
	

}
