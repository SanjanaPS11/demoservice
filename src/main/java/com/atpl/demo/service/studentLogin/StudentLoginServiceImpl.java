package com.atpl.demo.service.studentLogin;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.atpl.demo.constant.Constants;
import com.atpl.demo.dao.studentLogin.StudentLoginDao;
import com.atpl.demo.domain.student.StudentDomain;
import com.atpl.demo.domain.studentLogin.StudentLoginDomain;
import com.atpl.demo.mapper.studentLogin.StudentLoginMapper;
import com.atpl.demo.model.studentLogin.StudentLoginModel;

@Service("studentLoginService")
public class StudentLoginServiceImpl implements StudentLoginService, Constants{

	@Autowired
	StudentLoginDao studentLoginDao;

	@Autowired
	StudentLoginMapper studentLoginMapper;

	private static final Logger logger = LoggerFactory.getLogger(StudentLoginServiceImpl.class);

	public StudentLoginServiceImpl() {
		// constructor
	}

	@Override
	public String saveStudentLogin(StudentLoginModel studentLoginModel) throws Exception {
		// TODO Auto-generated method stub
         StudentLoginDomain studentLoginDomain = new StudentLoginDomain();
		
		//companyValidation.validateCompany(companyModel);
		BeanUtils.copyProperties(studentLoginModel, studentLoginDomain);
		return studentLoginDao.saveStudentLogin(studentLoginDomain);
	}

	@Override
	public Boolean login(StudentLoginModel studentLoginModel) throws Exception {
		// TODO Auto-generated method stub
		String userName= studentLoginModel.getUserName();
		String password=studentLoginDao.login(userName);
		if(password.equals(studentLoginModel.getPassWord()))
					return true;
		
		else
			return false;
		
	}

	@Override
	public String updateStudentLogin(StudentLoginModel studentLoginModel) throws Exception {
		// TODO Auto-generated method stub
		StudentLoginDomain studentLoginDomain = new StudentLoginDomain();
		BeanUtils.copyProperties(studentLoginModel, studentLoginDomain);
		return studentLoginDao.updateStudentLogin(studentLoginDomain);
	}
	
	

}

