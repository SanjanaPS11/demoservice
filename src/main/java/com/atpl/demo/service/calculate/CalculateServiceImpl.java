package com.atpl.demo.service.calculate;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.atpl.demo.constant.Constants;
import com.atpl.demo.dao.calculate.CalculateDao;
import com.atpl.demo.domain.calculate.CalculateDomain;
import com.atpl.demo.domain.employee.EmployeeDomain;
import com.atpl.demo.domain.registration.RegistrationDomain;
import com.atpl.demo.exception.MmgRestException.NOT_FOUND;
import com.atpl.demo.mapper.calculate.CalculateMapper;
import com.atpl.demo.model.calculate.CalculateModel;
import com.atpl.demo.model.employee.EmployeeModel;
import com.atpl.demo.service.calculate.CalculateService;
import com.atpl.demo.service.calculate.CalculateServiceImpl;

@Service("calculateService")
public class CalculateServiceImpl implements CalculateService, Constants{

	@Autowired
	CalculateDao calculateDao;

	@Autowired
	CalculateMapper calculateMapper;
	
	private static final Logger logger = LoggerFactory.getLogger(CalculateServiceImpl.class);

	public CalculateServiceImpl() {
		// constructor
	}
	
	public String saveList(CalculateModel calculateModel) throws Exception {
		CalculateDomain calculateDomain = new CalculateDomain();
		BeanUtils.copyProperties(calculateModel, calculateDomain);
		return calculateDao.saveList(calculateDomain);
	}
//	
//	@Override
//	public CalculateModel addTwoArrays(CalculateModel calculateModel) throws Exception; {
//		//List<CalculateDomain> calculateDomain = calculateDao.getList();
//		//return calculateMapper.entityList(calculateDomain);
//		//CalculateDomain calculateDomain = new CalculateDomain();
//		//BeanUtils.copyProperties(calculateModel, calculateDomain);
//		return NULL;
//	}
//	

	@Override
	public CalculateModel addTwoArrays(CalculateModel calculateModel) throws Exception {
		// TODO Auto-generated method stub
		return null;
	}
	
}
