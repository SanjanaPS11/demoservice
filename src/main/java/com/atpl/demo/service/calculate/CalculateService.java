package com.atpl.demo.service.calculate;

import com.atpl.demo.model.calculate.CalculateModel;
import com.atpl.demo.exception.GenericRes;
import java.util.List;


public interface CalculateService {
	
	public String saveList(CalculateModel calculateModel) throws Exception;
	
	public CalculateModel addTwoArrays(CalculateModel calculateModel) throws Exception;
}
