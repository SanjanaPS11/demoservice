package com.atpl.demo.service.student;

import java.util.List;

import com.atpl.demo.exception.GenericRes;
import com.atpl.demo.model.registration.RegistrationModel;
import com.atpl.demo.model.student.StudentModel;

public interface StudentService {

	public String saveStudent(StudentModel studentModel) throws Exception;
	
	public String updateStudent(StudentModel studentModel) throws Exception;
	
	public String deleteStudent(String uuId) throws Exception;
	
	public StudentModel getStudentById(String uuId) throws Exception;
	
	public List<StudentModel> getStudent() throws Exception;

}
