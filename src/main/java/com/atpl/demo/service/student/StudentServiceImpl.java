package com.atpl.demo.service.student;

import java.util.ArrayList;
import java.util.ArrayList.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.Collection;
import java.util.List;
import java.util.TimeZone;

import javax.activation.MimetypesFileTypeMap;
import javax.imageio.ImageIO;
import javax.sql.rowset.serial.SerialBlob;
import javax.swing.filechooser.FileNameExtensionFilter;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.PathVariable;

import org.springframework.validation.Validator;

import com.amazonaws.services.appstream.model.Image;
import com.amazonaws.util.IOUtils;
import com.atpl.demo.constant.Constants;
import com.atpl.demo.dao.company.CompanyDao;
import com.atpl.demo.dao.student.StudentDao;
import com.atpl.demo.domain.company.CompanyDomain;
import com.atpl.demo.domain.registration.RegistrationDomain;
import com.atpl.demo.domain.student.StudentDomain;
import com.atpl.demo.exception.MmgRestException.COMPANY_NAME_ALREADY_EXIST;
import com.atpl.demo.exception.MmgRestException.EMAILID_PATTERN_NOT_MATCH;
import com.atpl.demo.exception.MmgRestException.NOT_FOUND;
import com.atpl.demo.exception.MmgRestException.PAN_NUMBER_ALREADY_EXIST;
import com.atpl.demo.mapper.company.CompanyMapper;
import com.atpl.demo.mapper.student.StudentMapper;
import com.atpl.demo.model.company.CompanyModel;
import com.atpl.demo.model.registration.RegistrationModel;
import com.atpl.demo.model.student.StudentModel;
import com.atpl.demo.service.company.CompanyService;
import com.atpl.demo.service.company.CompanyServiceImpl;
import com.atpl.demo.utils.DateUtility;

import org.springframework.web.multipart.MultipartFile;
import org.apache.commons.compress.compressors.FileNameUtil;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.modelmapper.ModelMapper;

import java.io.File;
import java.io.IOException;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.io.*;
import java.util.UUID;
import com.atpl.demo.utils.DateUtility;
import java.sql.Blob;
import java.io.ByteArrayOutputStream;
import java.awt.image.BufferedImage;
import java.io.File;
import javax.imageio.ImageIO;
import com.atpl.demo.utils.CompanyValidationUtils;
import com.atpl.demo.utils.*;
import org.apache.poi.ss.usermodel.CellType;

@Service("studentService")
public class StudentServiceImpl implements StudentService, Constants{
	@Autowired
	StudentDao studentDao;

	@Autowired
	CompanyValidationUtils studentValidation;

	@Autowired
	StudentMapper studentMapper;

	private static final Logger logger = LoggerFactory.getLogger(StudentServiceImpl.class);

	public StudentServiceImpl() {
		// constructor
	}

	@Override
	public String saveStudent(StudentModel studentModel) throws Exception {
		// TODO Auto-generated method stub
		StudentDomain studentDomain = new StudentDomain();
		
		//companyValidation.validateCompany(companyModel);
		BeanUtils.copyProperties(studentModel, studentDomain);
		return studentDao.saveStudent(studentDomain);
		
	}

	@Override
	public String updateStudent(StudentModel studentModel) throws Exception {
		// TODO Auto-generated method stub
		StudentDomain studentDomain = new StudentDomain();
		BeanUtils.copyProperties(studentModel, studentDomain);
		return studentDao.updateStudent(studentDomain);
		
	}

	@Override
	public String deleteStudent(String uuId) throws Exception {
		// TODO Auto-generated method stub
		return studentDao.deleteStudent(uuId);
	}

	@Override
	public StudentModel getStudentById(String uuId) throws Exception {
		// TODO Auto-generated method stub
		StudentDomain studentDomain = studentDao.getStudentById(uuId);
		StudentModel studentModel = new StudentModel();
		if (studentDomain == null)
			throw new NOT_FOUND("student not found");
		BeanUtils.copyProperties(studentDomain, studentModel);
		return studentModel;
	
	}
	
	public List<StudentModel> getStudent() throws Exception{
		List<StudentDomain> studentDomain = studentDao.getStudent();
		return studentMapper.entityList(studentDomain);
	}
	
}
