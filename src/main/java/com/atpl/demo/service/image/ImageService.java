package com.atpl.demo.service.image;
import com.atpl.demo.model.image.ImageModel;
import java.util.List;
import org.springframework.web.multipart.MultipartFile;

public interface ImageService {

    public String uploadImage(MultipartFile image) throws Exception;
	
    public String downloadImage(String imagepath, String uuId) throws Exception;
    
	public List<ImageModel> getImage() throws Exception;
	
	public ImageModel downloadImage1()throws Exception;
	
}
