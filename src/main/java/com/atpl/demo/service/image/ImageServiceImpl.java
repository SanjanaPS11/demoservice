package com.atpl.demo.service.image;
import java.util.ArrayList;
import java.util.ArrayList.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.Collection;
import java.util.List;
import java.util.TimeZone;

import javax.activation.MimetypesFileTypeMap;
import javax.imageio.ImageIO;
import javax.sql.rowset.serial.SerialBlob;
import javax.swing.filechooser.FileNameExtensionFilter;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.PathVariable;

import org.springframework.validation.Validator;

import com.amazonaws.services.appstream.model.Image;
import com.amazonaws.util.IOUtils;
import com.atpl.demo.constant.Constants;
import com.atpl.demo.dao.company.CompanyDao;
import com.atpl.demo.dao.image.ImageDao;
import com.atpl.demo.domain.company.CompanyDomain;
import com.atpl.demo.domain.image.ImageDomain;
import com.atpl.demo.domain.registration.RegistrationDomain;
import com.atpl.demo.exception.MmgRestException.COMPANY_NAME_ALREADY_EXIST;
import com.atpl.demo.exception.MmgRestException.EMAILID_PATTERN_NOT_MATCH;
import com.atpl.demo.exception.MmgRestException.NOT_FOUND;
import com.atpl.demo.exception.MmgRestException.PAN_NUMBER_ALREADY_EXIST;
import com.atpl.demo.exception.MmgRestException.FILE_UPLOAD_FAILED;
import com.atpl.demo.mapper.company.CompanyMapper;
import com.atpl.demo.mapper.image.ImageMapper;
import com.atpl.demo.model.company.CompanyModel;
import com.atpl.demo.model.image.ImageModel;
import com.atpl.demo.model.registration.RegistrationModel;
import com.atpl.demo.service.company.CompanyService;
import com.atpl.demo.service.company.CompanyServiceImpl;
import com.atpl.demo.utils.DateUtility;

import org.springframework.web.multipart.MultipartFile;
import org.apache.commons.compress.compressors.FileNameUtil;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Iterator;
import java.io.*;
import java.util.UUID;
import com.atpl.demo.utils.DateUtility;
import java.sql.Blob;
import java.awt.image.BufferedImage;

import javax.imageio.ImageIO;
import com.atpl.demo.utils.CompanyValidationUtils;
import com.atpl.demo.utils.*;
@Service("imageService")
public class ImageServiceImpl implements ImageService, Constants{

	@Autowired
 	ImageDao imageDao;

	@Autowired
	ImageMapper imageMapper;
	
	private static final Logger logger = LoggerFactory.getLogger(CompanyServiceImpl.class);

	public ImageServiceImpl() {
		// constructor
	}
	
	@Override
	public String uploadImage(MultipartFile image) throws Exception {
		// TODO Auto-generated method stub
		String IMAGE_MIME_TYPE = "image";
		String filename=image.getOriginalFilename();
		
		System.out.println(filename);
        String match=image.getContentType();
        System.out.println("jjknjk"+match);
        String extension=match.split("\\/")[0];
        System.out.println(extension);
		if (!IMAGE_MIME_TYPE.equals(extension)) {
			throw new FILE_UPLOAD_FAILED("It is not imageFile, Choose image file");
		}
		
		
		String imageType= image.getOriginalFilename().split("\\.")[1];
		System.out.println(imageType);
		
		String imageName= image.getOriginalFilename().split("\\.")[0];
		System.out.println(imageName);
		
		byte[] data= image.getBytes();
		ImageDomain imageDomain=new ImageDomain();
		imageDomain.setUuId(UUID.randomUUID().toString());
		imageDomain.setImageName(imageName);
		imageDomain.setImageType(imageType);
		imageDomain.setImage(data);
		Blob blob;
		System.out.println(data);
		blob =new SerialBlob(data);
		System.out.println(blob);
			return imageDao.uploadImage(imageDomain);
	}

	

	@Override
	public String downloadImage(String imagepath, String uuId) throws Exception {
		// TODO Auto-generated method stub String uuId
		ImageModel imageModel=new ImageModel();
		ImageDomain imageDomain=new ImageDomain();
		imageDomain = imageDao.downloadImage(uuId);
		
		//Blob blob= imageDomain.getImage();
		Blob blob =new SerialBlob(imageDomain.getImage());
		int blobLength = (int) blob.length();  
		byte[] blobAsBytes = blob.getBytes(1, blobLength);
		ByteArrayInputStream bis = new ByteArrayInputStream(blobAsBytes);
		 BufferedImage bImage2 = ImageIO.read(bis);
		 String name=imageDomain.getImageName();
		 String type= imageDomain.getImageType();
		 System.out.println(imagepath+name+type);
	      ImageIO.write(bImage2,type,new File(imagepath+name+'.'+type));
		return "Image Downloaded";
	}
	
	public List<ImageModel> getImage() throws Exception{
		//ImageDomain imageDomain=new ImageDomain();
		List<ImageDomain> imageDomain = new ArrayList<ImageDomain>();
		imageDomain = imageDao.getImage();
		return imageMapper.entityList(imageDomain);
	}
	
	public ImageModel downloadImage1()throws Exception{
		ImageModel imageModel=new ImageModel();
		ImageDomain imageDomain=new ImageDomain();
		imageDomain = imageDao.downloadImage1();
		
		//Blob blob= imageDomain.getImage();
		Blob blob =new SerialBlob(imageDomain.getImage());
		int blobLength = (int) blob.length();  
		byte[] blobAsBytes = blob.getBytes(1, blobLength);
		ByteArrayInputStream bis = new ByteArrayInputStream(blobAsBytes);
		 BufferedImage bImage2 = ImageIO.read(bis);
		 String name=imageDomain.getImageName();
		 String type= imageDomain.getImageType();
		 System.out.println(name+type);
		 BeanUtils.copyProperties(imageDomain, imageModel);
		return null;
	}
}
//public string Get(string userId)
//{
//    var image = _profileImageService.GetProfileImage(userId);
//
//    return System.Convert.ToBase64String(image);
//}



