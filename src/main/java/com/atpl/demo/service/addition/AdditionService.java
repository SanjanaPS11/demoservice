package com.atpl.demo.service.addition;

import java.util.List;

import com.atpl.demo.exception.GenericRes;
import com.atpl.demo.model.addition.AdditionModel;
public interface AdditionService {

	public String saveAddition(AdditionModel additionModel) throws Exception;
	
	public AdditionModel getAddition(int id) throws Exception;
}
