package com.atpl.demo.service.addition;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.atpl.demo.constant.Constants;
import com.atpl.demo.dao.addition.AdditionDao;
import com.atpl.demo.domain.addition.AdditionDomain;
import com.atpl.demo.exception.MmgRestException.NOT_FOUND;
import com.atpl.demo.mapper.addition.AdditionMapper;
import com.atpl.demo.model.addition.AdditionModel;
import com.atpl.demo.service.addition.AdditionService;
import com.atpl.demo.service.addition.AdditionServiceImpl;

@Service("additionService")
public class AdditionServiceImpl  implements AdditionService, Constants{

	@Autowired
	AdditionDao additionDao;

	@Autowired
	AdditionMapper additionMapper;
	
	private static final Logger logger = LoggerFactory.getLogger(AdditionServiceImpl.class);

	public AdditionServiceImpl() {
		// constructor
	}
	
	
	public String saveAddition(AdditionModel additionModel) throws Exception {
		AdditionDomain additionDomain = new AdditionDomain();
		BeanUtils.copyProperties(additionModel, additionDomain);
		return additionDao.saveAddition(additionDomain);
	}
	
	@Override
	public AdditionModel getAddition(int id) throws Exception {
		AdditionDomain additionDomain = additionDao.getAddition(id);
		AdditionModel employeeModel = new AdditionModel();
		if (additionDomain == null)
			throw new NOT_FOUND("Bank not found");
		BeanUtils.copyProperties(additionDomain, employeeModel);
		return employeeModel;
	}
}
