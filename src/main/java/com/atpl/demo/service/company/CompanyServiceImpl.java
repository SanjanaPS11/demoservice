package com.atpl.demo.service.company;

import java.util.ArrayList;
import java.util.ArrayList.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.Collection;
import java.util.List;
import java.util.TimeZone;

import javax.activation.MimetypesFileTypeMap;
import javax.imageio.ImageIO;
import javax.sql.rowset.serial.SerialBlob;
import javax.swing.filechooser.FileNameExtensionFilter;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.PathVariable;

import org.springframework.validation.Validator;

import com.amazonaws.services.appstream.model.Image;
import com.amazonaws.util.IOUtils;
import com.atpl.demo.constant.Constants;
import com.atpl.demo.dao.company.CompanyDao;
import com.atpl.demo.domain.company.CompanyDomain;
import com.atpl.demo.domain.registration.RegistrationDomain;
import com.atpl.demo.exception.MmgRestException.COMPANY_NAME_ALREADY_EXIST;
import com.atpl.demo.exception.MmgRestException.EMAILID_PATTERN_NOT_MATCH;
import com.atpl.demo.exception.MmgRestException.NOT_FOUND;
import com.atpl.demo.exception.MmgRestException.PAN_NUMBER_ALREADY_EXIST;
import com.atpl.demo.mapper.company.CompanyMapper;
import com.atpl.demo.model.company.CompanyModel;
import com.atpl.demo.model.registration.RegistrationModel;
import com.atpl.demo.utils.DateUtility;

import org.springframework.web.multipart.MultipartFile;
import org.apache.commons.compress.compressors.FileNameUtil;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.modelmapper.ModelMapper;

import java.io.File;
import java.io.IOException;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.io.*;
import java.util.UUID;
import com.atpl.demo.utils.DateUtility;
import java.sql.Blob;
import java.io.ByteArrayOutputStream;
import java.awt.image.BufferedImage;
import java.io.File;
import javax.imageio.ImageIO;
import com.atpl.demo.utils.CompanyValidationUtils;
import com.atpl.demo.utils.*;
import org.apache.poi.ss.usermodel.CellType;

@Service("companyService")
public class CompanyServiceImpl implements CompanyService, Constants {

	@Autowired
	CompanyDao companyDao;

	@Autowired
	CompanyValidationUtils companyValidation;

	@Autowired
	CompanyMapper companyMapper;

	private static final Logger logger = LoggerFactory.getLogger(CompanyServiceImpl.class);

	public CompanyServiceImpl() {
		// constructor
	}

	public String saveCompany(CompanyModel companyModel) throws Exception {
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat(DateUtility.DATE_FORMAT_YYYY_MM_DD_HHMMSS);
		simpleDateFormat.setTimeZone(TimeZone.getTimeZone("GMT+5:30"));
		// TODO Auto-generated method stub
		CompanyDomain companyDomain = new CompanyDomain();
		companyModel.setCreatedDate(simpleDateFormat.format(new Date()));
		companyModel.setModifiedDate(simpleDateFormat.format(new Date()));
		companyModel.setUuId(UUID.randomUUID().toString());
		companyValidation.validateCompany(companyModel);
		BeanUtils.copyProperties(companyModel, companyDomain);
		return companyDao.saveCompany(companyDomain);
	}

	@Override
	public CompanyModel getCompany(String id) throws Exception {
		CompanyDomain companyDomain = companyDao.getCompany(id);
		CompanyModel companyModel = new CompanyModel();
		if (companyDomain == null)
			throw new NOT_FOUND("company not found");
		BeanUtils.copyProperties(companyDomain, companyModel);
		return companyModel;

	}

	@Override
	public List<CompanyModel> getCompanyStatus(Boolean status) throws Exception {
		List<CompanyDomain> companyDomain = companyDao.getCompanyStatus(status);
		return companyMapper.entityList(companyDomain);
	}

	@Override
	public String patchCompany(CompanyModel id) throws Exception {
		// TODO Auto-generated method stub
		CompanyDomain companyDomain = new CompanyDomain();
		return companyDao.patchCompany(companyDomain);
	}

	public String patchCompanyStatus(Boolean status, String uuId) throws Exception {
		System.out.println(status + uuId);
		return companyDao.patchCompanyStatus(status, uuId);
	}

	@Override
	public String updateCompany(CompanyModel companyModel, String uuId) throws Exception {
		// TODO Auto-generated method stub
		CompanyDomain companyDomain = new CompanyDomain();
		BeanUtils.copyProperties(companyModel, companyDomain);
		return companyDao.updateCompany(companyDomain, uuId);

	}

	public List<CompanyModel> search(String value) throws Exception {
		List<CompanyDomain> companyDomain = companyDao.search(value);
		return companyMapper.entityList(companyDomain);
	}

	public String uploadExcel1(MultipartFile file) throws Exception {

		List<CompanyDomain> company = new ArrayList<CompanyDomain>();
		String EXCEL_MIME_TYPE = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
		System.out.println(EXCEL_MIME_TYPE);

		if (!EXCEL_MIME_TYPE.equals(file.getContentType())) {
			throw new NOT_FOUND(" file type is not excel ");
		}
		InputStream in = file.getInputStream();
		XSSFWorkbook wb = new XSSFWorkbook(in);

		Sheet sheet = wb.getSheetAt(0);
		System.out.println(sheet);
		Iterator<Row> rowIterator;
		rowIterator = sheet.iterator();

		List<String> header = new ArrayList<String>();
		Sheet sheet1 = wb.getSheetAt(0);
		System.out.println(sheet);
		Iterator<Row> rowIterator1;
		rowIterator = sheet.iterator();
		Row row = rowIterator.next();
		row = sheet.getRow(0);
		Iterator<Cell> cellIterator = row.cellIterator();
		HashMap<Integer, String> com = new HashMap<Integer, String>();
		HashMap<String, String> com1 = new HashMap<String, String>();
		com1.put("1", "getUuId(var)");
		String v=com1.get("1");
		for (int j = 0; j < row.getLastCellNum(); j++) {

			Row row1 = sheet.getRow(0);
			com.put(j, row1.getCell(j).getStringCellValue());
			header.add(row1.getCell(j).getStringCellValue());
		}
		for (String i : com.values()) {
			System.out.println(i);
		}

		System.out.println(header);

		while (rowIterator.hasNext()) {

			CompanyDomain companyDomain = new CompanyDomain();
			Row row1 = rowIterator.next();
			Iterator<Cell> cellIterator1 = row.cellIterator();
			while (cellIterator.hasNext()) {
				Cell cell = cellIterator.next();

				int columnIndex = cell.getColumnIndex();
				switch (columnIndex + 1) {
				case 1:
					companyDomain.setUuId(cell.getStringCellValue());

					break;
				case 2:

					companyDomain.setCompanyName(cell.getStringCellValue());
					break;
				case 3:

					companyDomain.setCompanyType(cell.getStringCellValue());
					break;
				case 4:

					// companyDomain.setEstablished(Integer.parseInt(cell.getStringCellValue()));
					companyDomain.setEstablished((int) cell.getNumericCellValue());
					break;
				case 5:

					companyDomain.setMdName(cell.getStringCellValue());
					break;

				case 6:

					companyDomain.setMobileNumber(cell.getStringCellValue());
					// companyModel.setMobileNumber((String.valueOf((int)cell.getNumericCellValue())));
					break;

				case 7:

					companyDomain.setEmailId(cell.getStringCellValue());
					break;
				case 8:

					companyDomain.setPanNumber(cell.getStringCellValue());
					break;
				case 9:

					companyDomain.setGstNumber(cell.getStringCellValue());
				
					break;
				case 10:
					companyDomain.setWebSite(cell.getStringCellValue());

					break;
				case 11:

					companyDomain.setIsActive(cell.getBooleanCellValue());
					
					break;
				case 12:

					companyDomain.setCreatedDate(cell.getStringCellValue());
					break;
				case 13:

					companyDomain.setModifiedDate(cell.getStringCellValue());
					break;
				}

			}
			CompanyModel companyModel = new CompanyModel();
			BeanUtils.copyProperties(companyDomain, companyModel);
			companyValidation.validateCompany(companyModel);
			company.add(companyDomain);
			System.out.println("hi");
			in.close();
		}

		for (CompanyDomain compan : company) {
			System.out.println(company);

			System.out.println(compan.getUuId() + compan.getCompanyName());
		}
		return companyDao.AddCompanyList(company);
	}

	public String uploadExcel(MultipartFile file) throws Exception {

		List<CompanyDomain> company = new ArrayList<CompanyDomain>();
		String EXCEL_MIME_TYPE = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
		System.out.println(EXCEL_MIME_TYPE);

		if (!EXCEL_MIME_TYPE.equals(file.getContentType())) {
			throw new NOT_FOUND(" file type is not excel ");
		}
		InputStream in = file.getInputStream();
		XSSFWorkbook wb = new XSSFWorkbook(in);

		List<String> header = new ArrayList<String>();
		Sheet sheet = wb.getSheetAt(0);
		System.out.println(sheet);
		Iterator<Row> rowIterator;
		rowIterator = sheet.iterator();
		Row row = rowIterator.next();
		row = sheet.getRow(0);

		// Iterator<Cell> cellIterator = row.cellIterator();
		HashMap<Integer, String> com = new HashMap<Integer, String>();
		for (int j = 0; j < row.getLastCellNum(); j++) {

			Row row1 = sheet.getRow(0);
			com.put(j, row1.getCell(j).getStringCellValue());
			header.add(row1.getCell(j).getStringCellValue());
		}
		for (String i : com.values()) {
			System.out.println(i);
		}

		System.out.println(header);

		for (int rowIndex = 1; rowIndex < sheet.getLastRowNum(); rowIndex++)

		{
			CompanyDomain companyDomain = new CompanyDomain();

			for (int cellIndex = 0; cellIndex < row.getPhysicalNumberOfCells(); cellIndex++) {

				Cell value = wb.getSheetAt(0).getRow(rowIndex).getCell(cellIndex);

				String i = com.get(cellIndex);
				System.out.println(i);
				switch (value.getCellType()) {

				case STRING:
					if (i.equalsIgnoreCase("uuId")) {
						
						companyDomain.setUuId(value.toString());
					} else if (i.equalsIgnoreCase("companyName")) {
						companyDomain.setCompanyName(value.toString());
					} else if (i.equalsIgnoreCase("companyType")) {
						companyDomain.setCompanyType(value.toString());
					} else if (i.equalsIgnoreCase("mdName")) {
						companyDomain.setMdName(value.toString());
					} else if (i.equalsIgnoreCase("mobileNumber")) {
						companyDomain.setMobileNumber(value.toString());
					} else if (i.equalsIgnoreCase("emailId")) {

						companyDomain.setEmailId(value.toString());
					} else if (i.equalsIgnoreCase("panNumber")) {
						companyDomain.setPanNumber(value.toString());
					} else if (i.equalsIgnoreCase("gstNumber")) {
						companyDomain.setGstNumber(value.toString());
					} else if (i.equalsIgnoreCase("webSite")) {
						companyDomain.setWebSite(value.toString());
					} else if (i.equalsIgnoreCase("createdDate")) {
						companyDomain.setCreatedDate(value.toString());
					} else if (i.equalsIgnoreCase("modifiedDate")) {
						companyDomain.setModifiedDate(value.toString());
					}

					break;

				case BOOLEAN:
					if (i.equalsIgnoreCase("isActive")) {
						companyDomain.setIsActive(Boolean.parseBoolean(value.toString()));
					}

					break;
				case NUMERIC:
					if (i.equalsIgnoreCase("established")) {
						companyDomain.setEstablished((int) value.getNumericCellValue());
						break;
					}
				case _NONE: {
					break;
				}
				case FORMULA: {
					break;
				}
				case ERROR: {
					break;
				}
				case BLANK: {
					break;
				}
				}
			}

			CompanyModel companyModel = new CompanyModel();
			BeanUtils.copyProperties(companyDomain, companyModel);
			companyValidation.validateCompany(companyModel);
			company.add(companyDomain);
			System.out.println("hi");
			in.close();
		}
		wb.close();

		return companyDao.AddCompanyList(company);
	}

	/* correct program */
//	public String uploadExcel(MultipartFile file) throws Exception {
//
//		List<CompanyDomain> company = new ArrayList<CompanyDomain>();
//		String EXCEL_MIME_TYPE = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
//		System.out.println(EXCEL_MIME_TYPE);
//
//		if (!EXCEL_MIME_TYPE.equals(file.getContentType())) {
//			throw new NOT_FOUND(" file type is not excel ");
//		}
//		InputStream in = file.getInputStream();
//		XSSFWorkbook wb = new XSSFWorkbook(in);
//
//		List<String> header = new ArrayList<String>();
//		Sheet sheet = wb.getSheetAt(0);
//		System.out.println(sheet);
//		Iterator<Row> rowIterator;
//		rowIterator = sheet.iterator();
//		Row row = rowIterator.next();
//		row = sheet.getRow(0);
//
//		Iterator<Cell> cellIterator = row.cellIterator();
//
//		for (int j = 0; j < row.getLastCellNum(); j++) {
//
//			Row row1 = sheet.getRow(0);
//
//			header.add(row1.getCell(j).getStringCellValue());
//		}
//
//		System.out.println(header);
//
//		for (int rowIndex = 1; rowIndex < sheet.getLastRowNum(); rowIndex++) {
//			CompanyDomain companyDomain = new CompanyDomain();
//			for (int cellIndex = 0; cellIndex < row.getPhysicalNumberOfCells(); cellIndex++) {
//				// Cell cell = cellIterator.next();
//				System.out.println(header.get(cellIndex));
//				// int columnIndex = cell.getColumnIndex();
//				switch (header.get(cellIndex)) {
//
//				case "UuId":
//					Cell id = wb.getSheetAt(0).getRow(rowIndex).getCell(cellIndex);
//					System.out.println(id);
//					companyDomain.setUuId(id.toString());
//
//					break;
//
//				case "companyName":
//					Cell cn = wb.getSheetAt(0).getRow(rowIndex).getCell(cellIndex);
////					System.out.println(wb.getSheetAt(0).getRow(4).getCell(7));
//					companyDomain.setCompanyName(cn.toString());
//					System.out.println(cn);
//					break;
//				case "companyType":
//					Cell ct = wb.getSheetAt(0).getRow(rowIndex).getCell(cellIndex);
//					System.out.println(ct);
//					companyDomain.setCompanyType(ct.toString());
//					break;
//				case "established":
//					Cell e = wb.getSheetAt(0).getRow(rowIndex).getCell(cellIndex);
//					System.out.println(e);
//
//					// companyDomain.setEstablished(Integer.parseInt(cell.getStringCellValue()));
//					companyDomain.setEstablished((int) e.getNumericCellValue());
//					break;
//				case "mdName":
//					Cell md = wb.getSheetAt(0).getRow(rowIndex).getCell(cellIndex);
//					companyDomain.setMdName(md.toString());
//					System.out.println(md);
//					break;
//
//				case "mobileNumber":
//					Cell mn = wb.getSheetAt(0).getRow(rowIndex).getCell(cellIndex);
//					companyDomain.setMobileNumber(mn.toString());
//					System.out.println(mn);
//					// companyModel.setMobileNumber((String.valueOf((int)cell.getNumericCellValue())));
//					break;
//
//				case "emailId":
//					Cell ei = wb.getSheetAt(0).getRow(rowIndex).getCell(cellIndex);
//					companyDomain.setEmailId(ei.toString());
//					System.out.println(ei);
//					break;
//				case "panNumber":
//					Cell pn = wb.getSheetAt(0).getRow(rowIndex).getCell(cellIndex);
//					companyDomain.setPanNumber(pn.toString());
//					System.out.println(pn);
//					break;
//				case "gstNumber":
//					Cell gst = wb.getSheetAt(0).getRow(rowIndex).getCell(cellIndex);
//					companyDomain.setGstNumber(gst.toString());
//					System.out.println(gst);
//					// System.out.println(companyModel.getGstNumber());
//					break;
//				case "webSite":
//					Cell ws = wb.getSheetAt(0).getRow(rowIndex).getCell(cellIndex);
//					companyDomain.setWebSite(ws.toString());
//					System.out.println(ws);
//					System.out.println("Web");
//					break;
//				case "isActive":
//					Cell ia = wb.getSheetAt(0).getRow(rowIndex).getCell(cellIndex);
//					// System.out.println("Act"+ cell.getCellType());
//					System.out.println(ia);
//					companyDomain.setIsActive(Boolean.parseBoolean(ia.toString()));
//					// companyDomain.setIsActive(Boolean.parseBoolean(cell.getStringCellValue()));
//					break;
//				case "CreatedDate":
//					Cell cd = wb.getSheetAt(0).getRow(rowIndex).getCell(cellIndex);
//					companyDomain.setCreatedDate(cd.toString());
//					break;
//				case "modifiedDate":
//					Cell mda = wb.getSheetAt(0).getRow(rowIndex).getCell(cellIndex);
//					companyDomain.setModifiedDate(mda.toString());
//					break;
//				}
//
//			}
//			CompanyModel companyModel = new CompanyModel();
//			BeanUtils.copyProperties(companyDomain, companyModel);
//		    companyValidation.validateCompany(companyModel);
//			company.add(companyDomain);
//			System.out.println("hi");
//			in.close();
//		}
//
//		return companyDao.AddCompanyList(company);
//	}

	/* correct program */
//	public  String uploadExcel(MultipartFile file) throws Exception {
//		
//		
//		List<CompanyDomain> company = new ArrayList<CompanyDomain>();
//		String EXCEL_MIME_TYPE = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
//		System.out.println(EXCEL_MIME_TYPE);
//
//		if (!EXCEL_MIME_TYPE.equals(file.getContentType())) {
//			throw new NOT_FOUND(" file type is not excel ");
//		}
//		InputStream in = file.getInputStream();
//		XSSFWorkbook wb = new XSSFWorkbook(in);
//
//		
//		Sheet sheet = wb.getSheetAt(0);
//		System.out.println(sheet);
//		Iterator<Row> rowIterator;
//		rowIterator = sheet.iterator();
//		
//		while (rowIterator.hasNext()) {
//			 System.out.println("fgfh"+ rowIterator.hasNext()+1);
//		CompanyDomain companyDomain = new CompanyDomain();
//			Row row = rowIterator.next();
//			Iterator<Cell> cellIterator = row.cellIterator();
//			while (cellIterator.hasNext()) {
//				Cell cell = cellIterator.next();
//
//				int columnIndex = cell.getColumnIndex();
//				switch (columnIndex + 1) {
//				case 1:
//					companyDomain.setUuId(cell.getStringCellValue());
//					System.out.println(cell.getStringCellValue());
//					break;
//				case 2:
//					System.out.println("CN");
//					companyDomain.setCompanyName(cell.getStringCellValue());
//					break;
//				case 3:
//					System.out.println("CT");
//					companyDomain.setCompanyType(cell.getStringCellValue());
//					break;
//				case 4:
//					System.out.println("E");
//					System.out.println("Est"+ cell.getCellType());
//					//companyDomain.setEstablished(Integer.parseInt(cell.getStringCellValue()));
//					companyDomain.setEstablished((int) cell.getNumericCellValue());
//					break;
//				case 5:
//					System.out.println("Md");
//					companyDomain.setMdName(cell.getStringCellValue());
//					break;
//
//				case 6:
//					System.out.println("Mobile");
//					companyDomain.setMobileNumber(cell.getStringCellValue());
//					// companyModel.setMobileNumber((String.valueOf((int)cell.getNumericCellValue())));
//					break;
//
//				case 7:
//					System.out.println("Email");
//					companyDomain.setEmailId(cell.getStringCellValue());
//					break;
//				case 8:
//					System.out.println("PAN");
//					companyDomain.setPanNumber(cell.getStringCellValue());
//					break;
//				case 9:
//					System.out.println("GST");
//					companyDomain.setGstNumber(cell.getStringCellValue());
//					// System.out.println(companyModel.getGstNumber());
//					break;
//				case 10:
//					companyDomain.setWebSite(cell.getStringCellValue());
//					System.out.println("Web");
//					break;
//				case 11:
//					System.out.println("Active");
//					System.out.println("Act"+ cell.getCellType());
//					companyDomain.setIsActive(cell.getBooleanCellValue());
//					//companyDomain.setIsActive(Boolean.parseBoolean(cell.getStringCellValue()));
//					break;
//				case 12:
//					System.out.println("CD");
//					companyDomain.setCreatedDate(cell.getStringCellValue());
//					break;
//				case 13:
//					System.out.println("MD");
//					companyDomain.setModifiedDate(cell.getStringCellValue());
//					break;
//				}
//				
//			}
//			CompanyModel companyModel=new CompanyModel(); 
//			BeanUtils.copyProperties(companyDomain,companyModel);
//			companyValidation.validateCompany(companyModel);
//			company.add(companyDomain);
//			System.out.println("hi");	
//			in.close();
//		}
//	
//		for (CompanyDomain compan : company) { 
//			System.out.println(company);
//
//			System.out.println(compan.getUuId()+ compan.getCompanyName()
//							);
//       		}
//		return companyDao.AddCompanyList(company);
//	}

	public List<CompanyModel> companySearch(String key, String value) throws Exception {
		String name = null;
		System.out.println(key + "" + value);

		String[] Key = { "companyName", "companyType", "established", "mdName", "mobileNumber", "emailId", "panNumber",
				"gstNumber", "website", "isActive" };
		name = searchIgnoreCase(key, Key);
		List<CompanyDomain> companyDomain = companyDao.companySearch(name, value);
		return companyMapper.entityList(companyDomain);
	}

	public String searchIgnoreCase(String key, String[] value) {
		String name = "0";
		for (int i = 0; i < value.length; i++) {
			if (key.equalsIgnoreCase(value[i])) {
				name = value[i];
			}
		}
		return name;
	}

	public List<CompanyModel> companyPagination(int PageSize, int PageNo) throws Exception {
		CompanyDomain companyDomain = new CompanyDomain();
		List<CompanyDomain> pagination = companyDao.companyPagination(PageSize, PageNo);

		return companyMapper.entityList(pagination);
	}

	@Override
	public String uploadImage(MultipartFile image) throws Exception {
		// TODO Auto-generated method stub
		String imageType = image.getContentType();

		System.out.println(imageType);

		System.out.println(image.getContentType());

		byte[] data = image.getBytes();
		Blob blob;
		System.out.println(data);
		blob = new SerialBlob(data);
		System.out.println(blob);
		return companyDao.uploadImage(data);
	}

	@Override
	public String downloadImage(String imagepath, String uuId) throws Exception {
		// TODO Auto-generated method stub String uuId
		Blob blob = companyDao.downloadImage(uuId);
		int blobLength = (int) blob.length();
		byte[] blobAsBytes = blob.getBytes(1, blobLength);
		// blob.free();
		// Image newImage = byteArrayToImage(blobAsBytes);
		ByteArrayInputStream bis = new ByteArrayInputStream(blobAsBytes);
		BufferedImage bImage2 = ImageIO.read(bis);
		ImageIO.write(bImage2, "jpg", new File(imagepath));

		return null;
	}

	public String downloadExcel(String path) throws Exception {
		// String pa=path;
		// System.out.println(pa);
		// public static String TYPE =
		// "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
		System.out.println("hi");
		System.out.println(path);

		String[] HEADERs = { "uuId", "companyName", "companyType", "established", "mdName", "mobileNumber", "emailId",
				"panNumber", "gstNumber", "WebSite", "isActive", "createdDate", "modifiedDate" };
		// static String SHEET = "Tutorials";
		// String sheet = "Tutorials";
		List<CompanyDomain> companyDomain = companyDao.getCompany();
		int len = companyDomain.size();
		System.out.println(len);
		companyMapper.entityList(companyDomain);

		Workbook workbook = new XSSFWorkbook();
		Sheet sheet = workbook.createSheet("sheet");
		Row row = sheet.createRow(0);
		Cell cell;

		for (int i = 0; i <= HEADERs.length - 1; i++) {
			cell = row.createCell(i);
			cell.setCellValue(HEADERs[i]);
		}
		int i = 1;

		for (CompanyDomain cd : companyDomain) {

			row = sheet.createRow(i);
			cell = row.createCell(0);
			cell.setCellValue(cd.getUuId());
			System.out.print(cd.getUuId());
			cell = row.createCell(1);
			cell.setCellValue(cd.getCompanyName());
			cell = row.createCell(2);
			cell.setCellValue(cd.getCompanyType());
			cell = row.createCell(3);
			cell.setCellValue(cd.getEstablished());
			cell = row.createCell(4);
			cell.setCellValue(cd.getMdName());
			cell = row.createCell(5);
			cell.setCellValue(cd.getMobileNumber());
			cell = row.createCell(6);
			cell.setCellValue(cd.getEmailId());
			cell = row.createCell(7);
			cell.setCellValue(cd.getPanNumber());
			cell = row.createCell(8);
			cell.setCellValue(cd.getGstNumber());
			cell = row.createCell(9);
			cell.setCellValue(cd.getWebSite());
			cell = row.createCell(10);
			cell.setCellValue(cd.getIsActive());
			cell = row.createCell(11);
			cell.setCellValue(cd.getCreatedDate());
			cell = row.createCell(12);
			cell.setCellValue(cd.getModifiedDate());
			// System.out.println(row);
			i++;
		}
//		FileOutputStream out = new FileOutputStream(new File("C:\\Angular\\databas.xlsx"));
		FileOutputStream out = new FileOutputStream(new File(path));
		// System.out.println(out);
		workbook.write(out);
		out.close();
		System.out.println("excel.xlsx written successfully");

		return "excel.xlsx written successfully";
	}

	@Override
	public List<CompanyModel> getCompany() throws Exception {
		// TODO Auto-generated method stub
		List<CompanyDomain> companyDomain = companyDao.getCompany();
		return companyMapper.entityList(companyDomain);

	}

	@Override
	public String deleteCompany(String id) throws Exception {
		// TODO Auto-generated method stub
		return companyDao.deleteCompany(id);
	}
}

//Iterator<Cell> cellIterator = row.cellIterator();
//	Cell cell = cellIterator.next();
//	String val;
//	for (int i = 0; i < header - 1; i++) {
//		val=cell.getStringCellValue();
//	    
//	}
// For the colCount use something like row.getPhysicalNumberOfCells()

//	for (int rowIndex = 0; rowIndex <= sheet.getLastRowNum(); rowIndex++) {

//Row row = rowIterator.next();
//	  row = sheet.getRow(0);
//		  Iterator<Cell> cellIterator = row.cellIterator();
//		  if (row != null) {
//			int collen= row.getPhysicalNumberOfCells();

//			// Cell cell = row.getCell(collen);
//			Cell cell = cellIterator.next();
//			for(int i=0;i<collen;i++) {
//		       
//		      // Found column and there is value in the cell.
//		      header.add(cell.getStringCellValue());
//		      System.out.println(header);
//		      break; 
//		      // Do something with the cellValueMaybeNull here ...
//		      // break; ???
//		   // }
//		  
//			}
//		}
//		//  Row row1 = sheet.getRow(0);
//		//  Row row1 = sheet.getRow(0);

//	Cell cell = cellIterator.next();
//String val;
//for (int i = 0; i < header - 1; i++) {
//	val=cell.getStringCellValue();
//    
//}
//For the colCount use something like row.getPhysicalNumberOfCells()

//for (int rowIndex = 0; rowIndex <= sheet.getLastRowNum(); rowIndex++) {

//Iterator<Cell> cellIterator = row.cellIterator();
//if (row != null) {
//	int collen= row.getPhysicalNumberOfCells();

//	// Cell cell = row.getCell(collen);
//	Cell cell = cellIterator.next();
//	for(int i=0;i<collen;i++) {
//     
//    // Found column and there is value in the cell.
//    header.add(cell.getStringCellValue());
//    System.out.println(header);
//    break; 
//    // Do something with the cellValueMaybeNull here ...
//    // break; ???
// // }
//
//	}
//}
//
//
//
//
//
//for(int rowIndex = 1; rowIndex <= sheet.getLastRowNum(); rowIndex++) {
//    CompanyDomain companyDomain = new CompanyDomain();
//for(int cellIndex = 0; cellIndex <= row.getPhysicalNumberOfCells(); cellIndex++)  {
//		//Cell cell = cellIterator.next();
//
//		//int columnIndex = cell.getColumnIndex();
//		switch (cellIndex) {
//		case 0:
//			Cell id=wb.getSheetAt(0).getRow(rowIndex).getCell(cellIndex);
//			System.out.println(id);
//			companyDomain.setUuId(id.toString());
//			
//			break;
//	
//		case 1:
//			Cell cn=wb.getSheetAt(0).getRow(rowIndex).getCell(cellIndex);
////			System.out.println(wb.getSheetAt(0).getRow(4).getCell(7));
//			companyDomain.setCompanyName(cn.toString());
//			System.out.println(cn);
//			break;
//		case 2:
//			Cell ct=wb.getSheetAt(0).getRow(rowIndex).getCell(cellIndex);
//			System.out.println(ct);
//			companyDomain.setCompanyType(ct.toString());
//			break;
//		case 3:
//			Cell e=wb.getSheetAt(0).getRow(rowIndex).getCell(cellIndex);
//			System.out.println(e);
//		
//			//companyDomain.setEstablished(Integer.parseInt(cell.getStringCellValue()));
//		companyDomain.setEstablished((int) e.getNumericCellValue());
//			break;
//		case 4:
//			Cell md=wb.getSheetAt(0).getRow(rowIndex).getCell(cellIndex);
//			companyDomain.setMdName(md.toString());
//			System.out.println(md);
//			break;
//
//		case 5:
//			Cell mn=wb.getSheetAt(0).getRow(rowIndex).getCell(cellIndex);
//			companyDomain.setMobileNumber(mn.toString());
//			System.out.println(mn);
//			// companyModel.setMobileNumber((String.valueOf((int)cell.getNumericCellValue())));
//			break;
//
//		case 6:
//			Cell ei=wb.getSheetAt(0).getRow(rowIndex).getCell(cellIndex);
//			companyDomain.setEmailId(ei.toString());
//			System.out.println(ei);
//			break;
//		case 7:
//			Cell pn=wb.getSheetAt(0).getRow(rowIndex).getCell(cellIndex);
//			companyDomain.setPanNumber(pn.toString());
//			System.out.println(pn);
//			break;
//		case 8:
//			Cell gst=wb.getSheetAt(0).getRow(rowIndex).getCell(cellIndex);
//			companyDomain.setGstNumber(gst.toString());
//			System.out.println(gst);
//			// System.out.println(companyModel.getGstNumber());
//			break;
//		case 9:
//			Cell ws=wb.getSheetAt(0).getRow(rowIndex).getCell(cellIndex);
//			companyDomain.setWebSite(ws.toString());
//			System.out.println(ws);
//			System.out.println("Web");
//			break;
//		case 10:
//			Cell ia=wb.getSheetAt(0).getRow(rowIndex).getCell(cellIndex);
//			//System.out.println("Act"+ cell.getCellType());
//			System.out.println(ia);
//			companyDomain.setIsActive(Boolean.parseBoolean(ia.toString()));
//			//companyDomain.setIsActive(Boolean.parseBoolean(cell.getStringCellValue()));
//			break;
//		case 11:
//			Cell cd=wb.getSheetAt(0).getRow(rowIndex).getCell(cellIndex);
//			companyDomain.setCreatedDate(cd.toString());
//			break;
//		case 12:
//			Cell mda=wb.getSheetAt(0).getRow(rowIndex).getCell(cellIndex);
//			companyDomain.setModifiedDate(mda.toString());
//			break;
//		}
//		
//	}
//	CompanyModel companyModel=new CompanyModel(); 
//	BeanUtils.copyProperties(companyDomain,companyModel);
//	//companyValidation.validateCompany(companyModel);
//	company.add(companyDomain);
//	System.out.println("hi");	
//	in.close();