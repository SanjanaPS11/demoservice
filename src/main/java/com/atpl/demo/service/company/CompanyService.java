package com.atpl.demo.service.company;

import java.util.List;

import com.atpl.demo.model.company.CompanyModel;

import org.springframework.validation.Errors;
import org.springframework.web.multipart.MultipartFile;
import java.sql.Blob;
public interface CompanyService {
	
	public String uploadExcel(MultipartFile file) throws Exception;
	
	public List<CompanyModel> search(String value) throws Exception;
	
	public List<CompanyModel> companySearch(String key,String value) throws Exception;
	
	public List<CompanyModel> companyPagination(int PageSize,int PageNo) throws Exception;

	public String uploadImage(MultipartFile image) throws Exception;
	
	public String downloadImage(String imagepath, String uuId) throws Exception;
	
	public String downloadExcel(String path) throws Exception;
		
	public String patchCompanyStatus(Boolean status, String uuId ) throws Exception;
	
	public List<CompanyModel> getCompanyStatus(Boolean status) throws Exception;
	
	public String saveCompany(CompanyModel companyModel) throws Exception;

	public CompanyModel getCompany(String id) throws Exception;

	public List<CompanyModel> getCompany() throws Exception;

	public String updateCompany(CompanyModel id, String uuId) throws Exception;

	public String deleteCompany(String id) throws Exception;

   public String patchCompany(CompanyModel id) throws Exception;
}
