package com.atpl.demo.service.boarding;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.atpl.demo.constant.Constants;
import com.atpl.demo.dao.boarding.BoardingDao;
import com.atpl.demo.domain.boarding.BoardingDomain;
import com.atpl.demo.domain.company.CompanyDomain;
import com.atpl.demo.domain.boarding.BoardingDomain;
import com.atpl.demo.domain.boarding.BoardingDomain;
import com.atpl.demo.exception.MmgRestException.NOT_FOUND;
import com.atpl.demo.mapper.boarding.BoardingMapper;
import com.atpl.demo.model.boarding.BoardingModel;
import com.atpl.demo.model.company.CompanyModel;
import com.atpl.demo.model.boarding.BoardingModel;

import com.atpl.demo.service.boarding.BoardingService;
import com.atpl.demo.service.boarding.BoardingServiceImpl;
import com.atpl.demo.service.boarding.BoardingServiceImpl;

@Service("boardingService")
public class BoardingServiceImpl implements BoardingService, Constants{
	@Autowired
	BoardingDao boardingDao;

	@Autowired
	BoardingMapper boardingMapper;
	
	private static final Logger logger = LoggerFactory.getLogger(BoardingServiceImpl.class);
	public BoardingServiceImpl() {
		// constructor
	}
	@Override
	public BoardingModel getBoardingCount(Integer stateId) throws Exception {
		System.out.println(stateId);
		BoardingDomain boardingDomain = boardingDao.getBoardingCount(stateId);
	
		BoardingModel boardingModel = new BoardingModel();
		System.out.println(boardingModel);
		BeanUtils.copyProperties(boardingDomain,boardingModel);
		return boardingModel;
	}
}
