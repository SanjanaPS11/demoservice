package com.atpl.demo.service.boarding;
import java.util.List;

import org.springframework.web.bind.annotation.PathVariable;

import com.atpl.demo.exception.GenericRes;
import com.atpl.demo.model.boarding.BoardingModel;

public interface BoardingService {

	public BoardingModel getBoardingCount(Integer stateId) throws Exception;
}
