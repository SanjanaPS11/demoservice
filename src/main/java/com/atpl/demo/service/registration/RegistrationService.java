package com.atpl.demo.service.registration;

import java.util.List;

import com.atpl.demo.exception.GenericRes;
import com.atpl.demo.model.registration.RegistrationModel;

public interface RegistrationService {

	public String saveRegistration(RegistrationModel registrationModel) throws Exception;

	public RegistrationModel getRegistration(int id) throws Exception;

	public List<RegistrationModel> getRegistration() throws Exception;

	public String updateRegistration(RegistrationModel id) throws Exception;

	public String deleteRegistration(int id) throws Exception;
}
