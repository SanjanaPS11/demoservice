package com.atpl.demo.service.registration;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.atpl.demo.constant.Constants;
import com.atpl.demo.dao.registration.RegistrationDao;
import com.atpl.demo.domain.registration.RegistrationDomain;
import com.atpl.demo.exception.MmgRestException.NOT_FOUND;
import com.atpl.demo.mapper.registration.RegistrationMapper;
import com.atpl.demo.model.registration.RegistrationModel;

@Service("registrationService")
public class RegistrationServiceImpl implements RegistrationService, Constants {

	@Autowired
	RegistrationDao registrationDao;

	@Autowired
	RegistrationMapper registrationMapper;

	private static final Logger logger = LoggerFactory.getLogger(RegistrationServiceImpl.class);

	public RegistrationServiceImpl() {
		// constructor
	}

	public String saveRegistration(RegistrationModel registrationModel) throws Exception {
		RegistrationDomain registrationDomain = new RegistrationDomain();
		BeanUtils.copyProperties(registrationModel, registrationDomain);
		return registrationDao.saveRegistration(registrationDomain);
	}

	@Override
	public List<RegistrationModel> getRegistration() throws Exception {
		List<RegistrationDomain> registrationDomain = registrationDao.getRegistration();
		return registrationMapper.entityList(registrationDomain);
	}

	@Override
	public String updateRegistration(RegistrationModel id) throws Exception {
		RegistrationDomain registrationDomain = new RegistrationDomain();
		BeanUtils.copyProperties(id, registrationDomain);
		return registrationDao.updateRegistration(registrationDomain);

	}

	@Override
	public RegistrationModel getRegistration(int id) throws Exception {
		RegistrationDomain registrationDomain = registrationDao.getRegistration(id);
		RegistrationModel registrationModel = new RegistrationModel();
		if (registrationDomain == null)
			throw new NOT_FOUND("Bank not found");
		BeanUtils.copyProperties(registrationDomain, registrationModel);
		return registrationModel;
	}

	@Override
	public String deleteRegistration(int id) throws Exception {
		return registrationDao.deleteRegistration(id);
	}

}
