package com.atpl.demo.service.vehicle;
import java.util.List;

import org.springframework.web.bind.annotation.PathVariable;

import com.atpl.demo.exception.GenericRes;
import com.atpl.demo.model.vehicle.VehicleModel;

public interface VehicleService {

	public Integer getVehicleCount(String requestedBy,String status,Integer stateId) throws Exception;
}
