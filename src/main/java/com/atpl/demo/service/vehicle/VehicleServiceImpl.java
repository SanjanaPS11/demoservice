package com.atpl.demo.service.vehicle;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.atpl.demo.constant.Constants;
import com.atpl.demo.dao.vehicle.VehicleDao;
import com.atpl.demo.domain.vehicle.VehicleDomain;
import com.atpl.demo.domain.vehicle.VehicleDomain;
import com.atpl.demo.domain.vehicle.VehicleDomain;
import com.atpl.demo.exception.MmgRestException.NOT_FOUND;
import com.atpl.demo.mapper.vehicle.VehicleMapper;
import com.atpl.demo.model.vehicle.VehicleModel;
import com.atpl.demo.model.vehicle.VehicleModel;
import com.atpl.demo.model.vehicle.VehicleModel;
import com.atpl.demo.service.driver.DriverService;
import com.atpl.demo.service.vehicle.VehicleService;
import com.atpl.demo.service.vehicle.VehicleServiceImpl;
import com.atpl.demo.service.vehicle.VehicleServiceImpl;

@Service("vehicleService")
public class VehicleServiceImpl  implements VehicleService, Constants{

	@Autowired
	VehicleDao vehicleDao;

	@Autowired
	VehicleMapper driverMapper;
	private static final Logger logger = LoggerFactory.getLogger(VehicleServiceImpl.class);
	public VehicleServiceImpl() {
		// constructor
	}
	@Override
	public Integer getVehicleCount(String requestedBy,String status,Integer stateId) throws Exception {
//		DriverDomain driverDomain = new DriverDomain();
//		BeanUtils.copyProperties(driverModel, driverDomain);
		System.out.println(requestedBy+ status+ stateId);
		return vehicleDao.getVehicleCount(requestedBy,status,stateId);
	}
}
