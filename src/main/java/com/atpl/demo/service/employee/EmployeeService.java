package com.atpl.demo.service.employee;
import java.util.List;

import com.atpl.demo.exception.GenericRes;
import com.atpl.demo.model.employee.EmployeeModel;

public interface EmployeeService {

	public String saveEmployee(EmployeeModel employeeModel) throws Exception;

	public EmployeeModel getEmployee(int id) throws Exception;

	public List<EmployeeModel> getEmployee() throws Exception;

	public String updateEmployee(EmployeeModel id) throws Exception;

	public String deleteEmployee(int id) throws Exception;
}
