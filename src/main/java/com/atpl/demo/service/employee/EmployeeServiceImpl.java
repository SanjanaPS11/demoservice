package com.atpl.demo.service.employee;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.atpl.demo.constant.Constants;
import com.atpl.demo.dao.employee.EmployeeDao;
import com.atpl.demo.domain.employee.EmployeeDomain;
import com.atpl.demo.exception.MmgRestException.NOT_FOUND;
import com.atpl.demo.mapper.employee.EmployeeMapper;
import com.atpl.demo.model.employee.EmployeeModel;

@Service("employeeService")
public class EmployeeServiceImpl implements EmployeeService, Constants{

	@Autowired
	EmployeeDao employeeDao;

	@Autowired
	EmployeeMapper employeeMapper;
	
	private static final Logger logger = LoggerFactory.getLogger(EmployeeServiceImpl.class);

	public EmployeeServiceImpl() {
		// constructor
	}
	
	
	public String saveEmployee(EmployeeModel employeeModel) throws Exception {
		EmployeeDomain employeeDomain = new EmployeeDomain();
		BeanUtils.copyProperties(employeeModel, employeeDomain);
		return employeeDao.saveEmployee(employeeDomain);
	}
	
	@Override
	public List<EmployeeModel> getEmployee() throws Exception {
		List<EmployeeDomain> employeeDomain = employeeDao.getEmployee();
		return employeeMapper.entityList(employeeDomain);
	}
	
	@Override
	public String updateEmployee(EmployeeModel id) throws Exception {
		EmployeeDomain employeeDomain = new EmployeeDomain();
		BeanUtils.copyProperties(id, employeeDomain);
		return employeeDao.updateEmployee(employeeDomain);

	}

	@Override
	public EmployeeModel getEmployee(int id) throws Exception {
		EmployeeDomain employeeDomain = employeeDao.getEmployee(id);
		EmployeeModel employeeModel = new EmployeeModel();
		if (employeeDomain == null)
			throw new NOT_FOUND("Bank not found");
		BeanUtils.copyProperties(employeeDomain, employeeModel);
		return employeeModel;
	}
	
	@Override
	public String deleteEmployee(int id) throws Exception {
		return employeeDao.deleteEmployee(id);
	}
}
