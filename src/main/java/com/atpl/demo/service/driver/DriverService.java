package com.atpl.demo.service.driver;
import java.util.List;

import org.springframework.web.bind.annotation.PathVariable;

import com.atpl.demo.exception.GenericRes;
import com.atpl.demo.model.driver.DriverModel;

public interface DriverService {
	
//	public Integer getDriverCount(String requestedBy,String status,Integer stateId) throws Exception;
	
	public String saveDriver(DriverModel driverModel) throws Exception;
	
	public String saveDriver(DriverModel driverModel,String firstName,String roleName,String requestNumber)throws Exception;
}
