package com.atpl.demo.service.driver;
import java.sql.Blob;
import java.util.Properties;

import javax.mail.internet.MimeMessage;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.ModelAttribute;

import com.atpl.demo.constant.Constants;
import com.atpl.demo.dao.driver.DriverDao;
import com.atpl.demo.domain.driver.DriverDomain;
import com.atpl.demo.domain.message.RoleId;
import com.atpl.demo.exception.MmgRestException.SAVE_FAILED;
import com.atpl.demo.mapper.driver.DriverMapper;
import com.atpl.demo.model.driver.DriverModel;
import com.atpl.demo.utils.CompanyValidationUtils;
import com.atpl.demo.utils.EmailSend;

import javax.mail.Message;
import javax.mail.Message.RecipientType;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import org.springframework.mail.MailSender;  
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.mail.javamail.MimeMessageHelper; 
import java.util.Properties;
@Service("driverService")
public class DriverServiceImpl implements DriverService, Constants{

	@Autowired
	DriverDao driverDao;

	@Autowired
	EmailSend emailSend;
	
	@Autowired
	DriverMapper driverMapper;
	
	@Autowired
	CompanyValidationUtils companyValidation;
	
	private static final Logger logger = LoggerFactory.getLogger(DriverServiceImpl.class);
	public DriverServiceImpl() {
		// constructor
	}

//	@ModelAttribute
//	public void addAttributes(DriverModel driverModel) {
//		driverModel.addAttribute("name", "Welcome to the Netherlands!");
//	}
	
	@Override
	public String saveDriver(DriverModel driverModel) throws Exception {
		DriverDomain driverDomain = new DriverDomain();
		messageSend(driverModel);
		BeanUtils.copyProperties(driverModel, driverDomain);
		return "message Send";
	}
	
	@ModelAttribute("name")
	public String messages() {

	    return "san";
	}
	private String messageSend(DriverModel driverModel) throws Exception{
		DriverDomain driverDomain = new DriverDomain();
		companyValidation.validatePhone(driverModel.getPhoneNo());
		companyValidation.validateEmail(driverModel.getEmail());
		BeanUtils.copyProperties(driverModel, driverDomain);
		Blob res=driverDao.saveDriver(driverDomain);
		byte[] bdata = res.getBytes(1, (int) res.length());
		String message = new String(bdata);
		//String message = new String(bdat;
		
		String msg=emailSend.emailSend(driverModel,message);
		return msg;
	}
	public String saveDriver(DriverModel driverModel,String firstName,String roleName,String requestNumber)throws Exception{
		DriverDomain driverDomain = new DriverDomain();
		companyValidation.validatePhone(driverModel.getPhoneNo());
		companyValidation.validateEmail(driverModel.getEmail());
		BeanUtils.copyProperties(driverModel, driverDomain);
		Blob res=driverDao.saveDriver(driverDomain);
		byte[] bdata = res.getBytes(1, (int) res.length());
		String message = new String(bdata);
		//String message = new String(bdat;
		
		//String msg=emailSend.emailSend(driverModel,message,firstName,roleName);
		return "Semd";
	}
}












