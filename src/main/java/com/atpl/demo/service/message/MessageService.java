package com.atpl.demo.service.message;

import java.util.List;


import com.atpl.demo.model.message.MessageModel;

public interface MessageService {
	
	public String saveMessage(MessageModel MessageModel) throws Exception;
	
    public List<MessageModel> getMessage() throws Exception;
	
	public String updateMessage(MessageModel uuId) throws Exception;
	
	public String deleteMessage(String uuId) throws Exception;
	
	public MessageModel getMessageByuuId(String uuId)throws Exception;
	
	public MessageModel getMessageByActiveOrDeactive(Boolean isActive)throws Exception;

}
