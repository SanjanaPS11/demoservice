package com.atpl.demo.service.message;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;
import java.util.UUID;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.atpl.demo.constant.Constants;
import com.atpl.demo.dao.message.MessageDao;

import com.atpl.demo.domain.message.MessageDomain;

import com.atpl.demo.exception.MmgRestException.NOT_FOUND;
import com.atpl.demo.exception.MmgRestException.SAVE_FAILED;
import com.atpl.demo.mapper.message.MessageMapper;
import com.atpl.demo.model.message.MessageModel;
import com.atpl.demo.model.registration.RegistrationModel;
import com.atpl.demo.service.company.CompanyService;
import com.atpl.demo.utils.DateUtility;
import com.atpl.demo.domain.message.RoleId;
@Service("messageService")
public class MessageServiceImpl implements MessageService, Constants{
	
	@Autowired
	MessageDao messageDao;

	@Autowired
	MessageMapper messageMapper;

private static final Logger logger = LoggerFactory.getLogger(MessageServiceImpl.class);

public MessageServiceImpl() {
	// constructor
}
    public String saveMessage(MessageModel messageModel) throws Exception
	{

		MessageDomain messageDomain = new MessageDomain();
		BeanUtils.copyProperties(messageModel, messageDomain);
		int role;
		if(messageDomain.getRoleName().equals("Franchise")) {
			RoleId roleID= RoleId.Franchise;
			role=roleID.getRoleCode();
		}
		 else if(messageDomain.getRoleName().equals("Driver")) {
			 RoleId roleID= RoleId.Driver;
				role=roleID.getRoleCode();
		}
		 else if(messageDomain.getRoleName().equals("Customer")) {
			 RoleId roleID= RoleId.Customer;
				role=roleID.getRoleCode();
		}
		 else {
			 throw new SAVE_FAILED("Cant save you have entered wrong role name");
		 }

		Boolean uuid=messageDao.checkUuId(role,messageDomain.getMessageName());
		if(uuid) {
			String id = messageDao.getUuId(role,messageDomain.getMessageName());
			messageDao.updateActive(id);
		}
		
		System.out.println(uuid);
		return	messageDao.saveMessage(messageDomain,role);
	}
	@Override
	public List<MessageModel> getMessage() throws Exception {
		List<MessageDomain> messageDomain = messageDao.getMessage();
		return messageMapper.entityList(messageDomain);
	}
	@Override
	public String updateMessage(MessageModel uuId) throws Exception {
		MessageDomain messageDomain = new MessageDomain();
		BeanUtils.copyProperties(uuId, messageDomain);
		return messageDao.updateMessage(messageDomain);
	}
	@Override
	public String deleteMessage(String uuId) throws Exception {
		return messageDao.deleteMessage(uuId);
	}
	@Override
	public MessageModel getMessageByuuId(String uuId) throws Exception {
		MessageDomain messageDomain = messageDao.getMessageByuuId(uuId);
		MessageModel messageModel = new MessageModel();
		if (messageDomain == null)
			throw new NOT_FOUND("Bank not found");
		BeanUtils.copyProperties(messageDomain, messageModel);
		return messageModel;
	}
	@Override
	public MessageModel getMessageByActiveOrDeactive(Boolean isActive) throws Exception {
		// TODO Auto-generated method stub
		return null;
	}
	
}
