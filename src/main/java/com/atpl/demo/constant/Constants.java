package com.atpl.demo.constant;

public interface Constants {

	public static String ACCESS_KEY_HEADER = "X-API-KEY";
	public static String BEARER_TOKEN ="authorization";
	public static String BY_PASS_TOKEN = "X-BYPASS-TOKEN";
	public final static String EXCEL_TYPE = "Excel";
	public final static String PDF_TYPE = "Pdf";
	public final static int API_TIME_OUT=30000;
	/**
	 * Email configuration
	 */
	
	public static String EMAIL_SENDER = "MoveMyGoods <donotreply@arohaka.com>";
	public static String EMAIL_CONFIGURATION_SET = "mmgSimpleMail";

}
