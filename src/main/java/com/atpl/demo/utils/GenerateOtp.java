package com.atpl.demo.utils;
import java.util.Random;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;
@Repository
public class GenerateOtp {
private static final Logger logger = LoggerFactory.getLogger(EmailSend.class);
	
    public GenerateOtp(){
		
	}
//	   public static void main(String[] args) {
//
//	      System.out.println(generateOTP(4));
//	   }

	   
//	   private static char[] generateOTP(int length) {
//	      String numbers = "1234567890";
//	      Random random = new Random();
//	      char[] otp = new char[length];
//
//	      for(int i = 0; i< length ; i++) {
//	         otp[i] = numbers.charAt(random.nextInt(numbers.length()));
//	      }
//	      return otp;
//	   }
	   
	   public char[] generateOtp()throws Exception
	   {
		   int length=4;
		   String numbers = "1234567890";
		      Random random = new Random();
		      char[] otp = new char[length];

		      for(int i = 0; i< length ; i++) {
		         otp[i] = numbers.charAt(random.nextInt(numbers.length()));
		      }
		      return otp;
	   }
	   
	   
	}
