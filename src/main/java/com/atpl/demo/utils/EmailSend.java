package com.atpl.demo.utils;

import java.util.Properties;

import javax.mail.internet.MimeMessage;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;
import javax.mail.Message;
import javax.mail.Message.RecipientType;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import org.springframework.mail.MailSender;  
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.mail.javamail.MimeMessageHelper; 
import java.util.Properties;
import com.atpl.demo.model.driver.DriverModel;
//@Repository
@Component
public class EmailSend {
	private static final Logger logger = LoggerFactory.getLogger(EmailSend.class);
	
    public EmailSend(){
		
	}
	public String emailSend(DriverModel driverModel,String msg)throws Exception {
		JavaMailSenderImpl mailSender = new JavaMailSenderImpl();
		mailSender.setHost("smtp.gmail.com");
		mailSender.setPort(587);
		mailSender.setUsername("pssanjana29@gmail.com");
		mailSender.setPassword("sanjanabalu12345");
		 
		Properties properties = new Properties();
		properties.setProperty("mail.smtp.auth", "true");
		properties.setProperty("mail.smtp.starttls.enable", "true");
		properties.setProperty("mail.debug", "true"); 
		properties.setProperty("https.protocols", "TLSv1,TLSv1.1,TLSv1.2");
		mailSender.setJavaMailProperties(properties);
		String from = "pssanjana29@gmail.com";
		
		
		MimeMessage message = mailSender.createMimeMessage();
		MimeMessageHelper helper = new MimeMessageHelper(message);
		 
		
		String to = driverModel.getEmail();
		String name=driverModel.getName();
		char[] otpNumber;
		
		helper.setSubject("Move My Goods");
		helper.setFrom(from);
		helper.setTo(to);
		
		boolean html = true;
		String htmlString = msg.replace("${name}", name); 
		htmlString =htmlString.replace("${firstName}", driverModel.getFirstName());
		htmlString =htmlString.replace("${requestNumber}", driverModel.getRequestNumber());
		htmlString =htmlString.replace("${roleName}", driverModel.getRoleName());
		htmlString =htmlString.replace("${customerName}",driverModel.getCustomer());
		if(driverModel.getMessageName().equals("OTP")) {
			GenerateOtp OTP=new GenerateOtp();
			otpNumber = OTP.generateOtp();
			
			String otp = new String(otpNumber);
		    htmlString.replace("${otp}",otp); 
		}

		helper.setText(htmlString,html);
	
		mailSender.send(message);
		return "Message send";
	}
}
