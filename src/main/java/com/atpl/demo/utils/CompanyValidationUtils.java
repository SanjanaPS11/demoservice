package com.atpl.demo.utils;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.atpl.demo.dao.company.CompanyDao;
import com.atpl.demo.exception.MmgRestException.DETAILS_ALREADY_EXIST;
import com.atpl.demo.exception.MmgRestException.EMAILID_PATTERN_NOT_MATCH;
import com.atpl.demo.exception.MmgRestException.GST_PATTERN_NOT_MATCH;
import com.atpl.demo.exception.MmgRestException.MOBILENUMBER_PATTERN_NOT_MATCH;
import com.atpl.demo.exception.MmgRestException.NOT_FOUND;
import com.atpl.demo.exception.MmgRestException.PAN_NUMBER_ALREADY_EXIST;
import com.atpl.demo.exception.MmgRestException.PAN_PATTERN_NOT_MATCH;
import com.atpl.demo.model.company.CompanyModel;

@Repository
@SuppressWarnings("rawtypes")
public class CompanyValidationUtils {

	@Autowired
	CompanyDao companyDao;

	private static final Logger logger = LoggerFactory.getLogger(CompanyValidationUtils.class);

	public CompanyValidationUtils() {

	}

	public void validateCompany(CompanyModel companyModel) throws Exception {

		if (null == companyModel.getCompanyName())
			throw new NOT_FOUND("Enter the Company name");
		if (null == companyModel.getCompanyType())
			throw new NOT_FOUND("Enter the company Type");
		if (null == companyModel.getEmailId())
			throw new NOT_FOUND("Enter the Email Id ");
		if (null == companyModel.getEstablished())
			throw new NOT_FOUND("Enter the Established");
		if (null == companyModel.getGstNumber())
			throw new NOT_FOUND("Enter the Gst Number");
		if (null == companyModel.getPanNumber())
			throw new NOT_FOUND("Enter the Pan Number");

		if (companyDao.companyValidatation("uuId", companyModel.getUuId()))
			throw new DETAILS_ALREADY_EXIST("UuId already Exist");
		if (companyDao.companyValidatation("emailId", companyModel.getEmailId()))
			throw new DETAILS_ALREADY_EXIST("EmailId already Exist");
		if (companyDao.companyValidatation("panNumber", companyModel.getPanNumber()))
			throw new PAN_NUMBER_ALREADY_EXIST();
		if (companyDao.companyValidatation("gstNumber", companyModel.getGstNumber()))
			throw new DETAILS_ALREADY_EXIST("gst already Exist");
		if (companyDao.companyValidatation("webSite", companyModel.getWebSite()))
			throw new DETAILS_ALREADY_EXIST("website already Exist");

//		String regex = "^[a-zA-Z0-9_+&*-]+(?:\\." + "[a-zA-Z0-9_+&*-]+)*@" + "(?:[a-zA-Z0-9-]+\\.)+[a-z" + "A-Z]{2,7}$";
//		Pattern pattern = Pattern.compile(regex);
//		Matcher matcher = pattern.matcher(companyModel.getEmailId());
//		if (!matcher.matches()) {
//			throw new EMAILID_PATTERN_NOT_MATCH();
//		}
		validateEmail(companyModel.getEmailId());
		String panNumber = "[A-Z]{5}[0-9]{4}[A-Z]{1}";
		Pattern panNumberPattern = Pattern.compile(panNumber);
		Matcher panNumber1 = panNumberPattern.matcher(companyModel.getPanNumber());
   System.out.println(!panNumber1.matches());
		if (!panNumber1.matches()) {
			throw new PAN_PATTERN_NOT_MATCH();
		}
		String gst = "[0-9]{2}[a-zA-Z]{5}[0-9]{4}[a-zA-Z]{1}[1-9A-Za-z]{1}[Z]{1}[0-9a-zA-Z]{1}";
		Pattern gstNumberPattern = Pattern.compile(gst);
		Matcher gstNumber = gstNumberPattern.matcher(companyModel.getGstNumber());
		if (!gstNumber.matches()) {
			throw new GST_PATTERN_NOT_MATCH();
		}
		
//		String phone = "^[789]\\d{9}$";
//		Pattern phoneNumberPattern = Pattern.compile(phone);
//		Matcher phoneNumber = phoneNumberPattern.matcher(companyModel.getMobileNumber());
//		if (!phoneNumber.matches()) {
//			throw new MOBILENUMBER_PATTERN_NOT_MATCH();
//		}
		validatePhone(companyModel.getMobileNumber());
		
	}
	
	public void validatePhone(String phoneNo) {
	String phone = "^[789]\\d{9}$";
	Pattern phoneNumberPattern = Pattern.compile(phone);
	Matcher phoneNumber = phoneNumberPattern.matcher(phoneNo);
	if (!phoneNumber.matches()) {
		throw new MOBILENUMBER_PATTERN_NOT_MATCH();

	}
	}
	public void validateEmail(String emailId) {
	String regex = "^[a-zA-Z0-9_+&*-]+(?:\\." + "[a-zA-Z0-9_+&*-]+)*@" + "(?:[a-zA-Z0-9-]+\\.)+[a-z" + "A-Z]{2,7}$";
	Pattern pattern = Pattern.compile(regex);
	Matcher matcher = pattern.matcher(emailId);
	System.out.println(emailId);
	
	if (!matcher.matches()) {
		throw new EMAILID_PATTERN_NOT_MATCH();
	}
	}
}

