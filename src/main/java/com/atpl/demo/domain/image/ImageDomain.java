package com.atpl.demo.domain.image;

import java.io.Serializable;
import java.sql.Blob;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

@JsonInclude(Include.NON_DEFAULT)
public class ImageDomain implements Serializable{

	private static final long serialVersionUID = -2196266153406082047L;
	
	private String uuId;
	private String imageType;
	private String imageName;
	private byte[] image;
	/**
	 * @return the uuId
	 */
	public String getUuId() {
		return uuId;
	}
	/**
	 * @param uuId the uuId to set
	 */
	public void setUuId(String uuId) {
		this.uuId = uuId;
	}
	/**
	 * @return the imageType
	 */
	public String getImageType() {
		return imageType;
	}
	/**
	 * @param imageType the imageType to set
	 */
	public void setImageType(String imageType) {
		this.imageType = imageType;
	}
	/**
	 * @return the imageName
	 */
	public String getImageName() {
		return imageName;
	}
	/**
	 * @param imageName the imageName to set
	 */
	public void setImageName(String imageName) {
		this.imageName = imageName;
	}
	public byte[] getImage() {
		return image;
	}
	public void setImage(byte[] image) {
		this.image = image;
	}	
}
