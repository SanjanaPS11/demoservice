package com.atpl.demo.domain.addition;
import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

@JsonInclude(Include.NON_DEFAULT)
public class AdditionDomain implements Serializable{

	private static final long serialVersionUID = -2196266153406082047L;
	
	public Integer id;
	public Integer add1;
	public Integer add2;
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public Integer getAdd1() {
		return add1;
	}
	public void setAdd1(Integer add1) {
		this.add1 = add1;
	}
	public Integer getAdd2() {
		return add2;
	}
	public void setAdd2(Integer add2) {
		this.add2 = add2;
	}
}
