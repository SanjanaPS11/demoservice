package com.atpl.demo.domain.message;

public enum RoleId {
	
	Driver(1),Customer(2),Franchise(3);

	private final int roleCode;
	
	
	RoleId(int roleCode) {
		this.roleCode=roleCode;
	}
	
	public int getRoleCode() {
		return this.roleCode;
	}
}
