package com.atpl.demo.exception;

import static org.apache.http.HttpStatus.SC_OK;

import java.util.LinkedHashMap;
import java.util.Map;

import org.apache.http.HttpStatus;

import com.atpl.demo.exception.MmgRestException;

/**
 *
 * @author Raghu M
 * @since 2020-02-09
 */

public class MmgRestException extends RuntimeException {
	private static final long serialVersionUID = -977217199574702703L;

	public static int INTERNAL_ERROR_CODE = -1;
	protected int httpErrorCode = SC_OK;
	protected int errorCode = Integer.MAX_VALUE;

	protected String exReason;

	protected Map<String, Object> meta;
	protected Map<String, String> dictionary;

	public MmgRestException() {
		super();
		this.meta = new LinkedHashMap<String, Object>();
	}

	public MmgRestException(String message) {
		super(message);
		this.meta = new LinkedHashMap<String, Object>();
	}

	public MmgRestException(String message, Throwable t) {
		super(message, t);
		this.meta = new LinkedHashMap<String, Object>();
	}

	public MmgRestException(String message, Throwable t, int errCode) {
		this(message, t);
		errorCode = errCode;
	}

	public MmgRestException(String message, Throwable t, int errCode, int httpErrCode) {
		this(message, t);
		errorCode = errCode;
		httpErrorCode = httpErrCode;
	}

	public MmgRestException(int errCode, int httpErrCode, String message) {
		this(message);
		errorCode = errCode;
		httpErrorCode = httpErrCode;
	}

	public MmgRestException(int errCode, int httpErrCode, String message, Throwable t) {
		this(message, t);
		errorCode = errCode;
		httpErrorCode = httpErrCode;
	}

	public String getExReason() {
		return exReason;
	}

	public void setExReason(String exReason) {
		this.exReason = exReason;
	}

	public int getErrorCode() {
		return errorCode;
	}

	public void setErrorCode(int errorCode) {
		this.errorCode = errorCode;
	}

	public int getHttpErrorCode() {
		return httpErrorCode;
	}

	public void setHttpErrorCode(int httpErrorCode) {
		this.httpErrorCode = httpErrorCode;
	}

	public Map<String, Object> getMeta() {
		return meta;
	}

	public void setMeta(Map<String, Object> meta) {
		this.meta = meta;
	}

	public Map<String, String> getDictionary() {
		return dictionary;
	}
	

	public void setDictionary(Map<String, String> dictionary) {
		this.dictionary = dictionary;
	}

	/**
	 * Common Exceptions
	 */
	
	
	public static final class BACKEND_SERVER_ERROR extends MmgRestException {
		private static final long serialVersionUID = 1L;

		public BACKEND_SERVER_ERROR() {
			super(1000, HttpStatus.SC_INTERNAL_SERVER_ERROR, String.format("Backend Server Error, Please Retry."));
		}
		
		
	}

	
	public static final class BACKEND_SERVER_READ_TIMEOUT extends MmgRestException {
		private static final long serialVersionUID = 1L;

		public BACKEND_SERVER_READ_TIMEOUT() {
			super(1001, HttpStatus.SC_GATEWAY_TIMEOUT,
					String.format("Backend Server read timeout Error, Please Retry."));
		}
	}
	

	public static final class ACCESS_DENIED extends MmgRestException {
		private static final long serialVersionUID = 1L;

		public ACCESS_DENIED() {
			super(1002, HttpStatus.SC_UNAUTHORIZED, String.format("Access denied Error"));
		}
	}

	public static final class FORBIDDEN extends MmgRestException {
		private static final long serialVersionUID = 1L;

		public FORBIDDEN() {
			super(1003, HttpStatus.SC_FORBIDDEN, String.format("Forbidden Error"));
		}
	}

	public static final class SC_NOT_FOUND extends MmgRestException {
		private static final long serialVersionUID = 1L;

		public SC_NOT_FOUND() {
			super(1004, HttpStatus.SC_NOT_FOUND, String.format("Not Found"));
		}
	}

	public static final class BACKEND_PARSE_ERROR extends MmgRestException {
		private static final long serialVersionUID = 1L;

		public BACKEND_PARSE_ERROR() {
			super(1005, HttpStatus.SC_INTERNAL_SERVER_ERROR, String.format("Backend parse Error"));
		}
	}
	public static final class SAVE_FAILED extends MmgRestException {
		private static final long serialVersionUID = 1L;

		public SAVE_FAILED(String message) {
			super(1006, HttpStatus.SC_NOT_FOUND,message);
		}
	}
	public static final class UPDATE_FAILED extends MmgRestException {
		private static final long serialVersionUID = 1L;

		public UPDATE_FAILED(String message) {
			super(1007, HttpStatus.SC_NOT_FOUND,message);
		}
	}
	public static final class DELETE_FAILED extends MmgRestException {
		private static final long serialVersionUID = 1L;

		public DELETE_FAILED(String message) {
			super(1008, HttpStatus.SC_NOT_FOUND,message);
		}
	}

	public static final class NOT_FOUND extends MmgRestException {
		private static final long serialVersionUID = 1L;

		public NOT_FOUND(String message) {
			super(1009, HttpStatus.SC_NOT_FOUND, message);
		}
	}
	
	public static final class COUNT_NOT_FOUND extends MmgRestException {
		private static final long serialVersionUID = 1L;

		public COUNT_NOT_FOUND(String message) {
			super(1010, HttpStatus.SC_NOT_FOUND, message);
		}
	}
	/**
	 * Profile Exceptions : The Error codes can be used to handle in App or website
	 * Profile Error code range - 3001 to 3200
	 */
	public static final class PROFILE_NOT_FOUND extends MmgRestException {
		private static final long serialVersionUID = 1L;

		public PROFILE_NOT_FOUND(int id) {
			super(3001, HttpStatus.SC_NOT_FOUND, String.format("Profile not found, id: [%d]", id));
		}
		public PROFILE_NOT_FOUND() {
			super(3002, HttpStatus.SC_NOT_FOUND, String.format("Profile not found"));
		}
	}
	
	public static final class FILE_UPLOAD_FAILED extends MmgRestException {
		private static final long serialVersionUID = 1L;

		public FILE_UPLOAD_FAILED() {
			super(3302, HttpStatus.SC_NOT_FOUND, String.format("Failed to upload image. Please try later.."));
		}
		public FILE_UPLOAD_FAILED(String msg) {
			super(3303, HttpStatus.SC_NOT_FOUND, String.format(msg));
		}
	}
	
	public static final class FRANCHISE_NOT_FOUND extends MmgRestException {
		private static final long serialVersionUID = 1L;

		public FRANCHISE_NOT_FOUND(String id) {
			super(3003, HttpStatus.SC_NOT_FOUND, String.format("Franchise not found, id: [%s]", id));
		}
		public FRANCHISE_NOT_FOUND() {
			super(3004, HttpStatus.SC_NOT_FOUND, String.format("Franchise not found"));
		}
	}

	public static final class FIRSTNAME_PATTERN_NOT_MATCH extends MmgRestException {
		private static final long serialVersionUID = 1L;

		public FIRSTNAME_PATTERN_NOT_MATCH() {
			super(3005, HttpStatus.SC_NOT_FOUND, String.format("Please specify a Valid First name"));
		}
	}

	public static final class EMAILID_PATTERN_NOT_MATCH extends MmgRestException {
		private static final long serialVersionUID = 1L;

		public EMAILID_PATTERN_NOT_MATCH() {
			super(3006, HttpStatus.SC_NOT_FOUND, String.format("Please specify a Valid EmailId"));
		}
	}
	
	public static final class GST_PATTERN_NOT_MATCH extends MmgRestException {
		private static final long serialVersionUID = 1L;

		public GST_PATTERN_NOT_MATCH() {
			super(3006, HttpStatus.SC_NOT_FOUND, String.format("Please specify a Valid GST"));
		}
	}
	
	public static final class PAN_NUMBER_ALREADY_EXIST extends MmgRestException {
		private static final long serialVersionUID = 1L;

		public PAN_NUMBER_ALREADY_EXIST() {
			super(3007, HttpStatus.SC_NOT_FOUND,
					String.format("User already exist with PanNumber"));
		}
	}

	public static final class ACCOUNT_NUMBER_ALREADY_EXIST extends MmgRestException {
		private static final long serialVersionUID = 1L;

		public ACCOUNT_NUMBER_ALREADY_EXIST() {
			super(3008, HttpStatus.SC_NOT_FOUND,
					String.format("User already exist with Account Number"));
		}
	}
	
	public static final class AADHAR_NUMBER_ALREADY_EXIST extends MmgRestException {
		private static final long serialVersionUID = 1L; 
		
	    public AADHAR_NUMBER_ALREADY_EXIST() {
		     super(3009, HttpStatus.SC_NOT_FOUND,
				  String.format("User already exist with aadhar Number"));
	     }
	}
	public static final class PAN_PATTERN_NOT_MATCH extends MmgRestException {
		private static final long serialVersionUID = 1L;

		public PAN_PATTERN_NOT_MATCH() {
			super(3010, HttpStatus.SC_NOT_FOUND, String.format("Please specify the valid pan number"));
		}
	}
	/**
	 * Quotation Exceptions : The Error codes can be used to handle in App or website
	 * Quotation Error code range - 3201 to 3300
	 */
	public static final class QUOTATION_NOT_FOUND extends MmgRestException {
		private static final long serialVersionUID = 1L;

		public QUOTATION_NOT_FOUND(String id) {
			super(3201, HttpStatus.SC_NOT_FOUND, String.format("Quotation not found, id: [%d]", id));
		}
		public QUOTATION_NOT_FOUND() {
			super(3202, HttpStatus.SC_NOT_FOUND, String.format("Quotation not found"));
		}
	}
	/**
	/**
	 * Document and Image Upload Exceptions : The Error codes can be used to handle in App or website
	 * Document and Image upload Request Error code range - 3301 to 4000
	 */
	public static final class EMPTY_FILE extends MmgRestException {
		private static final long serialVersionUID = 1L;

		public EMPTY_FILE(String msg) {
			super(3301, HttpStatus.SC_NO_CONTENT, String.format(msg));
		}
	}
//	public static final class FILE_UPLOAD_FAILED extends MmgRestException {
//		private static final long serialVersionUID = 1L;
//
//		public FILE_UPLOAD_FAILED() {
//			super(3302, HttpStatus.SC_NOT_FOUND, String.format("Failed to upload image. Please try later.."));
//		}
//		public FILE_UPLOAD_FAILED(String msg) {
//			super(3303, HttpStatus.SC_NOT_FOUND, String.format(msg));
//		}
//	}
	
	
	public static final class COMPANY_NAME_ALREADY_EXIST extends MmgRestException {
		private static final long serialVersionUID = 1L; 
		
	    public COMPANY_NAME_ALREADY_EXIST() {
		     super(3009, HttpStatus.SC_NOT_FOUND,
				  String.format("company already  already exist with name"));
	     }
	}
	
	public static final class DETAILS_ALREADY_EXIST extends MmgRestException {
		private static final long serialVersionUID = 1L; 
		
	    public DETAILS_ALREADY_EXIST(String error) {
		     super(3009, HttpStatus.SC_NOT_FOUND,
				  String.format(error));
	     }
	}
	
	public static final class MOBILENUMBER_PATTERN_NOT_MATCH extends MmgRestException {
		private static final long serialVersionUID = 1L;

		public MOBILENUMBER_PATTERN_NOT_MATCH() {
			super(3005, HttpStatus.SC_NOT_FOUND, String.format("Please specify a Valid Phone number"));
		}
	}

}
