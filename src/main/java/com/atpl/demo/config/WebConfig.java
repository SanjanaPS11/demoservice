package com.atpl.demo.config;
import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.commons.CommonsMultipartResolver;
   
@Component
@Order(Ordered.HIGHEST_PRECEDENCE)
@ComponentScan(basePackages = {"com.atpl.demo"})
public class WebConfig implements Filter{
	 @Override
	    public void doFilter(ServletRequest req, ServletResponse res, FilterChain chain) throws IOException, ServletException {
	        final HttpServletResponse response = (HttpServletResponse) res;
	       
	        System.out.println("Access "+response);
	      // req.getMultipartFormBoundary();
	        response.setHeader("Access-Control-Allow-Origin", "*");
	       
	        response.setHeader("Access-Control-Allow-Methods", "POST, PUT,PATCH, GET, OPTIONS, DELETE");
	        //response.setHeader("Access-Control-Allow-Headers", "Access-Control-Allow-Headers, Origin,Accept, X-Requested-With, Content-Type, Access-Control-Request-Method, Access-Control-Request-Headers");
	        response.setHeader("Access-Control-Allow-Headers","response-type,boundary, Authorization,Access-Control-Allow-Headers,access-control-allow-origin,Content-Type,X-API-KEY,Access-Control-Allow-Methods");
	        response.setHeader("Access-Control-Max-Age", "3600");
	       // response.setHeader("Accept",MediaType.MULTIPART_FORM_DATA_VALUE);
	   
	       response.setHeader("Content-Type",MediaType.APPLICATION_JSON_VALUE);
	      // multipartResolver();
	        if ("OPTIONS".equalsIgnoreCase(((HttpServletRequest) req).getMethod())) {
	            response.setStatus(HttpServletResponse.SC_OK);
	        } else {
	            chain.doFilter(req, res);
	        }
	    }
//	@Bean
	public CommonsMultipartResolver multipartResolver() {
	   // CommonsMultipartResolver resolver = new CommonsMultipartResolver();
	    org.springframework.web.multipart.commons.CommonsMultipartResolver Resolver = new org.springframework.web.multipart.commons.CommonsMultipartResolver();
	   // resolver.setResolveLazily(false);
	   // resolver.setDefaultEncoding("utf-8");
	    return Resolver;
	}
	 
	    @Override
	    public void destroy() {
	    }
	 
	    @Override
	    public void init(FilterConfig config) throws ServletException {
	    }
    }
	
	
//@Bean
//public CommonsMultipartResolver multipartResolver() {
//    CommonsMultipartResolver resolver = new CommonsMultipartResolver();
//    resolver.setResolveLazily(false);
//    resolver.setDefaultEncoding("utf-8");
//    return resolver;
//}
	
//public MultipartResolver multipartResolver() {
//    org.springframework.web.multipart.commons.CommonsMultipartResolver multipartResolver = new org.springframework.web.multipart.commons.CommonsMultipartResolver();
//    multipartResolver.setMaxUploadSize(1000000);
//    return multipartResolver;
//}



//@Configuration
//@ComponentScan(basePackageClasses = AppConfig.class, useDefaultFilters = false, includeFilters = {
//      @Filter(org.springframework.stereotype.Controller.class) })
//@EnableWebMvc
//public class WebConfig extends WebMvcConfigurerAdapter{
//	@Override
//    public void addCorsMappings(CorsRegistry registry) {
//        registry.addMapping("/*").allowedOrigins("*").allowedMethods("GET", "POST", "OPTIONS", "PUT")
//                .allowedHeaders("Content-Type", "X-Requested-With", "accept", "Origin", "Access-Control-Request-Method",
//                        "Access-Control-Request-Headers")
//                .exposedHeaders("Access-Control-Allow-Origin", "Access-Control-Allow-Credentials")
//                .allowCredentials(true).maxAge(3600);
//    }