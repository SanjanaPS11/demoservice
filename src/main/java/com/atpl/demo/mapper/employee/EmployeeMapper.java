package com.atpl.demo.mapper.employee;
import org.springframework.stereotype.Component;

import com.atpl.demo.domain.employee.EmployeeDomain;
import com.atpl.demo.mapper.AbstractModelMapper;
import com.atpl.demo.model.employee.EmployeeModel;


@Component
public class EmployeeMapper extends AbstractModelMapper<EmployeeModel ,EmployeeDomain>{

	@Override
	public Class<EmployeeModel> entityType() {
		return EmployeeModel.class;
	}

	@Override
	public Class<EmployeeDomain> modelType() {
		return EmployeeDomain.class;
	}
}
