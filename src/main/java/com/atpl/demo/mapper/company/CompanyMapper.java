package com.atpl.demo.mapper.company;

import org.springframework.stereotype.Component;

import com.atpl.demo.domain.company.CompanyDomain;
import com.atpl.demo.mapper.AbstractModelMapper;
import com.atpl.demo.model.company.CompanyModel;

@Component
public class CompanyMapper extends AbstractModelMapper<CompanyModel,CompanyDomain>{
	
	@Override
	public Class<CompanyModel> entityType() {
		return CompanyModel.class;
	}

	@Override
	public Class<CompanyDomain> modelType() {
		return CompanyDomain.class;
	}

}
