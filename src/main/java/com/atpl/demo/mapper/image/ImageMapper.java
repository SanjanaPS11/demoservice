package com.atpl.demo.mapper.image;
import org.springframework.stereotype.Component;

import com.atpl.demo.domain.employee.EmployeeDomain;
import com.atpl.demo.domain.image.ImageDomain;
import com.atpl.demo.mapper.AbstractModelMapper;
import com.atpl.demo.model.employee.EmployeeModel;
import com.atpl.demo.model.image.ImageModel;


@Component
public class ImageMapper extends AbstractModelMapper<ImageModel ,ImageDomain>{
	@Override
	public Class<ImageModel> entityType() {
		return ImageModel.class;
	}

	@Override
	public Class<ImageDomain> modelType() {
		return ImageDomain.class;
	}

}
