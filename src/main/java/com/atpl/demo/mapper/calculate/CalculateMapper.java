package com.atpl.demo.mapper.calculate;

import org.springframework.stereotype.Component;
import com.atpl.demo.domain.calculate.CalculateDomain;
import com.atpl.demo.mapper.AbstractModelMapper;
import com.atpl.demo.model.calculate.CalculateModel;

@Component
public class CalculateMapper extends AbstractModelMapper<CalculateModel ,CalculateDomain>{

	@Override
	public Class<CalculateModel> entityType() {
		return CalculateModel.class;
	}

	@Override
	public Class<CalculateDomain> modelType() {
		return CalculateDomain.class;
	}
}
