package com.atpl.demo.mapper.registration;

import org.springframework.stereotype.Component;

import com.atpl.demo.domain.registration.RegistrationDomain;
import com.atpl.demo.mapper.AbstractModelMapper;
import com.atpl.demo.model.registration.RegistrationModel;

@Component
public class RegistrationMapper extends AbstractModelMapper<RegistrationModel, RegistrationDomain> {

	@Override
	public Class<RegistrationModel> entityType() {
		return RegistrationModel.class;
	}

	@Override
	public Class<RegistrationDomain> modelType() {
		return RegistrationDomain.class;
	}

}