package com.atpl.demo.mapper.studentLogin;

import org.springframework.stereotype.Component;

import com.atpl.demo.domain.studentLogin.StudentLoginDomain;
import com.atpl.demo.mapper.AbstractModelMapper;
import com.atpl.demo.model.studentLogin.StudentLoginModel;

@Component
public class StudentLoginMapper  extends AbstractModelMapper<StudentLoginModel ,StudentLoginDomain>{

	@Override
	public Class<StudentLoginModel> entityType() {
		return StudentLoginModel.class;
	}

	@Override
	public Class<StudentLoginDomain> modelType() {
		return StudentLoginDomain.class;
	}
}
