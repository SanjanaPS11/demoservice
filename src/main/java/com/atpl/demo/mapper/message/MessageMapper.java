package com.atpl.demo.mapper.message;
import org.springframework.stereotype.Component;
import com.atpl.demo.domain.message.MessageDomain;
import com.atpl.demo.mapper.AbstractModelMapper;
import com.atpl.demo.model.message.MessageModel;


@Component
public class MessageMapper extends AbstractModelMapper<MessageModel ,MessageDomain>{
	@Override
	public Class<MessageModel> entityType() {
		return MessageModel.class;
	}

	@Override
	public Class<MessageDomain> modelType() {
		return MessageDomain.class;
	}

}
