package com.atpl.demo.mapper.driver;

import org.springframework.stereotype.Component;

import com.atpl.demo.domain.driver.DriverDomain;
import com.atpl.demo.mapper.AbstractModelMapper;
import com.atpl.demo.model.driver.DriverModel;


@Component
public class DriverMapper extends AbstractModelMapper<DriverModel ,DriverDomain>{
	
	@Override
	public Class<DriverModel> entityType() {
		return DriverModel.class;
	}

	@Override
	public Class<DriverDomain> modelType() {
		return DriverDomain.class;
	}
}
