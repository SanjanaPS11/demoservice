package com.atpl.demo.mapper.vehicle;
import org.springframework.stereotype.Component;

import com.atpl.demo.domain.vehicle.VehicleDomain;
import com.atpl.demo.mapper.AbstractModelMapper;
import com.atpl.demo.model.vehicle.VehicleModel;


@Component
public class VehicleMapper extends AbstractModelMapper<VehicleModel ,VehicleDomain>{
	
	@Override
	public Class<VehicleModel> entityType() {
		return VehicleModel.class;
	}

	@Override
	public Class<VehicleDomain> modelType() {
		return VehicleDomain.class;
	}
}
