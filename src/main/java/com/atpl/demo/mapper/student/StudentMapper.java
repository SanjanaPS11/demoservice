package com.atpl.demo.mapper.student;
import org.springframework.stereotype.Component;

import com.atpl.demo.domain.student.StudentDomain;
import com.atpl.demo.mapper.AbstractModelMapper;
import com.atpl.demo.model.student.StudentModel;

@Component
public class StudentMapper extends AbstractModelMapper<StudentModel ,StudentDomain>{

		@Override
		public Class<StudentModel> entityType() {
			return StudentModel.class;
		}

		@Override
		public Class<StudentDomain> modelType() {
			return StudentDomain.class;
		}
	}

