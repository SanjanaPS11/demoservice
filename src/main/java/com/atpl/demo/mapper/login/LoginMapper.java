package com.atpl.demo.mapper.login;

import org.springframework.stereotype.Component;

import com.atpl.demo.domain.login.LoginDomain;
import com.atpl.demo.mapper.AbstractModelMapper;
import com.atpl.demo.model.login.LoginModel;


@Component
public class LoginMapper extends AbstractModelMapper<LoginModel ,LoginDomain> {

	
	@Override
	public Class<LoginModel> entityType() {
		return LoginModel.class;
	}

	@Override
	public Class<LoginDomain> modelType() {
		return LoginDomain.class;
	}

}
