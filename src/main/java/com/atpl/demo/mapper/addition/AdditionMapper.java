package com.atpl.demo.mapper.addition;

import org.springframework.stereotype.Component;

import com.atpl.demo.domain.addition.AdditionDomain;
import com.atpl.demo.mapper.AbstractModelMapper;
import com.atpl.demo.model.addition.AdditionModel;

@Component
public class AdditionMapper extends AbstractModelMapper<AdditionModel ,AdditionDomain>{

		@Override
		public Class<AdditionModel> entityType() {
			return AdditionModel.class;
		}

		@Override
		public Class<AdditionDomain> modelType() {
			return AdditionDomain.class;
		}
	}
