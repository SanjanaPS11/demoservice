package com.atpl.demo.mapper.boarding;
import org.springframework.stereotype.Component;

import com.atpl.demo.domain.boarding.BoardingDomain;
import com.atpl.demo.mapper.AbstractModelMapper;
import com.atpl.demo.model.boarding.BoardingModel;


@Component
public class BoardingMapper extends AbstractModelMapper<BoardingModel ,BoardingDomain>{

	@Override
	public Class<BoardingModel> entityType() {
		return BoardingModel.class;
	}

	@Override
	public Class<BoardingDomain> modelType() {
		return BoardingDomain.class;
	}
}
