package com.atpl.demo.mapper.labour;
import org.springframework.stereotype.Component;

import com.atpl.demo.domain.labour.LabourDomain;
import com.atpl.demo.mapper.AbstractModelMapper;
import com.atpl.demo.model.labour.LabourModel;


@Component
public class LabourMapper extends AbstractModelMapper<LabourModel ,LabourDomain>{

	@Override
	public Class<LabourModel> entityType() {
		return LabourModel.class;
	}

	@Override
	public Class<LabourDomain> modelType() {
		return LabourDomain.class;
	}
}
