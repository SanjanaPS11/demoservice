package com.atpl.demo.common;
/**
 * @author Raghu M
 *
 */
public enum Status {

    FAILURE,
    SUCCESS;
}
