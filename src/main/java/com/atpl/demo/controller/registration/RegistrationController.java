package com.atpl.demo.controller.registration;

import static com.atpl.demo.exception.HttpResponseUtils.prepareSuccessResponse;

import javax.ws.rs.core.MediaType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.atpl.demo.constant.Constants;
import com.atpl.demo.exception.GenericRes;
import com.atpl.demo.model.registration.RegistrationModel;
import com.atpl.demo.service.registration.RegistrationService;

@RestController
@RequestMapping("/v1")
public class RegistrationController implements Constants {

	private static final Logger logger = LoggerFactory.getLogger(RegistrationController.class);

	@Autowired
	RegistrationService registrationService;

	@RequestMapping(value = "/registration", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON, produces = MediaType.APPLICATION_JSON)
	@ResponseBody
	public ResponseEntity<GenericRes> saveRegistration(@RequestBody RegistrationModel registrationModel) throws Exception {
		return prepareSuccessResponse(registrationService.saveRegistration(registrationModel));
	}

	@RequestMapping(value = "/registration", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON)
	@ResponseBody
	public ResponseEntity<GenericRes> getRegistration() throws Exception {
		return prepareSuccessResponse(registrationService.getRegistration());
	}

	@RequestMapping(value = "/registration", method = RequestMethod.PUT, consumes = MediaType.APPLICATION_JSON, produces = MediaType.APPLICATION_JSON)
	@ResponseBody
	public ResponseEntity<GenericRes> updateRegistration(@RequestBody RegistrationModel registrationModel) throws Exception {
		return prepareSuccessResponse(registrationService.updateRegistration(registrationModel));
	}

	@RequestMapping(value = "/registration/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON)
	@ResponseBody
	public ResponseEntity<GenericRes> getRegistrationById(@PathVariable("id") int id) throws Exception {
		return prepareSuccessResponse(registrationService.getRegistration(id));
	}

	@RequestMapping(value = "/registration/{id}", method = RequestMethod.DELETE, consumes = MediaType.APPLICATION_JSON, produces = MediaType.APPLICATION_JSON)
	@ResponseBody
	public ResponseEntity<GenericRes> deleteRegistration(@PathVariable("id") int id) throws Exception {
		return prepareSuccessResponse(registrationService.deleteRegistration(id));
	}

}
