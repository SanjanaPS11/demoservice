package com.atpl.demo.controller.image;
import org.springframework.web.bind.annotation.PostMapping;
import static com.atpl.demo.exception.HttpResponseUtils.prepareSuccessResponse;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import javax.ws.rs.core.MediaType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.atpl.demo.constant.Constants;
import com.atpl.demo.controller.company.CompanyController;
import com.atpl.demo.exception.GenericRes;

import com.atpl.demo.service.image.ImageService;

import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.bind.annotation.RequestParam;



@RestController
@RequestMapping("/v1")
public class ImageController implements Constants{
private static final Logger logger = LoggerFactory.getLogger(CompanyController.class);
	
	@Autowired
	ImageService imageService;

	@RequestMapping(value = "image/uploadimage", method = RequestMethod.POST,consumes= MediaType.MULTIPART_FORM_DATA, produces = MediaType.APPLICATION_JSON)
	@ResponseBody
	  public ResponseEntity<GenericRes> uploadFile(@RequestParam("image") MultipartFile image) throws Exception {
		return prepareSuccessResponse(imageService.uploadImage(image));
	}
	
	@RequestMapping(value = "image/downloadimage/{uuid}", method = RequestMethod.POST)
	@ResponseBody
	public ResponseEntity<GenericRes> downloadImage(@RequestParam("image") String path,@PathVariable("uuid") String uuId)throws Exception {
		return prepareSuccessResponse(imageService.downloadImage(path, uuId));
	}
	
	@RequestMapping(value = "image/downloadimage", method = RequestMethod.POST)
	@ResponseBody
	public ResponseEntity<GenericRes> downloadImage1()throws Exception {
		return prepareSuccessResponse(imageService.downloadImage1());
	}
	
	@RequestMapping(value = "image/getImage", method = RequestMethod.GET)
	@ResponseBody
	public ResponseEntity<GenericRes> getImage()throws Exception {
		return prepareSuccessResponse(imageService.getImage());
	}

}
