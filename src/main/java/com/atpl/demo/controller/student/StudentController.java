package com.atpl.demo.controller.student;
import static com.atpl.demo.exception.HttpResponseUtils.prepareSuccessResponse;

import javax.ws.rs.core.MediaType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.atpl.demo.constant.Constants;
import com.atpl.demo.controller.employee.EmployeeController;
import com.atpl.demo.exception.GenericRes;
import com.atpl.demo.model.employee.EmployeeModel;
import com.atpl.demo.model.student.StudentModel;
import com.atpl.demo.service.employee.EmployeeService;
import com.atpl.demo.service.student.StudentService;

@RestController
@RequestMapping("/v1")
public class StudentController implements Constants{
	private static final Logger logger = LoggerFactory.getLogger(StudentController.class);

	@Autowired
	StudentService studentService;
	
	@RequestMapping(value = "/student", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON, produces = MediaType.APPLICATION_JSON)
	@ResponseBody
	public ResponseEntity<GenericRes> saveStudent(@RequestBody StudentModel studentModel) throws Exception {
		return prepareSuccessResponse(studentService.saveStudent(studentModel));
	}

	@RequestMapping(value = "/student", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON)
	@ResponseBody
	public ResponseEntity<GenericRes> getStudent() throws Exception {
		return prepareSuccessResponse(studentService.getStudent());
	}

	@RequestMapping(value = "/student", method = RequestMethod.PUT, consumes = MediaType.APPLICATION_JSON, produces = MediaType.APPLICATION_JSON)
	@ResponseBody
	public ResponseEntity<GenericRes> updateStudent(@RequestBody StudentModel studentModel) throws Exception {
		return prepareSuccessResponse(studentService.updateStudent(studentModel));
	}

	@RequestMapping(value = "/student/{uuId}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON)
	@ResponseBody
	public ResponseEntity<GenericRes> getStudentById(@PathVariable("uuId") String uuId) throws Exception {
		return prepareSuccessResponse(studentService.getStudentById(uuId));
	}

	@RequestMapping(value = "/student/{uuId}", method = RequestMethod.DELETE, consumes = MediaType.APPLICATION_JSON, produces = MediaType.APPLICATION_JSON)
	@ResponseBody
	public ResponseEntity<GenericRes> deleteStudent(@PathVariable("uuId") String uuId) throws Exception {
		return prepareSuccessResponse(studentService.deleteStudent(uuId));
	}

}
