package com.atpl.demo.controller.vehicle;

import static com.atpl.demo.exception.HttpResponseUtils.prepareSuccessResponse;

import javax.validation.Valid;
import javax.ws.rs.core.MediaType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.atpl.demo.constant.Constants;
import com.atpl.demo.controller.vehicle.VehicleController;
import com.atpl.demo.exception.GenericRes;
import com.atpl.demo.model.vehicle.VehicleModel;
import com.atpl.demo.service.vehicle.VehicleService;


@RestController
@RequestMapping("/v2")
public class VehicleController implements Constants{

private static final Logger logger = LoggerFactory.getLogger(VehicleController.class);
	
	@Autowired
	VehicleService vehicleService;


	@RequestMapping(value = "/dashboard/vehicle/requestedBy/{requestedBy}/status/{status}&stateId={stateId}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON)
	@ResponseBody
	public ResponseEntity<GenericRes> getVehicleCount(@PathVariable("requestedBy") String requestedBy,@PathVariable("status") String status,@PathVariable("stateId") Integer stateId) throws Exception {
		return prepareSuccessResponse(vehicleService.getVehicleCount(requestedBy,status,stateId));
	}
}
