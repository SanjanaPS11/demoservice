package com.atpl.demo.controller.driver;

import static com.atpl.demo.exception.HttpResponseUtils.prepareSuccessResponse;

import javax.validation.Valid;
import javax.ws.rs.core.MediaType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.atpl.demo.constant.Constants;
import com.atpl.demo.controller.driver.DriverController;
import com.atpl.demo.exception.GenericRes;
import com.atpl.demo.model.driver.DriverModel;
import com.atpl.demo.service.driver.DriverService;



@RestController
@RequestMapping("/v2")
public class DriverController implements Constants{

private static final Logger logger = LoggerFactory.getLogger(DriverController.class);
	
	@Autowired
	DriverService driverService;

	@RequestMapping(value = "/driver", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON)
	
	public ResponseEntity<GenericRes> saveCompany(@RequestBody DriverModel driverModel) throws Exception {
		return prepareSuccessResponse(driverService.saveDriver(driverModel));
	}
	
//   @RequestMapping(value = "/driver", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON)
//	
//	public ResponseEntity<GenericRes> saveCompany(@RequestBody DriverModel driverModel, @Para("firstName") String firstName,@RequestParam("roleName") String roleName,@RequestParam("requestNumber") String requestNumber) throws Exception {
//		return prepareSuccessResponse(driverService.saveDriver(driverModel,firstName,roleName,requestNumber));
//	}
	
	
	
	
	
	
	
	

//	@RequestMapping(value = "/dashboard/driver/requestedBy/{requestedBy}/status/{status}&stateId={stateId}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON)
	//@RequestMapping(value = "/dashboard/driver/{requestedBy}/{status}/{stateId}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON)
	
//	@ResponseBody
//	public ResponseEntity<GenericRes> getDriverCount(@PathVariable("requestedBy") String requestedBy,@PathVariable("status") String status,@PathVariable("stateId") Integer stateId) throws Exception {
//		return prepareSuccessResponse(driverService.getDriverCount(requestedBy,status,stateId));
//	}
}
