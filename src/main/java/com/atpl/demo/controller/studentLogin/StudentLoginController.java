package com.atpl.demo.controller.studentLogin;
import static com.atpl.demo.exception.HttpResponseUtils.prepareSuccessResponse;

import javax.ws.rs.core.MediaType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.atpl.demo.constant.Constants;
import com.atpl.demo.controller.employee.EmployeeController;
import com.atpl.demo.exception.GenericRes;
import com.atpl.demo.model.employee.EmployeeModel;
import com.atpl.demo.model.login.LoginModel;
import com.atpl.demo.model.student.StudentModel;
import com.atpl.demo.model.studentLogin.StudentLoginModel;
import com.atpl.demo.service.employee.EmployeeService;
import com.atpl.demo.service.student.StudentService;
import com.atpl.demo.service.studentLogin.StudentLoginService;

@RestController
@RequestMapping("/v1")
public class StudentLoginController  implements Constants{
	
	private static final Logger logger = LoggerFactory.getLogger(StudentLoginController.class);

	@Autowired
	StudentLoginService studentLoginService;
	
	@RequestMapping(value = "/studentLogin", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON, produces = MediaType.APPLICATION_JSON)
	@ResponseBody
	public ResponseEntity<GenericRes> saveStudent(@RequestBody StudentLoginModel studentLoginModel) throws Exception {
		return prepareSuccessResponse(studentLoginService.saveStudentLogin(studentLoginModel));
	}

	@RequestMapping(value = "/Studentlogin", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON, produces = MediaType.APPLICATION_JSON)
	@ResponseBody
	public ResponseEntity<GenericRes> login(@RequestBody StudentLoginModel studentLoginModel) throws Exception {
		return prepareSuccessResponse(studentLoginService.login(studentLoginModel));
	}
	
	@RequestMapping(value = "/studentLogin", method = RequestMethod.PUT, consumes = MediaType.APPLICATION_JSON, produces = MediaType.APPLICATION_JSON)
	@ResponseBody
	public ResponseEntity<GenericRes> updateStudent(@RequestBody StudentLoginModel studentLoginModel) throws Exception {
		return prepareSuccessResponse(studentLoginService.updateStudentLogin(studentLoginModel));
	}


}
