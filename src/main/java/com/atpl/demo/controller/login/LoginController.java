package com.atpl.demo.controller.login;
import static com.atpl.demo.exception.HttpResponseUtils.prepareSuccessResponse;

import javax.ws.rs.core.MediaType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.atpl.demo.constant.Constants;
import com.atpl.demo.controller.company.CompanyController;
import com.atpl.demo.exception.GenericRes;
import com.atpl.demo.model.login.LoginModel;
import com.atpl.demo.service.login.LoginService;

@RestController
@RequestMapping("/v1")
public class LoginController implements Constants{
	
	private static final Logger logger = LoggerFactory.getLogger(CompanyController.class);

	@Autowired
	LoginService loginService;

	@RequestMapping(value = "/login", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON, produces = MediaType.APPLICATION_JSON)
	@ResponseBody
	public ResponseEntity<GenericRes> Login(@RequestBody LoginModel loginModel) throws Exception {
		return prepareSuccessResponse(loginService.login(loginModel));
	}

}
