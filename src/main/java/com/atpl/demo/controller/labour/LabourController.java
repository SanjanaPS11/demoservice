package com.atpl.demo.controller.labour;

import static com.atpl.demo.exception.HttpResponseUtils.prepareSuccessResponse;

import javax.validation.Valid;
import javax.ws.rs.core.MediaType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.atpl.demo.constant.Constants;
import com.atpl.demo.controller.labour.LabourController;
import com.atpl.demo.exception.GenericRes;
import com.atpl.demo.model.labour.LabourModel;
import com.atpl.demo.service.labour.LabourService;



@RestController
@RequestMapping("/v2")
public class LabourController implements Constants{
	
	private static final Logger logger = LoggerFactory.getLogger(LabourController.class);

	@Autowired
	LabourService labourService;


	@RequestMapping(value = "/dashboard/labour/requestedBy/{requestedBy}/status/{status}&stateId={stateId}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON)
	
	@ResponseBody
	public ResponseEntity<GenericRes> getLabourCount(@PathVariable("requestedBy") String requestedBy,@PathVariable("status") String status,@PathVariable("stateId") Integer stateId) throws Exception {
		return prepareSuccessResponse(labourService.getLabourCount(requestedBy,status,stateId));
	}
}
