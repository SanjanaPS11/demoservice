package com.atpl.demo.controller.calculate;

import static com.atpl.demo.exception.HttpResponseUtils.prepareSuccessResponse;

import javax.ws.rs.core.MediaType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.atpl.demo.constant.Constants;
import com.atpl.demo.controller.calculate.CalculateController;
import com.atpl.demo.exception.GenericRes;
import com.atpl.demo.model.calculate.CalculateModel;
import com.atpl.demo.service.calculate.CalculateService;

@RestController
@RequestMapping("/v1")
public class CalculateController implements Constants{
	
	private static final Logger logger = LoggerFactory.getLogger(CalculateController.class);

	@Autowired
	CalculateService calculateService;
	
	@RequestMapping(value = "/arraylist", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON, produces = MediaType.APPLICATION_JSON)
	@ResponseBody
	public ResponseEntity<GenericRes> saveList(@RequestBody CalculateModel calculateModel) throws Exception {
		return prepareSuccessResponse(calculateService.saveList(calculateModel));
	}
	
//	@RequestMapping(value = "/arraylist", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON)
//	@ResponseBody
//	public ResponseEntity<GenericRes> getList() throws Exception {
//		return prepareSuccessResponse(calculateService.getList());
//	}
	
}
