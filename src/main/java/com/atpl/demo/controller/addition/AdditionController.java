package com.atpl.demo.controller.addition;

import static com.atpl.demo.exception.HttpResponseUtils.prepareSuccessResponse;

import javax.ws.rs.core.MediaType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.atpl.demo.constant.Constants;
import com.atpl.demo.controller.addition.AdditionController;
import com.atpl.demo.exception.GenericRes;
import com.atpl.demo.model.addition.AdditionModel;
import com.atpl.demo.service.addition.AdditionService;


@RestController
@RequestMapping("/v1")
public class AdditionController implements Constants{
	
	private static final Logger logger = LoggerFactory.getLogger(AdditionController.class);

	@Autowired
	AdditionService additionService;
	
	@RequestMapping(value = "/addition", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON, produces = MediaType.APPLICATION_JSON)
	@ResponseBody
	public ResponseEntity<GenericRes> saveAddition(@RequestBody AdditionModel additionModel) throws Exception {
		return prepareSuccessResponse(additionService.saveAddition(additionModel));
	}
	
	@RequestMapping(value = "/addition/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON)
	@ResponseBody
	public ResponseEntity<GenericRes> getAdditionById(@PathVariable("id") int id) throws Exception {
		return prepareSuccessResponse(additionService.getAddition(id));
	}


}
