package com.atpl.demo.controller.message;
import org.springframework.web.bind.annotation.PostMapping;
import static com.atpl.demo.exception.HttpResponseUtils.prepareSuccessResponse;
import org.springframework.web.bind.annotation.PostMapping;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import javax.ws.rs.core.MediaType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.atpl.demo.constant.Constants;
import com.atpl.demo.controller.company.CompanyController;
import com.atpl.demo.exception.GenericRes;
import com.atpl.demo.model.company.CompanyModel;
import com.atpl.demo.model.message.MessageModel;
import com.atpl.demo.model.registration.RegistrationModel;
import com.atpl.demo.service.company.CompanyService;
import com.atpl.demo.service.message.MessageService;

import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.bind.annotation.RequestParam;

import org.springframework.web.bind.annotation.PostMapping;

@RestController
@RequestMapping("/v1")
public class MessageController implements Constants{
	
	private static final Logger logger = LoggerFactory.getLogger(CompanyController.class);

	@Autowired
	MessageService messageService;

	@RequestMapping(value = "/message", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON, produces = MediaType.APPLICATION_JSON)
	@ResponseBody
	public ResponseEntity<GenericRes> saveCompany(@RequestBody MessageModel messageModel) throws Exception {
		return prepareSuccessResponse(messageService.saveMessage(messageModel));
	}
	
	@RequestMapping(value = "/message", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON)
	@ResponseBody
	public ResponseEntity<GenericRes> getRegistration() throws Exception {
		return prepareSuccessResponse(messageService.getMessage());
	}

	@RequestMapping(value = "/message", method = RequestMethod.PUT, consumes = MediaType.APPLICATION_JSON, produces = MediaType.APPLICATION_JSON)
	@ResponseBody
	public ResponseEntity<GenericRes> updateRegistration(@RequestBody MessageModel messageModel) throws Exception {
		return prepareSuccessResponse(messageService.updateMessage(messageModel));
	}

	@RequestMapping(value = "/message/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON)
	@ResponseBody
	public ResponseEntity<GenericRes> getMessageByuuId(@PathVariable("id") String id) throws Exception {
		return prepareSuccessResponse(messageService.getMessageByuuId(id));
	}
	
	@RequestMapping(value = "/message/{uuid}", method = RequestMethod.DELETE, produces = MediaType.APPLICATION_JSON)
	@ResponseBody
	public ResponseEntity<GenericRes> deleteMessage(@PathVariable("uuid") String uuid) throws Exception {
		return prepareSuccessResponse(messageService.deleteMessage(uuid));
	}

}
