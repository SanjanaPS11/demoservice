package com.atpl.demo.controller.employee;

import static com.atpl.demo.exception.HttpResponseUtils.prepareSuccessResponse;

import javax.ws.rs.core.MediaType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.atpl.demo.constant.Constants;
import com.atpl.demo.exception.GenericRes;
import com.atpl.demo.model.employee.EmployeeModel;
import com.atpl.demo.service.employee.EmployeeService;

@RestController
@RequestMapping("/v1")
public class EmployeeController implements Constants{
	
	private static final Logger logger = LoggerFactory.getLogger(EmployeeController.class);

	@Autowired
	EmployeeService employeeService;

	
	@RequestMapping(value = "/employee", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON, produces = MediaType.APPLICATION_JSON)
	@ResponseBody
	public ResponseEntity<GenericRes> saveEmployee(@RequestBody EmployeeModel employeeModel) throws Exception {
		return prepareSuccessResponse(employeeService.saveEmployee(employeeModel));
	}

	@RequestMapping(value = "/employee", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON)
	@ResponseBody
	public ResponseEntity<GenericRes> getEmployee() throws Exception {
		return prepareSuccessResponse(employeeService.getEmployee());
	}

	@RequestMapping(value = "/employee", method = RequestMethod.PUT, consumes = MediaType.APPLICATION_JSON, produces = MediaType.APPLICATION_JSON)
	@ResponseBody
	public ResponseEntity<GenericRes> updateEmployee(@RequestBody EmployeeModel employeeModel) throws Exception {
		return prepareSuccessResponse(employeeService.updateEmployee(employeeModel));
	}

	@RequestMapping(value = "/employee/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON)
	@ResponseBody
	public ResponseEntity<GenericRes> getEmployeeById(@PathVariable("id") int id) throws Exception {
		return prepareSuccessResponse(employeeService.getEmployee(id));
	}

	@RequestMapping(value = "/employee/{id}", method = RequestMethod.DELETE, consumes = MediaType.APPLICATION_JSON, produces = MediaType.APPLICATION_JSON)
	@ResponseBody
	public ResponseEntity<GenericRes> deleteEmployee(@PathVariable("id") int id) throws Exception {
		return prepareSuccessResponse(employeeService.deleteEmployee(id));
	}
}
