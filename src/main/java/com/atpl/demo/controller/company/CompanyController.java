package com.atpl.demo.controller.company;

import org.springframework.web.bind.annotation.PostMapping;
import static com.atpl.demo.exception.HttpResponseUtils.prepareSuccessResponse;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import javax.ws.rs.core.MediaType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.atpl.demo.constant.Constants;
import com.atpl.demo.exception.GenericRes;
import com.atpl.demo.model.company.CompanyModel;
import com.atpl.demo.service.company.CompanyService;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.bind.annotation.RequestParam;

import org.springframework.web.bind.annotation.PostMapping;

@RestController
//@CrossOrigin(origins.="*")
//@CrossOrigin(origins="http://localhost:4200")
@RequestMapping("/v1")

public class CompanyController implements Constants {

	private static final Logger logger = LoggerFactory.getLogger(CompanyController.class);

	@Autowired
	CompanyService companyService;

	@RequestMapping(value = "/company", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON, produces = MediaType.APPLICATION_JSON)
	@ResponseBody
	public ResponseEntity<GenericRes> saveCompany(@RequestBody CompanyModel companyModel) throws Exception {
		return prepareSuccessResponse(companyService.saveCompany(companyModel));
	}

	@RequestMapping(value = "/company", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON)
	@ResponseBody
	public ResponseEntity<GenericRes> getCompany() throws Exception {
		return prepareSuccessResponse(companyService.getCompany());
	}

	@RequestMapping(value = "/company/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON)
	@ResponseBody
	public ResponseEntity<GenericRes> getCompanyById(@PathVariable("id") String id) throws Exception {
		return prepareSuccessResponse(companyService.getCompany(id));
	}

	@RequestMapping(value = "/company/{uuId}", method = RequestMethod.PUT, consumes = MediaType.APPLICATION_JSON, produces = MediaType.APPLICATION_JSON)
	@ResponseBody
	public ResponseEntity<GenericRes> updateCompany(@PathVariable("uuId") String uuId,
			@RequestBody CompanyModel companyModel) throws Exception {
		return prepareSuccessResponse(companyService.updateCompany(companyModel, uuId));
	}

	@RequestMapping(value = "/company/{id}", method = RequestMethod.DELETE, consumes = MediaType.APPLICATION_JSON, produces = MediaType.APPLICATION_JSON)
	@ResponseBody
	public ResponseEntity<GenericRes> deleteCompany(@PathVariable("id") String id) throws Exception {
		return prepareSuccessResponse(companyService.deleteCompany(id));

	}

	@RequestMapping(value = "/company/isActive/{status}/uuId/{uuId}", method = RequestMethod.PATCH, consumes = MediaType.APPLICATION_JSON, produces = MediaType.APPLICATION_JSON)
	@ResponseBody
	public ResponseEntity<GenericRes> patchCompanyStatus(@PathVariable("status") Boolean status,
			@PathVariable("uuId") String uuId) throws Exception {
		return prepareSuccessResponse(companyService.patchCompanyStatus(status, uuId));
	}

	@RequestMapping(value = "/company", method = RequestMethod.PATCH, consumes = MediaType.APPLICATION_JSON, produces = MediaType.APPLICATION_JSON)
	@ResponseBody
	public ResponseEntity<GenericRes> patchCompany(@RequestBody CompanyModel companyModel) throws Exception {
		return prepareSuccessResponse(companyService.patchCompany(companyModel));
	}

	@RequestMapping(value = "/company/status/{status}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON)
	@ResponseBody
	public ResponseEntity<GenericRes> getCompanyStatus(@PathVariable("status") Boolean status) throws Exception {
		return prepareSuccessResponse(companyService.getCompanyStatus(status));
	}

	// Excel file import and Export
	@RequestMapping(value = "/uploadExcel", method = RequestMethod.POST, consumes = MediaType.MULTIPART_FORM_DATA, produces = MediaType.APPLICATION_JSON)
	@ResponseBody
	public ResponseEntity<GenericRes> uploadExcel(@RequestParam("file") MultipartFile file,
			MultipartHttpServletRequest req) throws Exception {
		return prepareSuccessResponse(companyService.uploadExcel(file));
	}

	@RequestMapping(value = "/downloadExcel", method = RequestMethod.POST)
	@ResponseBody
	public ResponseEntity<GenericRes> downloadExcel(@RequestParam("file") String path, HttpServletResponse response,
			HttpServletRequest req) throws Exception {
		return prepareSuccessResponse(companyService.downloadExcel(path));
	}

	// image file import and export
	@RequestMapping(value = "/image", method = RequestMethod.POST, consumes = MediaType.MULTIPART_FORM_DATA, produces = MediaType.APPLICATION_JSON)
	@ResponseBody
	public ResponseEntity<GenericRes> uploadFile(@RequestParam("image") MultipartFile image) throws Exception {
		return prepareSuccessResponse(companyService.uploadImage(image));
	}

	@RequestMapping(value = "/downloadimage/{uuid}", method = RequestMethod.POST)
	@ResponseBody
	public ResponseEntity<GenericRes> downloadImage(@RequestParam("image") String path,
			@PathVariable("uuid") String uuId) throws Exception {
		return prepareSuccessResponse(companyService.downloadImage(path, uuId));
	}

	// Seaching for company details
	@RequestMapping(value = "/companySearch/{key}/{value}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON)
	@ResponseBody
	public ResponseEntity<GenericRes> companySearch(@PathVariable("key") String key,
			@PathVariable("value") String value) throws Exception {
		return prepareSuccessResponse(companyService.companySearch(key, value));
	}
	
	
	@RequestMapping(value = "/Search/{value}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON)
	@ResponseBody
	public ResponseEntity<GenericRes> search(@PathVariable("value") String value) throws Exception {
		return prepareSuccessResponse(companyService.search(value));
	}

	
	// Pagination for company details
	@RequestMapping(value = "/company/pageSize/{pageSize}/pageNo/{pageNo}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON)
	@ResponseBody
	public ResponseEntity<GenericRes> patchCompanyStatus(@PathVariable("pageSize") int pageSize,
			@PathVariable("pageNo") int pageNo) throws Exception {
		return prepareSuccessResponse(companyService.companyPagination(pageSize, pageNo));
	}

}
