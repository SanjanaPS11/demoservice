package com.atpl.demo.controller.boarding;
import static com.atpl.demo.exception.HttpResponseUtils.prepareSuccessResponse;

import javax.validation.Valid;
import javax.ws.rs.core.MediaType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.atpl.demo.constant.Constants;
import com.atpl.demo.controller.boarding.BoardingController;
import com.atpl.demo.exception.GenericRes;
import com.atpl.demo.model.boarding.BoardingModel;
import com.atpl.demo.service.boarding.BoardingService;



@RestController
@RequestMapping("/v2")
public class BoardingController implements Constants{

	private static final Logger logger = LoggerFactory.getLogger(BoardingController.class);
	@Autowired
	BoardingService boardingService;

//http://{mmg-host}:8083/mmg/api/v2/boardingrequest/count?stateId={stateId}
	@RequestMapping(value = "/dashboard/boardingrequest/count/stateId={stateId}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON)
	@ResponseBody
	public ResponseEntity<GenericRes> getDriverCount(@PathVariable("stateId") Integer stateId) throws Exception {
		return prepareSuccessResponse(boardingService.getBoardingCount(stateId));
	}
}
