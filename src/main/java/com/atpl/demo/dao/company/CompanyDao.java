package com.atpl.demo.dao.company;

import java.sql.Blob;
import java.util.List;

import org.springframework.web.multipart.MultipartFile;

import com.atpl.demo.domain.company.CompanyDomain;
import com.atpl.demo.model.company.CompanyModel;

public interface CompanyDao {

	public String AddCompanyList(List<CompanyDomain> companyDomain) throws Exception;

	public List<CompanyDomain> search(String value) throws Exception;

	public String uploadImage(byte[] image) throws Exception;

	public List<CompanyDomain> companySearch(String key, String value) throws Exception;

	public List<CompanyDomain> companyPagination(int PageSize, int PageNo) throws Exception;

	public Blob downloadImage(String uuId) throws Exception;

	public Boolean companyValidatation(String key, String value) throws Exception;

	public List<CompanyDomain> getCompanyStatus(Boolean status) throws Exception;

	public String saveCompany(CompanyDomain companyDomain) throws Exception;

	public CompanyDomain getCompany(String id) throws Exception;

	public List<CompanyDomain> getCompany() throws Exception;

	public String updateCompany(CompanyDomain id, String uuId) throws Exception;

	public String deleteCompany(String id) throws Exception;

	public String patchCompany(CompanyDomain id) throws Exception;

	public String patchCompanyStatus(Boolean status, String uuId) throws Exception;

	// public CompanyDomain getCompanyUuId(String uuid) throws Exception;

//	public Boolean validateEmail(String email) throws Exception;
//	
//	public Boolean validateGst(String gst) throws Exception;
//	
//	public Boolean validateWebsite(String website) throws Exception;
//	
//	public Boolean validatePan(String pan) throws Exception;
}
