package com.atpl.demo.dao.company;

import java.sql.Blob;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;
import org.springframework.web.multipart.MultipartFile;

import com.atpl.demo.constant.Constants;
import com.atpl.demo.dao.company.CompanyDaoImpl;
import com.atpl.demo.domain.company.CompanyDomain;
import com.atpl.demo.exception.MmgRestException.BACKEND_SERVER_ERROR;
import com.atpl.demo.exception.MmgRestException.DELETE_FAILED;
import com.atpl.demo.exception.MmgRestException.NOT_FOUND;
import com.atpl.demo.exception.MmgRestException.SAVE_FAILED;
import com.atpl.demo.exception.MmgRestException.UPDATE_FAILED;
import com.atpl.demo.model.company.CompanyModel;
import com.atpl.demo.utils.DateUtility;

import java.util.UUID;

@Repository
@SuppressWarnings("rawtypes")
public class CompanyDaoImpl implements CompanyDao, Constants {

	private static final Logger logger = LoggerFactory.getLogger(CompanyDaoImpl.class);

	@Autowired
	JdbcTemplate jdbcTemplate;

	@Override
	public String saveCompany(CompanyDomain companyDomain) throws Exception {

		try {

			// UUID uuid=UUID.randomUUID().toString();
			SimpleDateFormat simpleDateFormat = new SimpleDateFormat(DateUtility.DATE_FORMAT_YYYY_MM_DD_HHMMSS);
			simpleDateFormat.setTimeZone(TimeZone.getTimeZone("GMT+5:30"));
			String sql = "INSERT INTO companydetails(uuId,companyName,companyType,established,mdName,mobileNumber,emailId,panNumber,gstNumber,webSite,createdDate,modifiedDate) VALUES(?,?,?,?,?,?,?,?,?,?,?,?)";
			int res = jdbcTemplate.update(sql,
					new Object[] { companyDomain.getUuId(), companyDomain.getCompanyName(),
							companyDomain.getCompanyType(), companyDomain.getEstablished(), companyDomain.getMdName(),
							companyDomain.getMobileNumber(), companyDomain.getEmailId(), companyDomain.getPanNumber(),
							companyDomain.getGstNumber(), companyDomain.getWebSite(), companyDomain.getCreatedDate(),
							companyDomain.getModifiedDate() });

			if (res == 1) {
				return "Saved successfully";
			} else
				throw new SAVE_FAILED("Company Save Failed");
		} catch (Exception e) {
			logger.error("Exception saveCompany in CompanyDAOImpl", e);
			throw new BACKEND_SERVER_ERROR();
		}
	}

	public String AddCompanyList(List<CompanyDomain> companyDomain) throws Exception {
		try {
			String sql = "INSERT INTO companydetails(uuId,companyName,companyType,established,mdName,mobileNumber,emailId,panNumber,gstNumber,webSite,createdDate,modifiedDate) VALUES(?,?,?,?,?,?,?,?,?,?,?,?)";
			for (CompanyDomain company : companyDomain) {
				System.out.println(company);
				jdbcTemplate.update(sql,
						new Object[] { company.getUuId(), company.getCompanyName(), company.getCompanyType(),
								company.getEstablished(), company.getMdName(), company.getMobileNumber(),
								company.getEmailId(), company.getPanNumber(), company.getGstNumber(),
								company.getWebSite(), company.getCreatedDate(), company.getModifiedDate() });
				System.out.println(company.getCompanyName());
			}

			return "Saved successfully";

		} catch (Exception e) {
			logger.error("Exception saveCompany in CompanyDAOImpl", e);
			throw new BACKEND_SERVER_ERROR();
		}
	}

	@Override
	public List<CompanyDomain> companySearch(String key, String value) throws Exception {
		try {
			System.out.println(key);
			String sql = "SELECT * FROM companydetails where " + key + " LIKE '%" + value + "%'";
			System.out.println(sql);
			List<CompanyDomain> company = jdbcTemplate.query(sql, new Object[] {},
					new BeanPropertyRowMapper<CompanyDomain>(CompanyDomain.class));
			return company;
		} catch (EmptyResultDataAccessException e) {
			logger.error("EmptyResultDataAccessException getCompany in CompanyDAOImpl" + e.getMessage());
			throw new NOT_FOUND(" Company not found");
		} catch (Exception e) {
			logger.error("Exception getcompany in CompanyDAOImpl", e);
			throw new BACKEND_SERVER_ERROR();
		}
	}

	public List<CompanyDomain> search(String value) throws Exception {
		try {
			String sql = "SELECT * FROM companydetails where companyName LIKE '%" + value + "%' OR companyType LIKE '%"
					+ value + "%' OR emailId LIKE '%" + value + "%' OR panNumber LIKE '%" + value
					+ "%' OR gstNumber LIKE '%" + value + "%' OR isActive LIKE '%" + value + "%' OR webSite LIKE '%"
					+ value + "%' OR mobileNumber LIKE '%" + value + "%' OR mdName LIKE '%" + value + "%'";
			// System.out.println(sql);
			List<CompanyDomain> company = jdbcTemplate.query(sql, new Object[] {},
					new BeanPropertyRowMapper<CompanyDomain>(CompanyDomain.class));
			return company;
		} catch (EmptyResultDataAccessException e) {
			logger.error("EmptyResultDataAccessException getCompany in CompanyDAOImpl" + e.getMessage());
			throw new NOT_FOUND(" Company not found");
		} catch (Exception e) {
			logger.error("Exception getcompany in CompanyDAOImpl", e);
			throw new BACKEND_SERVER_ERROR();
		}
	}

	public List<CompanyDomain> companyPagination(int PageSize, int PageNo) throws Exception {
		try {
			String sql = "SELECT * FROM companydetails LIMIT " + PageNo + "," + PageSize;
			List<CompanyDomain> company = jdbcTemplate.query(sql, new Object[] {},
					new BeanPropertyRowMapper<CompanyDomain>(CompanyDomain.class));
			return company;
		} catch (EmptyResultDataAccessException e) {
			logger.error("EmptyResultDataAccessException getCompany in CompanyDAOImpl" + e.getMessage());
			throw new NOT_FOUND(" Company not found");
		} catch (Exception e) {
			logger.error("Exception getcompany in CompanyDAOImpl", e);
			throw new BACKEND_SERVER_ERROR();
		}
	}

	public String uploadImage(byte[] image) throws Exception {
		try {

			System.out.println(image);
			String sql = "INSERT INTO image(uuId,image) VALUES(?,?)";
			int res = jdbcTemplate.update(sql, new Object[] { UUID.randomUUID().toString(), image });

			if (res == 1) {
				return "Saved successfully";
			} else
				throw new SAVE_FAILED("Image Save Failed");
		} catch (Exception e) {
			logger.error("Exception saveImage in CompanyDAOImpl", e);
			throw new BACKEND_SERVER_ERROR();
		}

	}

	public Blob downloadImage(String uuId) throws Exception {
		try {

			String sql = "SELECT image from image WHERE uuId=?";
			Blob res = jdbcTemplate.queryForObject(sql, Blob.class, new Object[] { uuId });
			return res;
		} catch (EmptyResultDataAccessException e) {
			throw new NOT_FOUND("image not found");
		} catch (Exception e) {
			logger.error("Exception geImagebyid", e);
			throw new BACKEND_SERVER_ERROR();
		}
	}

	public Boolean companyValidatation(String key, String value) {
		String sql = "SELECT EXISTS(SELECT " + key + " from companydetails WHERE " + key + "=\"" +value +"\")";
		
		Boolean result = jdbcTemplate.queryForObject(sql, Boolean.class);
		return result;

	}

	@Override
	public CompanyDomain getCompany(String id) throws Exception {
		try {
			String sql = "SELECT * FROM companydetails where uuId=?";
			return (CompanyDomain) jdbcTemplate.queryForObject(sql, new Object[] { id },
					new BeanPropertyRowMapper<CompanyDomain>(CompanyDomain.class));
		} catch (EmptyResultDataAccessException e) {
			throw new NOT_FOUND("Company not found");
		} catch (Exception e) {
			logger.error("Exception getCompanybyid in CompanyDAOImpl", e);
			throw new BACKEND_SERVER_ERROR();
		}
	}

	@Override
	public List<CompanyDomain> getCompanyStatus(Boolean status) throws Exception {
		try {
			String sql = "SELECT * FROM companydetails where isActive=?";
			List<CompanyDomain> company = jdbcTemplate.query(sql, new Object[] { status },
					new BeanPropertyRowMapper<CompanyDomain>(CompanyDomain.class));
			return company;
		} catch (EmptyResultDataAccessException e) {
			logger.error("EmptyResultDataAccessException getCompany in CompanyDAOImpl" + e.getMessage());
			throw new NOT_FOUND(" Company not found");
		} catch (Exception e) {
			logger.error("Exception getcompany in CompanyDAOImpl", e);
			throw new BACKEND_SERVER_ERROR();
		}

	}

	@Override
	public List<CompanyDomain> getCompany() throws Exception {
		try {
			String sql = "SELECT * FROM companydetails";
			List<CompanyDomain> company = jdbcTemplate.query(sql, new Object[] {},
					new BeanPropertyRowMapper<CompanyDomain>(CompanyDomain.class));
			return company;
		} catch (EmptyResultDataAccessException e) {
			logger.error("EmptyResultDataAccessException getCompany in CompanyDAOImpl" + e.getMessage());
			throw new NOT_FOUND(" Company not found");
		} catch (Exception e) {
			logger.error("Exception getcompany in CompanyDAOImpl", e);
			throw new BACKEND_SERVER_ERROR();
		}

	}

	@Override
	public String updateCompany(CompanyDomain companyDomain, String uuId) throws Exception {
		try {

			SimpleDateFormat simpleDateFormat = new SimpleDateFormat(DateUtility.DATE_FORMAT_YYYY_MM_DD_HHMMSS);
			simpleDateFormat.setTimeZone(TimeZone.getTimeZone("GMT+5:30"));
			String sql = "UPDATE companydetails SET companyName=?, companyType=?,established=?,mdName=?,mobileNumber=?,emailId=?,panNumber=?,gstNumber=?,webSite=?,modifiedDate=? WHERE uuId=?";
			int res = jdbcTemplate.update(sql,
					new Object[] { companyDomain.getCompanyName(), companyDomain.getCompanyType(),
							companyDomain.getEstablished(), companyDomain.getMdName(), companyDomain.getMobileNumber(),
							companyDomain.getEmailId(), companyDomain.getPanNumber(), companyDomain.getGstNumber(),
							companyDomain.getWebSite(), simpleDateFormat.format(new Date()), uuId });
			if (res == 1) {
				return "Updated Successfully";
			} else
				throw new UPDATE_FAILED("Company Update Failed");
		} catch (Exception e) {
			logger.error("Exception updateCompany in CompanyDAOImpl", e);
			throw new BACKEND_SERVER_ERROR();
		}
	}

	@Override
	public String patchCompany(CompanyDomain companyDomain) throws Exception {
		// TODO Auto-generated method stub
		try {
			String sql = "UPDATE companydetails SET isActive=? WHERE uuId=?";
			int res = jdbcTemplate.update(sql, new Object[] { companyDomain.getIsActive(), companyDomain.getUuId() });

			if (res == 1) {
				return "Patch Successfully";
			} else
				throw new UPDATE_FAILED("Company Patch Failed");
		} catch (Exception e) {
			logger.error("Exception updateCompany in CompanyDAOImpl", e);
			throw new BACKEND_SERVER_ERROR();
		}
	}

	public String patchCompanyStatus(Boolean status, String uuId) throws Exception {
		// TODO Auto-generated method stub
		try {
			String sql = "UPDATE companydetails SET isActive=? WHERE uuId=?";
			int res = jdbcTemplate.update(sql, new Object[] { status, uuId });

			if (res == 1) {
				return "Patch Successfully";
			} else
				throw new UPDATE_FAILED("Company Patch Failed");
		} catch (Exception e) {
			logger.error("Exception updateCompany in CompanyDAOImpl", e);
			throw new BACKEND_SERVER_ERROR();
		}
	}

	@Override
	public String deleteCompany(String id) throws Exception {
		try {
			String sql = "DELETE FROM companydetails WHERE uuId=?";
			int res = jdbcTemplate.update(sql, new Object[] { id });
			if (res == 1) {
				return "Deleted successfully";
			} else
				throw new DELETE_FAILED("company Delete Failed");
		} catch (Exception e) {
			logger.error("Exception deletecompany in CompanyDAOImpl", e);
			throw new BACKEND_SERVER_ERROR();
		}
	}

}

//	public Boolean validateGst(String gst) throws Exception{
//		String sql = "SELECT EXISTS(SELECT gstNumber from companydetails WHERE gstNumber=?)";
//		Boolean result=jdbcTemplate.queryForObject(sql,Boolean.class,gst);
//		return result ;
//	}
//	
//	public Boolean validatePan(String pan) throws Exception{
//		String sql = "SELECT EXISTS(SELECT panNumber from companydetails WHERE panNumber=?)";
//		Boolean result=jdbcTemplate.queryForObject(sql,Boolean.class,pan);
//		return result ;
//	}
//	public Boolean validateWebsite(String website) throws Exception{
//		String sql = "SELECT EXISTS(SELECT webSite from companydetails WHERE webSite=?)";
//		Boolean result=jdbcTemplate.queryForObject(sql,Boolean.class,website);
//		return result ;
//	}
//	public Boolean validateEmail(String email) throws Exception{
//		String sql = "SELECT EXISTS(SELECT webSite from companydetails WHERE emailId=?)";
//		Boolean result=jdbcTemplate.queryForObject(sql,Boolean.class,email);
//		return result ;
//	}

//	public Boolean search(String email) throws Exception{
//		String sql = "SELECT EXISTS(SELECT webSite from companydetails WHERE ?=?)";
//		Boolean result=jdbcTemplate.queryForObject(sql,Boolean.class,email);
//		return result ;
//	}

//	@Override
//	public List<CompanyDomain> getCompanyValidation(String cn) throws Exception {
//		try {
//			String sql = "SELECT * FROM companydetails where companyName=?";
//			List<CompanyDomain> Bank = jdbcTemplate.query(sql, new Object[] {cn},
//					new BeanPropertyRowMapper<CompanyDomain>(CompanyDomain.class));
//			return Bank;
//		} catch (EmptyResultDataAccessException e) {
//			logger.error("EmptyResultDataAccessException getCompany in CompanyDAOImpl" + e.getMessage());
//			throw new NOT_FOUND(" Company not found");
//		} catch (Exception e) {
//			logger.error("Exception getcompany in CompanyDAOImpl", e);
//			throw new BACKEND_SERVER_ERROR();
//		}
//
//	}
