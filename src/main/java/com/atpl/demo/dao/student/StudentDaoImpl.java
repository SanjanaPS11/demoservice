package com.atpl.demo.dao.student;
import java.sql.Blob;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;
import org.springframework.web.multipart.MultipartFile;

import com.atpl.demo.constant.Constants;
import com.atpl.demo.dao.company.CompanyDao;
import com.atpl.demo.dao.company.CompanyDaoImpl;
import com.atpl.demo.domain.company.CompanyDomain;
import com.atpl.demo.domain.registration.RegistrationDomain;
import com.atpl.demo.domain.student.StudentDomain;
import com.atpl.demo.exception.MmgRestException.BACKEND_SERVER_ERROR;
import com.atpl.demo.exception.MmgRestException.DELETE_FAILED;
import com.atpl.demo.exception.MmgRestException.NOT_FOUND;
import com.atpl.demo.exception.MmgRestException.SAVE_FAILED;
import com.atpl.demo.exception.MmgRestException.UPDATE_FAILED;
import com.atpl.demo.model.company.CompanyModel;
import com.atpl.demo.utils.DateUtility;

import java.util.UUID;

@Repository
@SuppressWarnings("rawtypes")
public class StudentDaoImpl implements StudentDao, Constants {
	
	private static final Logger logger = LoggerFactory.getLogger(CompanyDaoImpl.class);

	@Autowired
	JdbcTemplate jdbcTemplate;

	@Override
	public String saveStudent(StudentDomain studentDomain) throws Exception {
		// TODO Auto-generated method stub
		try {

			// UUID uuid=UUID.randomUUID().toString();
			SimpleDateFormat simpleDateFormat = new SimpleDateFormat(DateUtility.DATE_FORMAT_YYYY_MM_DD_HHMMSS);
			simpleDateFormat.setTimeZone(TimeZone.getTimeZone("GMT+5:30"));
			String sql = "INSERT INTO student(uuId,firstName,lastName,phoneNumber,address,email,profile,createdDate,modifiedDate) VALUES(?,?,?,?,?,?,?,?,?)";
			int res = jdbcTemplate.update(sql,
					new Object[] { UUID.randomUUID().toString(),studentDomain.getFirstName(),studentDomain.getLastName(),studentDomain.getPhoneNumber(),studentDomain.getAddress(),
							studentDomain.getProfile(),studentDomain.getEmail(),simpleDateFormat.format(new Date()),simpleDateFormat.format(new Date()) });

			if (res == 1) {
				return "Saved successfully";
			} else
				throw new SAVE_FAILED("Student Save Failed");
		} catch (Exception e) {
			logger.error("Exception saveStudent in StudentDAOImpl", e);
			throw new BACKEND_SERVER_ERROR();
		}
	}

	@Override
	public String updateStudent(StudentDomain studentDomain) throws Exception {
		// TODO Auto-generated method stub
		try {
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat(DateUtility.DATE_FORMAT_YYYY_MM_DD_HHMMSS);
		simpleDateFormat.setTimeZone(TimeZone.getTimeZone("GMT+5:30"));
		String sql = "UPDATE student SET firstName=?, lastName=?,phoneNumber=?,address=?,email=?,createdDate=?,modifiedDate=? WHERE uuId=?";
		int res = jdbcTemplate.update(sql,
				new Object[] {studentDomain.getFirstName(),studentDomain.getLastName(),studentDomain.getPhoneNumber(),studentDomain.getAddress(),
						studentDomain.getEmail(),studentDomain.getCreatedDate(),studentDomain.getModifiedDate(),  simpleDateFormat.format(new Date()),studentDomain.getUuId()});
		if (res == 1) {
			return "Updated Successfully";
		} else
			throw new UPDATE_FAILED("Student Update Failed");
	} catch (Exception e) {
		logger.error("Exception updateStudent in StudentDAOImpl", e);
		throw new BACKEND_SERVER_ERROR();
	}
	}

	@Override
	public String deleteStudent(String uuId) throws Exception {
		// TODO Auto-generated method stub
		try {
			String sql = "DELETE FROM student WHERE uuId=?";
			int res = jdbcTemplate.update(sql, new Object[] { uuId });
			if (res == 1) {
				return "Deleted successfully";
			} else
				throw new DELETE_FAILED("studentDelete Failed");
		} catch (Exception e) {
			logger.error("Exception deleteStudent in StudentDAOImpl", e);
			throw new BACKEND_SERVER_ERROR();
		}
	}

	@Override
	public StudentDomain getStudentById(String uuId) throws Exception {
		// TODO Auto-generated method stub
		try {
			String sql = "SELECT * FROM student where uuId=?";
			return (StudentDomain) jdbcTemplate.queryForObject(sql, new Object[] { uuId },
					new BeanPropertyRowMapper<StudentDomain>(StudentDomain.class));
		} catch (EmptyResultDataAccessException e) {
			throw new NOT_FOUND("Student not found");
		} catch (Exception e) {
			logger.error("Exception getStudentbyid in StudentDAOImpl", e);
			throw new BACKEND_SERVER_ERROR();
		}
	}

	public List<StudentDomain> getStudent() throws Exception{
		try {
			String sql = "SELECT * FROM student";
			List<StudentDomain> student = jdbcTemplate.query(sql, new Object[] {},
					new BeanPropertyRowMapper<StudentDomain>(StudentDomain.class));
			return student;
		} catch (EmptyResultDataAccessException e) {
			logger.error("EmptyResultDataAccessException getRegistration in StudentDAOImpl" + e.getMessage());
			throw new NOT_FOUND("Student not found");
		} catch (Exception e) {
			logger.error("Exception getStudent in StudentDAOImpl", e);
			throw new BACKEND_SERVER_ERROR();
		}
	

}
}
