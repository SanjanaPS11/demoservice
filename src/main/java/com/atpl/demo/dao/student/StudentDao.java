package com.atpl.demo.dao.student;

import java.util.List;

import com.atpl.demo.domain.company.CompanyDomain;
import com.atpl.demo.domain.student.StudentDomain;
import com.atpl.demo.model.student.StudentModel;

public interface StudentDao {

	public String saveStudent(StudentDomain studentDomain) throws Exception;
	
	public String updateStudent(StudentDomain studentDomain) throws Exception;
	
	public String deleteStudent(String uuId) throws Exception;
	
	public StudentDomain getStudentById(String uuId) throws Exception;

	public List<StudentDomain> getStudent() throws Exception;
}
