package com.atpl.demo.dao.image;

import org.springframework.stereotype.Repository;
import java.sql.Blob;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;
import org.springframework.web.multipart.MultipartFile;

import com.atpl.demo.constant.Constants;
import com.atpl.demo.dao.company.CompanyDao;
import com.atpl.demo.dao.company.CompanyDaoImpl;
import com.atpl.demo.domain.company.CompanyDomain;
import com.atpl.demo.domain.image.ImageDomain;
import com.atpl.demo.exception.MmgRestException.BACKEND_SERVER_ERROR;
import com.atpl.demo.exception.MmgRestException.DELETE_FAILED;
import com.atpl.demo.exception.MmgRestException.NOT_FOUND;
import com.atpl.demo.exception.MmgRestException.SAVE_FAILED;
import com.atpl.demo.exception.MmgRestException.UPDATE_FAILED;
import com.atpl.demo.model.company.CompanyModel;
import com.atpl.demo.utils.DateUtility;

import java.util.UUID;
@Repository
@SuppressWarnings("rawtypes")
public class ImageDaoImpl implements ImageDao, Constants{
	
	private static final Logger logger = LoggerFactory.getLogger(ImageDaoImpl.class);

	@Autowired
	JdbcTemplate jdbcTemplate;
	
	public String uploadImage(ImageDomain imageDomain) throws Exception{
		try {
            // Blob img=image;
			
			String sql = "INSERT INTO image(uuId,image,imageType,imageName) VALUES(?,?,?,?)";
			System.out.println(imageDomain.getImage());
			System.out.println(imageDomain.getImageType());
			System.out.println(imageDomain.getImageName());
			System.out.println(imageDomain.getUuId());
			
			int res = jdbcTemplate.update(sql,
					new Object[] {imageDomain.getUuId(),imageDomain.getImage(),imageDomain.getImageType(),imageDomain.getImageName()});

			if (res == 1) {
				return "Saved successfully";
			} else
				throw new SAVE_FAILED("Image Save Failed");
		} catch (Exception e) {
			logger.error("Exception saveImage in ImageDAOImpl", e);
			throw new BACKEND_SERVER_ERROR();
		}
		
	}
	public ImageDomain downloadImage(String uuId) throws Exception{
		try {

			String sql = "SELECT * from image WHERE uuId=?";
			return (ImageDomain) jdbcTemplate.queryForObject(sql, new Object[] { uuId },
					new BeanPropertyRowMapper<ImageDomain>(ImageDomain.class));
		} catch (EmptyResultDataAccessException e) {
			throw new NOT_FOUND("image not found");
		} catch (Exception e) {
			logger.error("Exception geImagebyid", e);
			throw new BACKEND_SERVER_ERROR();
		}
	}
	
	public ImageDomain downloadImage1()throws Exception{
		try {

			String sql = "SELECT * from image WHERE uuId=?";
			return (ImageDomain) jdbcTemplate.queryForObject(sql, new Object[] {  },
					new BeanPropertyRowMapper<ImageDomain>(ImageDomain.class));
		} catch (EmptyResultDataAccessException e) {
			throw new NOT_FOUND("image not found");
		} catch (Exception e) {
			logger.error("Exception geImagebyid", e);
			throw new BACKEND_SERVER_ERROR();
		}
		
	}
	
	public List<ImageDomain> getImage() throws Exception{
		try {

			String sql = "SELECT * from image";
			List<ImageDomain> image = jdbcTemplate.query(sql, new Object[] {},
					new BeanPropertyRowMapper<ImageDomain>(ImageDomain.class));
			return image;
		} catch (EmptyResultDataAccessException e) {
			throw new NOT_FOUND("image not found");
		} catch (Exception e) {
			logger.error("Exception geImagebyid", e);
			throw new BACKEND_SERVER_ERROR();
		}
	}

}
