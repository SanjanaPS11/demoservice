package com.atpl.demo.dao.image;

import java.sql.Blob;
import java.util.List;

import com.atpl.demo.domain.image.ImageDomain;
import com.atpl.demo.model.image.ImageModel;

public interface ImageDao {
	
    public String uploadImage(ImageDomain imageDomain ) throws Exception;
	
	public ImageDomain downloadImage(String uuId) throws Exception;
	
	public List<ImageDomain> getImage() throws Exception;
	
	public ImageDomain downloadImage1()throws Exception;
}
