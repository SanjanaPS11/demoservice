package com.atpl.demo.dao.login;

import com.atpl.demo.domain.login.LoginDomain;
import com.atpl.demo.domain.message.MessageDomain;

public interface LoginDao {
	public String Login(String username) throws Exception;
	
}
