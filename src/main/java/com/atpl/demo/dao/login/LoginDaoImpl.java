package com.atpl.demo.dao.login;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import com.atpl.demo.constant.Constants;
import com.atpl.demo.dao.labour.LabourDaoImpl;
import com.atpl.demo.domain.login.LoginDomain;
import com.atpl.demo.exception.MmgRestException.BACKEND_SERVER_ERROR;
import com.atpl.demo.exception.MmgRestException.NOT_FOUND;

@Repository
@SuppressWarnings("rawtypes")
public class LoginDaoImpl  implements LoginDao, Constants {

private static final Logger logger = LoggerFactory.getLogger(LabourDaoImpl.class);
	
	@Autowired
	JdbcTemplate jdbcTemplate;
	
	
	@Override
	public String Login(String username) throws Exception {
		try {
			String sql = "SELECT passWord FROM login where userName=?";
			String password = jdbcTemplate.queryForObject(sql, new Object[] {username},
					String.class);
			return password;
		} catch (EmptyResultDataAccessException e) {
			logger.error("EmptyResultDataAccessException getLogin in LoginDao" + e.getMessage());
			throw new NOT_FOUND("userName not found");
		} catch (Exception e) {
			logger.error("Exception in LoginDAOImpl", e);
			throw new BACKEND_SERVER_ERROR();
		}

	}

}
