package com.atpl.demo.dao.driver;
import java.sql.Blob;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;
import java.util.UUID;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import com.atpl.demo.constant.Constants;
import com.atpl.demo.dao.employee.EmployeeDao;
import com.atpl.demo.dao.employee.EmployeeDaoImpl;
import com.atpl.demo.domain.company.CompanyDomain;
import com.atpl.demo.domain.driver.DriverDomain;
import com.atpl.demo.domain.employee.EmployeeDomain;
import com.atpl.demo.domain.message.RoleId;
import com.atpl.demo.exception.MmgRestException.BACKEND_SERVER_ERROR;
import com.atpl.demo.exception.MmgRestException.DELETE_FAILED;
import com.atpl.demo.exception.MmgRestException.NOT_FOUND;
import com.atpl.demo.exception.MmgRestException.SAVE_FAILED;
import com.atpl.demo.exception.MmgRestException.UPDATE_FAILED;
import com.atpl.demo.utils.DateUtility;

import java.util.Properties;

import javax.mail.Message;
import javax.mail.Message.RecipientType;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import org.springframework.mail.MailSender;  
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.mail.javamail.MimeMessageHelper; 
@Repository
@SuppressWarnings("rawtypes")
public class DriverDaoImpl implements DriverDao, Constants{

	private static final Logger logger = LoggerFactory.getLogger(DriverDaoImpl.class);
	
	@Autowired
	JdbcTemplate jdbcTemplate;
	
	public Blob saveDriver(DriverDomain driverDomain) throws Exception{
		try {
			SimpleDateFormat simpleDateFormat = new SimpleDateFormat(DateUtility.DATE_FORMAT_YYYY_MM_DD_HHMMSS);
			simpleDateFormat.setTimeZone(TimeZone.getTimeZone("GMT+5:30"));
			String save="INSERT INTO driver (uuId,name,phoneNo,email,createdDate,modifiedDate)VALUES(?,?,?,?,?,?)";
			int res = jdbcTemplate.update(save,
					new Object[] {UUID.randomUUID().toString(),driverDomain.getName(),driverDomain.getPhoneNo(),driverDomain.getEmail(), simpleDateFormat.format(new Date()), simpleDateFormat.format(new Date()) });
			
			String sql="SELECT message FROM message where messageName=? and roleId=?";
			return  jdbcTemplate.queryForObject(sql, new Object[] {driverDomain.getMessageName(),driverDomain.getRoleId() },Blob.class);
			
	} catch (Exception e) {
		logger.error("Exception saveDriver in DriverDAOImpl", e);
		throw new BACKEND_SERVER_ERROR();
	}
	}
}
	
	
	
	
	


