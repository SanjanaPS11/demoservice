package com.atpl.demo.dao.driver;
import java.sql.Blob;
import java.util.List;
import com.atpl.demo.domain.driver.DriverDomain;
import com.atpl.demo.model.driver.DriverModel;

public interface DriverDao {
//	public Integer getDriverCount(String requestedBy,String status,Integer stateId) throws Exception;
	
	public Blob saveDriver(DriverDomain driverDomain) throws Exception;
}
