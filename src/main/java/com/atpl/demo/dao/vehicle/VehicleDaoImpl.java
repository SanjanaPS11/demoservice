package com.atpl.demo.dao.vehicle;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import com.atpl.demo.constant.Constants;
import com.atpl.demo.dao.vehicle.VehicleDao;
import com.atpl.demo.dao.vehicle.VehicleDaoImpl;
import com.atpl.demo.domain.driver.DriverDomain;
import com.atpl.demo.domain.vehicle.VehicleDomain;
import com.atpl.demo.exception.MmgRestException.BACKEND_SERVER_ERROR;
import com.atpl.demo.exception.MmgRestException.DELETE_FAILED;
import com.atpl.demo.exception.MmgRestException.NOT_FOUND;
import com.atpl.demo.exception.MmgRestException.SAVE_FAILED;
import com.atpl.demo.exception.MmgRestException.UPDATE_FAILED;
import com.atpl.demo.utils.DateUtility;

@Repository
@SuppressWarnings("rawtypes")
public class VehicleDaoImpl implements VehicleDao, Constants{

private static final Logger logger = LoggerFactory.getLogger(VehicleDaoImpl.class);
	
	@Autowired
	JdbcTemplate jdbcTemplate;
	
	public Integer getVehicleCount(String requestedBy,String status,Integer stateId) throws Exception {
		try {
			VehicleDomain vehicleDomain=new VehicleDomain();
			String sql = "select count(*) as total from vehicle where requestedBy=? AND status=? AND stateId=?";
			int total = jdbcTemplate.queryForObject(sql, new Object[] {requestedBy,status,stateId},
					(Integer.class));
			vehicleDomain.setTotal(total);
			//driverDomain.setTotal=total;
			return vehicleDomain.getTotal();
		} catch (EmptyResultDataAccessException e) {
			logger.error("EmptyResultDataAccessException getVehicleCount in driverDAOImpl" + e.getMessage());
			throw new NOT_FOUND("driver not found");
		} catch (Exception e) {
			logger.error("Exception getVehicleCount in vehicleDAOImpl", e);
			throw new BACKEND_SERVER_ERROR();
		}

	}
}
