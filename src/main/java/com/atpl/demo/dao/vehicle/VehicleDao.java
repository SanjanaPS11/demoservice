package com.atpl.demo.dao.vehicle;
import java.util.List;
import com.atpl.demo.domain.vehicle.VehicleDomain;
import com.atpl.demo.model.vehicle.VehicleModel;

public interface VehicleDao {

	public Integer getVehicleCount(String requestedBy,String status,Integer stateId) throws Exception;
}
