package com.atpl.demo.dao.employee;

import java.util.List;

import com.atpl.demo.domain.employee.EmployeeDomain;

public interface EmployeeDao {

	public String saveEmployee(EmployeeDomain employeeDomain) throws Exception;

	public List<EmployeeDomain> getEmployee() throws Exception;

	public EmployeeDomain getEmployee(int id) throws Exception;

	public String updateEmployee(EmployeeDomain id) throws Exception;

	public String deleteEmployee(int id) throws Exception;
}
