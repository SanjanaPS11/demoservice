package com.atpl.demo.dao.employee;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import com.atpl.demo.constant.Constants;
import com.atpl.demo.dao.employee.EmployeeDao;
import com.atpl.demo.dao.employee.EmployeeDaoImpl;
import com.atpl.demo.domain.employee.EmployeeDomain;
import com.atpl.demo.exception.MmgRestException.BACKEND_SERVER_ERROR;
import com.atpl.demo.exception.MmgRestException.DELETE_FAILED;
import com.atpl.demo.exception.MmgRestException.NOT_FOUND;
import com.atpl.demo.exception.MmgRestException.SAVE_FAILED;
import com.atpl.demo.exception.MmgRestException.UPDATE_FAILED;
import com.atpl.demo.utils.DateUtility;

@Repository
@SuppressWarnings("rawtypes")
public class EmployeeDaoImpl implements EmployeeDao, Constants {
	
	private static final Logger logger = LoggerFactory.getLogger(EmployeeDaoImpl.class);

	@Autowired
	JdbcTemplate jdbcTemplate;

	public String saveEmployee(EmployeeDomain employeeDomain) {
		try {
	
			String sql = " INSERT INTO employee(firstName,lastName,gender,address) VALUES (?,?,?,?)";
			int res = jdbcTemplate.update(sql, new Object[] {employeeDomain.getId(), employeeDomain.getFirstName(),employeeDomain.getLastName(),employeeDomain.getGender(),employeeDomain.getAddress()});
			if (res == 1) {
				return "Saved successfully";
			} else
				throw new SAVE_FAILED("Employee Save Failed");
		} catch (Exception e) {
			logger.error("Exception saveEmployee in EmployeeDAOImpl", e);
			throw new BACKEND_SERVER_ERROR();
		}
	}
	
	public List<EmployeeDomain> getEmployee() throws Exception {
		try {
			String sql = "SELECT * FROM employee";
			List<EmployeeDomain> Bank = jdbcTemplate.query(sql, new Object[] {},
					new BeanPropertyRowMapper<EmployeeDomain>(EmployeeDomain.class));
			return Bank;
		} catch (EmptyResultDataAccessException e) {
			logger.error("EmptyResultDataAccessException getEmployee in EmployeeDAOImpl" + e.getMessage());
			throw new NOT_FOUND("Employee not found");
		} catch (Exception e) {
			logger.error("Exception getEmployee in EmployeeDAOImpl", e);
			throw new BACKEND_SERVER_ERROR();
		}

	}
	
	@Override
	public String updateEmployee(EmployeeDomain employeeDomain) throws Exception {
		try {
			
			String sql = "UPDATE employee SET firstName=?, lastName=?,gender=?,address=? WHERE id=?";
			int res = jdbcTemplate.update(sql, new Object[] { employeeDomain.getFirstName(),employeeDomain.getLastName(),employeeDomain.getGender(),employeeDomain.getAddress(),employeeDomain.getId() });
			if (res == 1) {
				return "Updated Successfully";
			} else
				throw new UPDATE_FAILED("Employee Update Failed");
		} catch (Exception e) {
			logger.error("Exception updateEmployee inEmployeeDAOImpl", e);
			throw new BACKEND_SERVER_ERROR();
		}
	}

	@Override
	public EmployeeDomain getEmployee(int id) throws Exception {
		try {
			String sql = "SELECT * FROM employee where id=?";
			return (EmployeeDomain) jdbcTemplate.queryForObject(sql, new Object[] { id },
					new BeanPropertyRowMapper<EmployeeDomain>(EmployeeDomain.class));
		} catch (EmptyResultDataAccessException e) {
			throw new NOT_FOUND("Employee not found");
		} catch (Exception e) {
			logger.error("Exception getEmployeebyid in EmployeeDAOImpl", e);
			throw new BACKEND_SERVER_ERROR();
		}
	}

	@Override
	public String deleteEmployee(int id) throws Exception {
		
		try {
			String sql = "DELETE FROM employee WHERE id=?";
			int res = jdbcTemplate.update(sql, new Object[] { id });
			if (res == 1) {
				return "Deleted successfully";
			} else
				throw new DELETE_FAILED("employee Delete Failed");
		} catch (Exception e) {
			logger.error("Exception deleteemployee in EmployeeDAOImpl", e);
			throw new BACKEND_SERVER_ERROR();
		}
	}

}
