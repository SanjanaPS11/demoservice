package com.atpl.demo.dao.boarding;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import com.atpl.demo.constant.Constants;
import com.atpl.demo.dao.boarding.BoardingDao;
import com.atpl.demo.dao.boarding.BoardingDaoImpl;
import com.atpl.demo.domain.boarding.BoardingDomain;
import com.atpl.demo.exception.MmgRestException.BACKEND_SERVER_ERROR;
import com.atpl.demo.exception.MmgRestException.DELETE_FAILED;
import com.atpl.demo.exception.MmgRestException.NOT_FOUND;
import com.atpl.demo.exception.MmgRestException.SAVE_FAILED;
import com.atpl.demo.exception.MmgRestException.UPDATE_FAILED;
import com.atpl.demo.utils.DateUtility;
import java.util.HashMap;

@Repository
@SuppressWarnings("rawtypes")

public class BoardingDaoImpl implements BoardingDao, Constants {

	private static final Logger logger = LoggerFactory.getLogger(BoardingDaoImpl.class);

	@Autowired
	JdbcTemplate jdbcTemplate;
	
	public BoardingDomain getBoardingCount(Integer stateId) throws Exception {
	
		BoardingDomain boardingDomain=new BoardingDomain();
		HashMap<Integer, String> boarding = new HashMap<Integer, String>();
		
		try {
			boarding.put(3,"franchise");
			boarding.put(9,"fleet");
			boarding.put(4,"warehouse");
			boarding.put(19,"enterprise");
			boarding.forEach((k, v) -> {
		          System.out.format("key: %d, value: %s%n", k, v);
			});
			boarding.forEach((k, v) -> {
			          System.out.format("key: %d, value: %s%n", k, v);
					String sql = "SELECT count(*) from boarding where roleId=? AND stateId=?";
					Integer total = jdbcTemplate.queryForObject(sql, new Object[] { k, stateId },(Integer.class));
					System.out.println(total);
					if(k==3) {
						boardingDomain.setFranchise(total);
					}
					else if(k==9){
						boardingDomain.setFleet(total);
					}
					else if(k==4) {
						boardingDomain.setWarehouse(total);
					}
					else if(k==19) {
						boardingDomain.setEnterprise(total);
					}
					else {
						System.out.println("doestnot exist");
					}
					  });
			return  boardingDomain;
			
		} catch (EmptyResultDataAccessException e) {
			throw new NOT_FOUND("Boarding not found");
		} catch (Exception e) {
			logger.error("Exception getdoarding count in BoardingDAOImpl", e);
			throw new BACKEND_SERVER_ERROR();
		}
	}
}
