package com.atpl.demo.dao.boarding;
import java.util.List;

import com.atpl.demo.domain.boarding.BoardingDomain;


public interface BoardingDao {

	public BoardingDomain getBoardingCount(Integer stateId) throws Exception;
}
