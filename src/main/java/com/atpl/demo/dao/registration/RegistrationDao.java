package com.atpl.demo.dao.registration;

import java.util.List;

import com.atpl.demo.domain.registration.RegistrationDomain;

public interface RegistrationDao {

	public String saveRegistration(RegistrationDomain registrationDomain) throws Exception;

	public List<RegistrationDomain> getRegistration() throws Exception;

	public RegistrationDomain getRegistration(int id) throws Exception;

	public String updateRegistration(RegistrationDomain id) throws Exception;

	public String deleteRegistration(int id) throws Exception;

}
