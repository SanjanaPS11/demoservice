package com.atpl.demo.dao.registration;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import com.atpl.demo.constant.Constants;
import com.atpl.demo.domain.registration.RegistrationDomain;
import com.atpl.demo.exception.MmgRestException.BACKEND_SERVER_ERROR;
import com.atpl.demo.exception.MmgRestException.DELETE_FAILED;
import com.atpl.demo.exception.MmgRestException.NOT_FOUND;
import com.atpl.demo.exception.MmgRestException.SAVE_FAILED;
import com.atpl.demo.exception.MmgRestException.UPDATE_FAILED;
import com.atpl.demo.utils.DateUtility;


@Repository
@SuppressWarnings("rawtypes")
public class RegistrationDaoImpl implements RegistrationDao, Constants {

	private static final Logger logger = LoggerFactory.getLogger(RegistrationDaoImpl.class);

	@Autowired
	JdbcTemplate jdbcTemplate;

	public String saveRegistration(RegistrationDomain registrationDomain) {
		try {
			SimpleDateFormat simpleDateFormat = new SimpleDateFormat(DateUtility.DATE_FORMAT_YYYY_MM_DD_HHMMSS);
			simpleDateFormat.setTimeZone(TimeZone.getTimeZone("GMT+5:30"));
			String sql = "INSERT INTO registration(id,firstName, middleName, lastName, gender, dob, address, mobileNumber, pincode, creationDate, modificationDate) VALUES(?,?,?,?,?,?,?,?,?,?,?)";
			int res = jdbcTemplate.update(sql, new Object[] {registrationDomain.getId(), registrationDomain.getFirstName(),registrationDomain.getMiddleName(),registrationDomain.getLastName(),registrationDomain.getGender(),registrationDomain.getDob(),registrationDomain.getAddress(),registrationDomain.getMobileNumber(),registrationDomain.getPinCode(),simpleDateFormat.format(new Date()), simpleDateFormat.format(new Date()) });
			if (res == 1) {
				return "Saved successfully";
			} else
				throw new SAVE_FAILED("Registration Save Failed");
		} catch (Exception e) {
			logger.error("Exception saveRegistration in RegistrationDAOImpl", e);
			throw new BACKEND_SERVER_ERROR();
		}

	}

	public List<RegistrationDomain> getRegistration() throws Exception {
		try {
			String sql = "SELECT * FROM registration";
			List<RegistrationDomain> Bank = jdbcTemplate.query(sql, new Object[] {},
					new BeanPropertyRowMapper<RegistrationDomain>(RegistrationDomain.class));
			return Bank;
		} catch (EmptyResultDataAccessException e) {
			logger.error("EmptyResultDataAccessException getRegistration in RegistrationDAOImpl" + e.getMessage());
			throw new NOT_FOUND("Registration not found");
		} catch (Exception e) {
			logger.error("Exception getRegistration in RegistrationDAOImpl", e);
			throw new BACKEND_SERVER_ERROR();
		}

	}

	@Override
	public String updateRegistration(RegistrationDomain registrationDomain) throws Exception {
		try {
			
			String sql = "UPDATE registration SET firstName=?, lastName=?, middleName=?, gender=?,dob=?,address=?,mobileNumber=?,pinCode=?  WHERE id=?";
			int res = jdbcTemplate.update(sql, new Object[] { registrationDomain.getFirstName(),registrationDomain.getLastName(),registrationDomain.getMiddleName(),registrationDomain.getGender(),registrationDomain.getDob(),registrationDomain.getAddress(),registrationDomain.getMobileNumber(),registrationDomain.getPinCode(),registrationDomain.getId() });
			if (res == 1) {
				return "Updated Successfully";
			} else
				throw new UPDATE_FAILED("Registration Update Failed");
		} catch (Exception e) {
			logger.error("Exception updateRegistration in RegistrationDAOImpl", e);
			throw new BACKEND_SERVER_ERROR();
		}
	}

	@Override
	public RegistrationDomain getRegistration(int id) throws Exception {
		try {
			String sql = "SELECT * FROM registration where id=?";
			return (RegistrationDomain) jdbcTemplate.queryForObject(sql, new Object[] { id },
					new BeanPropertyRowMapper<RegistrationDomain>(RegistrationDomain.class));
		} catch (EmptyResultDataAccessException e) {
			throw new NOT_FOUND("Registration not found");
		} catch (Exception e) {
			logger.error("Exception getRegistrationbyid in RegistrationDAOImpl", e);
			throw new BACKEND_SERVER_ERROR();
		}
	}

	@Override
	public String deleteRegistration(int id) throws Exception {
		
		try {
			String sql = "DELETE FROM registration WHERE id=?";
			int res = jdbcTemplate.update(sql, new Object[] { id });
			if (res == 1) {
				return "Deleted successfully";
			} else
				throw new DELETE_FAILED("Registration Delete Failed");
		} catch (Exception e) {
			logger.error("Exception deleteRegistration in RegistrationDAOImpl", e);
			throw new BACKEND_SERVER_ERROR();
		}
	}

}
