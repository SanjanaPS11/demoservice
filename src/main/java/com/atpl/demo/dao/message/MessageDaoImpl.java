package com.atpl.demo.dao.message;

import java.sql.Blob;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;
import java.util.UUID;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import com.atpl.demo.constant.Constants;
import com.atpl.demo.dao.labour.LabourDaoImpl;
import com.atpl.demo.domain.message.MessageDomain;
import com.atpl.demo.domain.message.RoleId;
import com.atpl.demo.domain.registration.RegistrationDomain;
import com.atpl.demo.exception.MmgRestException.BACKEND_SERVER_ERROR;
import com.atpl.demo.exception.MmgRestException.DELETE_FAILED;
import com.atpl.demo.exception.MmgRestException.NOT_FOUND;
import com.atpl.demo.exception.MmgRestException.SAVE_FAILED;
import com.atpl.demo.exception.MmgRestException.UPDATE_FAILED;

import com.atpl.demo.utils.DateUtility;

@Repository
@SuppressWarnings("rawtypes")
public class MessageDaoImpl implements MessageDao, Constants{
private static final Logger logger = LoggerFactory.getLogger(LabourDaoImpl.class);
	
	@Autowired
	JdbcTemplate jdbcTemplate;
	
	@Override
	public String saveMessage(MessageDomain messageDomain,int role) throws Exception {
		try {
						
			SimpleDateFormat simpleDateFormat = new SimpleDateFormat(DateUtility.DATE_FORMAT_YYYY_MM_DD_HHMMSS);
			simpleDateFormat.setTimeZone(TimeZone.getTimeZone("GMT+5:30"));

			String sql = "INSERT INTO message(uuId,roleId,messageName,message,createdDate,modifiedDate) VALUES(?,?,?,?,?,?)";
			 int res = jdbcTemplate.update(sql,
					new Object[] {UUID.randomUUID().toString(),role,messageDomain.getMessageName(),messageDomain.getMessage(),simpleDateFormat.format(new Date()), simpleDateFormat.format(new Date())});
            
			if (res == 1) {
				return "Saved successfully";
			} else
				throw new SAVE_FAILED("message Save Failed");
		} catch (Exception e) {
			logger.error("Exception savemessage in messageDAOImpl", e);
			throw new BACKEND_SERVER_ERROR();
		}
		
	}
		
		public String getUuId(int roleId, String messageName) throws Exception{
		String sql = "SELECT uuId FROM message where roleId=? AND messageName=? AND isActive=true";
		String result=jdbcTemplate.queryForObject(sql, new Object[] {roleId,messageName},String.class);
		return  result;
	}
		
	
		public Boolean checkUuId(int roleId, String messageName) {
			

				String 	 sql = "SELECT EXISTS(SELECT uuId FROM message where roleId=? AND messageName=? AND isActive=true)";
				Boolean res = jdbcTemplate.queryForObject(sql, Boolean.class, new Object[] { roleId,messageName });
				return res;
		
		}	

	@Override
		public String updateActive(String id) throws Exception {
	
			try {
			String sql = "Update message SET isActive=0 where uuId=?";
			int result = jdbcTemplate.update(sql, new Object[] { id });
			if (result == 1) {
				return "Updated Successfully";
			} else
				throw new UPDATE_FAILED("Message Update Failed");
		}
		catch (Exception e) {
			logger.error("Exception updateMessage in MessageDAOImpl", e);
			throw new BACKEND_SERVER_ERROR();
		}
	}
		

	@Override
	public List<MessageDomain> getMessage() throws Exception {
		try {
			String sql = "SELECT * FROM message where isActive=1";
			List<MessageDomain> message = jdbcTemplate.query(sql, new Object[] {},
					new BeanPropertyRowMapper<MessageDomain>(MessageDomain.class));
			return message;
		} catch (EmptyResultDataAccessException e) {
			logger.error("EmptyResultDataAccessException getMessage in MessageDao" + e.getMessage());
			throw new NOT_FOUND("Message not found");
		} catch (Exception e) {
			logger.error("Exception getMessage in MessageDAOImpl", e);
			throw new BACKEND_SERVER_ERROR();
		}

	}

	@Override
	public String updateMessage(MessageDomain messageDomain) throws Exception {
try {
			SimpleDateFormat simpleDateFormat = new SimpleDateFormat(DateUtility.DATE_FORMAT_YYYY_MM_DD_HHMMSS);
			simpleDateFormat.setTimeZone(TimeZone.getTimeZone("GMT+5:30"));
			String sql = "UPDATE message SET message=?,modifiedDate=?  WHERE uuId=?";
			int res = jdbcTemplate.update(sql, new Object[] {messageDomain.getMessage(),simpleDateFormat.format(new Date()),messageDomain.getUuId()});
			if (res == 1) {
				return "Updated Successfully";
			} else
				throw new UPDATE_FAILED("Message Update Failed");
		} catch (Exception e) {
			logger.error("Exception updateMessage in MessageDAOImpl", e);
			throw new BACKEND_SERVER_ERROR();
		}
	}

	@Override
	public String deleteMessage(String uuId) throws Exception {
		
		try {
			String sql = "Update message SET isActive=0 where uuId=?";
			int res = jdbcTemplate.update(sql, new Object[] { uuId });
			if (res == 1) {
				return "Deleted successfully";
			} else
				throw new DELETE_FAILED("message Delete Failed");
		} catch (Exception e) {
			logger.error("Exception deletemessage in messageDAOImpl", e);
			throw new BACKEND_SERVER_ERROR();
		}
	
	}

	@Override
	public MessageDomain getMessageByuuId(String uuId) throws Exception {
		try {
			String sql = "SELECT * FROM message where uuId=?";
			return (MessageDomain) jdbcTemplate.queryForObject(sql, new Object[] { uuId },
					new BeanPropertyRowMapper<MessageDomain>(MessageDomain.class));
		} catch (EmptyResultDataAccessException e) {
			throw new NOT_FOUND("Message not found");
		} catch (Exception e) {
			logger.error("Exception getMessagebyid in MessageDAOImpl", e);
			throw new BACKEND_SERVER_ERROR();
		}
	}

	@Override
	public List<MessageDomain> getMessageByActiveOrDeactive(Boolean isActive) throws Exception {
		try {
			String sql = "SELECT * FROM message where isActive=0";
			List<MessageDomain> message = jdbcTemplate.query(sql, new Object[] {},
					new BeanPropertyRowMapper<MessageDomain>(MessageDomain.class));
			return message;
		} catch (EmptyResultDataAccessException e) {
			logger.error("EmptyResultDataAccessException getMessage in MessageDao" + e.getMessage());
			throw new NOT_FOUND("Message not found");
		} catch (Exception e) {
			logger.error("Exception getMessage in MessageDAOImpl", e);
			throw new BACKEND_SERVER_ERROR();
		}
	}
}


