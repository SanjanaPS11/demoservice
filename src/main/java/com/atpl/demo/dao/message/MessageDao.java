package com.atpl.demo.dao.message;
import java.util.List;
import com.atpl.demo.domain.message.MessageDomain;

public interface MessageDao {
	public String saveMessage(MessageDomain messageDomain,int role) throws Exception;
	
	public List<MessageDomain> getMessage() throws Exception;
	
	public String updateMessage(MessageDomain messageDomain) throws Exception;
	
	public String deleteMessage(String uuId) throws Exception;
	
	public MessageDomain getMessageByuuId(String uuId)throws Exception;
	
	public List<MessageDomain> getMessageByActiveOrDeactive(Boolean isActive)throws Exception;
	
	public Boolean checkUuId(int roleId, String messageName) throws Exception;
	
	public String getUuId(int roleId, String messageName) throws Exception;
	
	public String updateActive(String id) throws Exception;
}
