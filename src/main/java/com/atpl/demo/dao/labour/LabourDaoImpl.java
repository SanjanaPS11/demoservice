package com.atpl.demo.dao.labour;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import com.atpl.demo.constant.Constants;
import com.atpl.demo.dao.labour.LabourDao;
import com.atpl.demo.dao.labour.LabourDaoImpl;
import com.atpl.demo.dao.labour.LabourDao;
import com.atpl.demo.dao.labour.LabourDaoImpl;
import com.atpl.demo.domain.labour.LabourDomain;
import com.atpl.demo.domain.labour.LabourDomain;
import com.atpl.demo.domain.labour.LabourDomain;
import com.atpl.demo.exception.MmgRestException.BACKEND_SERVER_ERROR;
import com.atpl.demo.exception.MmgRestException.DELETE_FAILED;
import com.atpl.demo.exception.MmgRestException.NOT_FOUND;
import com.atpl.demo.exception.MmgRestException.SAVE_FAILED;
import com.atpl.demo.exception.MmgRestException.UPDATE_FAILED;
import com.atpl.demo.utils.DateUtility;

@Repository
@SuppressWarnings("rawtypes")
public class LabourDaoImpl implements LabourDao, Constants{
	
private static final Logger logger = LoggerFactory.getLogger(LabourDaoImpl.class);
	
	@Autowired
	JdbcTemplate jdbcTemplate;
	
	
	public Integer getLabourCount(String requestedBy,String status,Integer stateId) throws Exception {
		try {
			LabourDomain labourDomain=new LabourDomain();
			String sql = "select count(*) as total from driver where requestedBy=? AND status=? AND stateId=?";
			int total = jdbcTemplate.queryForObject(sql, new Object[] {requestedBy,status,stateId},
					(Integer.class));
			labourDomain.setTotal(total);
			
			return labourDomain.getTotal();
		} catch (EmptyResultDataAccessException e) {
			logger.error("EmptyResultDataAccessException getLabourCount in labourDAOImpl" + e.getMessage());
			throw new NOT_FOUND("driver not found");
		} catch (Exception e) {
			logger.error("Exception getlabourCount in labourDAOImpl", e);
			throw new BACKEND_SERVER_ERROR();
		}

	}

}
