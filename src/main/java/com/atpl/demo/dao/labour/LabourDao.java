package com.atpl.demo.dao.labour;
import java.util.List;
import com.atpl.demo.domain.labour.LabourDomain;
import com.atpl.demo.model.labour.LabourModel;

public interface LabourDao {
	public Integer getLabourCount(String requestedBy,String status,Integer stateId) throws Exception;
}
