package com.atpl.demo.dao.studentLogin;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;

import com.atpl.demo.constant.Constants;
import com.atpl.demo.dao.registration.RegistrationDao;
import com.atpl.demo.dao.registration.RegistrationDaoImpl;
import com.atpl.demo.dao.student.StudentDao;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;
import java.util.UUID;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import com.atpl.demo.constant.Constants;
import com.atpl.demo.domain.registration.RegistrationDomain;
import com.atpl.demo.domain.studentLogin.StudentLoginDomain;
import com.atpl.demo.exception.MmgRestException.BACKEND_SERVER_ERROR;
import com.atpl.demo.exception.MmgRestException.DELETE_FAILED;
import com.atpl.demo.exception.MmgRestException.NOT_FOUND;
import com.atpl.demo.exception.MmgRestException.SAVE_FAILED;
import com.atpl.demo.exception.MmgRestException.UPDATE_FAILED;
import com.atpl.demo.utils.DateUtility;


@Repository
@SuppressWarnings("rawtypes")
public class StudentLoginDaoImpl implements StudentLoginDao, Constants{

	private static final Logger logger = LoggerFactory.getLogger(StudentLoginDaoImpl.class);

	@Autowired
	JdbcTemplate jdbcTemplate;

	@Override
	public String saveStudentLogin(StudentLoginDomain studentLoginDomain) throws Exception {
		// TODO Auto-generated method stub
		
		try {
			SimpleDateFormat simpleDateFormat = new SimpleDateFormat(DateUtility.DATE_FORMAT_YYYY_MM_DD_HHMMSS);
			simpleDateFormat.setTimeZone(TimeZone.getTimeZone("GMT+5:30"));
			String sql = "INSERT INTO studentlogin(uuId,studentUuId,userName,passWord, createdDate, modifiedDate) VALUES(?,?,?,?,?,?)";
			int res = jdbcTemplate.update(sql, new Object[] {UUID.randomUUID().toString(),studentLoginDomain.getStudentUuId(),studentLoginDomain.getUserName(),studentLoginDomain.getPassWord(),simpleDateFormat.format(new Date()), simpleDateFormat.format(new Date()) });
			if (res == 1) {
				return "Saved successfully";
			} else
				throw new SAVE_FAILED("StudentLogin Save Failed");
		} catch (Exception e) {
			logger.error("Exception saveStudentLogin in StudentLoginDAOImpl", e);
			throw new BACKEND_SERVER_ERROR();
		}
	}

	@Override
	public String login(String userName) throws Exception {
		// TODO Auto-generated method stub
		try {
			String sql = "SELECT passWord FROM studentlogin where userName=?";
			String password = jdbcTemplate.queryForObject(sql, new Object[] {userName},
					String.class);
			return password;
		} catch (EmptyResultDataAccessException e) {
			logger.error("EmptyResultDataAccessException getLogin in LoginDao" + e.getMessage());
			throw new NOT_FOUND("username not found");
		} catch (Exception e) {
			logger.error("Exception in StudentLoginDAOImpl", e);
			throw new BACKEND_SERVER_ERROR();
		}
	
	}

	@Override
	public String updateStudentLogin(StudentLoginDomain studentLoginDomain) throws Exception {
		// TODO Auto-generated method stub
      try {
    	    SimpleDateFormat simpleDateFormat = new SimpleDateFormat(DateUtility.DATE_FORMAT_YYYY_MM_DD_HHMMSS);
			simpleDateFormat.setTimeZone(TimeZone.getTimeZone("GMT+5:30"));
			String sql = "UPDATE studentlogin SET userName=?,passWord=?,modifiedDate=? WHERE uuId=?";
			int res = jdbcTemplate.update(sql, new Object[] { studentLoginDomain.getUserName(),studentLoginDomain.getPassWord(),studentLoginDomain.getUuId(), simpleDateFormat.format(new Date())});
			if (res == 1) {
				return "Updated Successfully";
			} else
				throw new UPDATE_FAILED("StudentLogin Update Failed");
		} catch (Exception e) {
			logger.error("Exception updateStudentLogin in StudentLoginDAOImpl", e);
			throw new BACKEND_SERVER_ERROR();
		}

	}

	
	
}
