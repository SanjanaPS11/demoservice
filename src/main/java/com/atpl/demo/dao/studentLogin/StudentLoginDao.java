package com.atpl.demo.dao.studentLogin;

import com.atpl.demo.domain.studentLogin.StudentLoginDomain;
import com.atpl.demo.model.studentLogin.StudentLoginModel;

public interface StudentLoginDao {

	public String saveStudentLogin(StudentLoginDomain studentLoginDomain)throws Exception;
	
	public String login(String userName) throws Exception;
	
	public String updateStudentLogin(StudentLoginDomain studentLoginDomain)throws Exception;
	

}
