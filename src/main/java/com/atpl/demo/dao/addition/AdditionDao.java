package com.atpl.demo.dao.addition;


import java.util.List;
import com.atpl.demo.domain.addition.AdditionDomain;

public interface AdditionDao {

	public String saveAddition(AdditionDomain additionDomain) throws Exception;
	
	public AdditionDomain getAddition(int id) throws Exception;
	

}
