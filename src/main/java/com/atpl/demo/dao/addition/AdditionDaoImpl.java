package com.atpl.demo.dao.addition;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import com.atpl.demo.constant.Constants;
import com.atpl.demo.dao.addition.AdditionDao;
import com.atpl.demo.dao.addition.AdditionDaoImpl;
import com.atpl.demo.domain.addition.AdditionDomain;
import com.atpl.demo.exception.MmgRestException.BACKEND_SERVER_ERROR;
import com.atpl.demo.exception.MmgRestException.DELETE_FAILED;
import com.atpl.demo.exception.MmgRestException.NOT_FOUND;
import com.atpl.demo.exception.MmgRestException.SAVE_FAILED;
import com.atpl.demo.exception.MmgRestException.UPDATE_FAILED;
import com.atpl.demo.utils.DateUtility;

@Repository
@SuppressWarnings("rawtypes")
public class AdditionDaoImpl implements AdditionDao, Constants{

	private static final Logger logger = LoggerFactory.getLogger(AdditionDaoImpl.class);

	@Autowired
	JdbcTemplate jdbcTemplate;
	
	public String saveAddition(AdditionDomain additionDomain) {
		try {
			
			String sql = "INSERT INTO addition(id,add1,add2) VALUES(?,?,?)";
			int res = jdbcTemplate.update(sql, new Object[] {additionDomain.getId(),additionDomain.getAdd1(),additionDomain.getAdd2()});
			String s1="SELECT add1 FROM addition where id=?";
	
			String s2="SELECT add2 FROM addition where id=?";
	
			int a1=Integer.parseInt(s1); 
			int a2=Integer.parseInt(s2);
			int sum=a1+a2;
			
			String s = "INSERT INTO addition(sum) VALUES(sum) where id=?";
			jdbcTemplate.update(s);
			
			if (res == 1) {
				return "Saved successfully";
			} else
				throw new SAVE_FAILED("add Save Failed");
		} catch (Exception e) {
			logger.error("Exception save add in addDAOImpl", e);
			throw new BACKEND_SERVER_ERROR();
		}
	}
	
	@Override
	public AdditionDomain getAddition(int id) throws Exception {
		try {
			String sql = "SELECT sum FROM addition where id=?";
			return (AdditionDomain) jdbcTemplate.queryForObject(sql, new Object[] { id },
					new BeanPropertyRowMapper<AdditionDomain>(AdditionDomain.class));
			
		} catch (EmptyResultDataAccessException e) {
			throw new NOT_FOUND("Addition not found");
		} catch (Exception e) {
			logger.error("Exception getAdditionDomainbyid in AdditionDomainDAOImpl", e);
			throw new BACKEND_SERVER_ERROR();
		}
	}
	
	
}
