package com.atpl.demo.dao.calculate;

import java.util.List;
import com.atpl.demo.exception.GenericRes;
import com.atpl.demo.domain.calculate.CalculateDomain;
public interface CalculateDao {

	public String saveList(CalculateDomain calculateDomain) throws Exception;
	
	public String getList() throws Exception;
	
}
