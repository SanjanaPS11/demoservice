package com.atpl.demo.dao.calculate;

import java.util.List;

import java.util.*;
import org.bouncycastle.util.Arrays;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import com.atpl.demo.constant.Constants;
import com.atpl.demo.domain.calculate.CalculateDomain;
import com.atpl.demo.exception.MmgRestException.BACKEND_SERVER_ERROR;
import com.atpl.demo.exception.MmgRestException.NOT_FOUND;
import com.atpl.demo.exception.MmgRestException.SAVE_FAILED;
import com.fasterxml.jackson.databind.ObjectMapper;
@Repository
@SuppressWarnings("rawtypes")
public class CalculateDaoImpl  implements CalculateDao, Constants{

		private static final Logger logger = LoggerFactory.getLogger(CalculateDaoImpl.class);
		
		@Autowired
		JdbcTemplate jdbcTemplate;

		public String saveList(CalculateDomain calculateDomain){
			ObjectMapper obj = new ObjectMapper();
			try {
				//obj.writerWithDefaultPrettyPrinter().writeValueAsString(new Object[] {calculateDomain.getAddition1()});
				String sql = " INSERT INTO arraylist(addition1,addition2) VALUES (?,?)";
				int res = jdbcTemplate.update(sql,obj.writerWithDefaultPrettyPrinter().writeValueAsString(new Object[] {calculateDomain.getAddition1()}),obj.writerWithDefaultPrettyPrinter().writeValueAsString(new Object[] {calculateDomain.getAddition2()}));
				if (res == 1) {
					return "List successfully";
				} else
					throw new SAVE_FAILED("List Save Failed");
			} catch (Exception e) {
				logger.error("Exception saveList in calculateDAOImpl", e);
				throw new BACKEND_SERVER_ERROR();
			}
			
		}
		
		public String getList() throws Exception
		{
//			String qur="Select * from arraylist";
//			List<CalculateDomain> list=new ArrayList<CalculateDomain>();
//			CalculateDomain calculate=null;
//			List<CalculateDomain> sum = jdbcTemplate.query(sql, new Object[] {},
//					new BeanPropertyRowMapper<CalculateDomain>(CalculateDomain.class));
			
//			while(sum.next())
//			{
//				calculate=new CalculateDomain();
//				calculate.setAddition1(calculate.getArrayList("Addition1"));
//			
				try {
					String sql = "SELECT addition1 FROM arraylist";
					List<CalculateDomain> sum = jdbcTemplate.query(sql, new Object[] {},
							new BeanPropertyRowMapper<CalculateDomain>(CalculateDomain.class));
//					CalculateDomain cd=new CalculateDomain();
//					List<CalculateDomain> list=new ArrayList<CalculateDomain>(sum);
//					List<String> al = new List<String>();
//					al = Arrays.asList(sum);
//					for(String s: al){
//					   System.out.println(s);
//					}
//					System.out.print(list);
					//List<String> list = Arrays.asList(sum); 
					  
		            // printing the list 
		            System.out.println("The list is: " + sum); 
			
					return "List";
				} catch (EmptyResultDataAccessException e) {
					logger.error("EmptyResultDataAccessException getList in CalculateDAOImpl" + e.getMessage());
					throw new NOT_FOUND("calculate not found");
				} catch (Exception e) {
					logger.error("Exception getList in CalculateDAOImpl", e);
					throw new BACKEND_SERVER_ERROR();
				}

		}
}
