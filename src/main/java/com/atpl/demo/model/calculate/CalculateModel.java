package com.atpl.demo.model.calculate;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import com.atpl.demo.domain.calculate.CalculateDomain;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

@JsonInclude(Include.NON_DEFAULT)

public class CalculateModel implements Serializable{
	
	private static final long serialVersionUID = -2196266153406082047L;
	
	
	private int[] addition1;
	private int[] addition2;
	
	public int[] getAddition1() {
		return addition1;
	}
	public void setAddition1(int[] addition1) {
		this.addition1 = addition1;
	}
	public int[] getAddition2() {
		return addition2;
	}
	public void setAddition2(int[] addition2) {
		this.addition2 = addition2;
	}
	
	
	
}
