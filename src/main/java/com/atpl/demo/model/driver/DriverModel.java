package com.atpl.demo.model.driver;
import java.io.Serializable;
import javax.validation.constraints.*;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.NotBlank;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

@JsonInclude(Include.NON_DEFAULT)
public class DriverModel implements Serializable{

	private static final long serialVersionUID = -2196266153406082047L;
	
	private String firstName;
	private String roleName;
	private String requestNumber;
	private String customer;
	private String uuId;
	private String name;
	private String phoneNo;
	private String email;
	private Integer roleId;
	private String messageName;
	private Boolean isActive;
	private String createdDate;
	private String modifiedDate;
	
	
	
	public String getCustomer() {
		return customer;
	}
	public void setCustomer(String customer) {
		this.customer = customer;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getRoleName() {
		return roleName;
	}
	public void setRoleName(String roleName) {
		this.roleName = roleName;
	}
	public String getRequestNumber() {
		return requestNumber;
	}
	public void setRequestNumber(String requestNumber) {
		this.requestNumber = requestNumber;
	}
	public String getMessageName() {
		return messageName;
	}
	public void setMessageName(String messageName) {
		this.messageName = messageName;
	}
	public Integer getRoleId() {
		return roleId;
	}
	public void setRoleId(Integer roleId) {
		this.roleId = roleId;
	}
	public String getUuId() {
		return uuId;
	}
	public void setUuId(String uuId) {
		this.uuId = uuId;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getPhoneNo() {
		return phoneNo;
	}
	public void setPhoneNo(String phoneNo) {
		this.phoneNo = phoneNo;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public Boolean getIsActive() {
		return isActive;
	}
	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}
	public String getCreatedDate() {
		return createdDate;
	}
	public void setCreatedDate(String createdDate) {
		this.createdDate = createdDate;
	}
	public String getModifiedDate() {
		return modifiedDate;
	}
	public void setModifiedDate(String modifiedDate) {
		this.modifiedDate = modifiedDate;
	}
	
	
//	private String driverId;
//	private String driverName;
//	private Integer stateId;
//	private Integer cityId;
//	private String requestedBy;
//	private String status;
//	private Integer total;
//	public String getDriverId() {
//		return driverId;
//	}
//	public void setDriverId(String driverId) {
//		this.driverId = driverId;
//	}
//	public String getDriverName() {
//		return driverName;
//	}
//	public void setDriverName(String driverName) {
//		this.driverName = driverName;
//	}
//	public Integer getStateId() {
//		return stateId;
//	}
//	public void setStateId(Integer stateId) {
//		this.stateId = stateId;
//	}
//	public Integer getCityId() {
//		return cityId;
//	}
//	public void setCityId(Integer cityId) {
//		this.cityId = cityId;
//	}
//	public String getRequestedBy() {
//		return requestedBy;
//	}
//	public void setRequestedBy(String requestedBy) {
//		this.requestedBy = requestedBy;
//	}
//	public String getStatus() {
//		return status;
//	}
//	public void setStatus(String status) {
//		this.status = status;
//	}
//	public Integer getTotal() {
//		return total;
//	}
//	public void setTotal(Integer total) {
//		this.total = total;
//	}
	


	
}
