package com.atpl.demo.model.boarding;
import java.io.Serializable;
import javax.validation.constraints.*;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.NotBlank;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

@JsonInclude(Include.NON_DEFAULT)
public class BoardingModel implements Serializable{

private static final long serialVersionUID = -2196266153406082047L;
	
	private String uuId;
	private Integer roleId;
	private Integer stateId;
	private Integer cityId;
	private Integer countryId;
	private String status;
	private Integer franchise;
	private Integer fleet;
	private Integer warehouse;
	private Integer enterprise;
	
	public String getUuId() {
		return uuId;
	}
	public void setUuId(String uuId) {
		this.uuId = uuId;
	}
	public Integer getRoleId() {
		return roleId;
	}
	public void setRoleId(Integer roleId) {
		this.roleId = roleId;
	}
	public Integer getStateId() {
		return stateId;
	}
	public void setStateId(Integer stateId) {
		this.stateId = stateId;
	}
	public Integer getCityId() {
		return cityId;
	}
	public void setCityId(Integer cityId) {
		this.cityId = cityId;
	}
	public Integer getCountryId() {
		return countryId;
	}
	public void setCountryId(Integer countryId) {
		this.countryId = countryId;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public Integer getFranchise() {
		return franchise;
	}
	public void setFranchise(Integer franchise) {
		this.franchise = franchise;
	}
	public Integer getFleet() {
		return fleet;
	}
	public void setFleet(Integer fleet) {
		this.fleet = fleet;
	}
	public Integer getWarehouse() {
		return warehouse;
	}
	public void setWarehouse(Integer warehouse) {
		this.warehouse = warehouse;
	}
	public Integer getEnterprise() {
		return enterprise;
	}
	public void setEnterprise(Integer enterprise) {
		this.enterprise = enterprise;
	}
	
	
}
