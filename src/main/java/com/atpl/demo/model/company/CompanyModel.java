package com.atpl.demo.model.company;
import java.io.Serializable;
import javax.validation.constraints.*;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.NotBlank;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
@JsonInclude(Include.NON_DEFAULT)
@Entity
@Table(name = "company")
public class CompanyModel implements Serializable{

	private static final long serialVersionUID = -2196266153406082047L;
	

	
	@Id
	@Column(name = "UuId")
	private String uuId;
	
	@Column(name = "companyName")
	private String companyName;
	
	@Column(name = "companyType")
	private String companyType;
	
	@Column(name = "established")
	private Integer established;

	@Column(name = "mdName")
	private String mdName;
	
	@Column(name = "mobileNumber")
	private String mobileNumber;
	
	@Column(name = "emailId")
	private String emailId;
	
	@Column(name = "panNumber")
	private String panNumber;
	
	@Column(name = "gstNumber")
	private String gstNumber;
	
	@Column(name = "webSite")
	private String webSite;
	
	@Column(name = "isActive")
	private Boolean isActive;
	
	@Column(name = "CreatedDate")
	private String createdDate;
	
	@Column(name = "modifiedDate")
	private String modifiedDate;

	public String getCompanyName() {
		return companyName;
	}
	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}
	public String getCompanyType() {
		return companyType;
	}
	public void setCompanyType(String companyType) {
		this.companyType = companyType;
	}
	public String getUuId() {
		return uuId;
	}
	public void setUuId(String uuId) {
		this.uuId = uuId;
	}


	public Integer getEstablished() {
		return established;
	}
	public void setEstablished(Integer established) {
		this.established = established;
	}
	public String getMdName() {
		return mdName;
	}
	public void setMdName(String mdName) {
		this.mdName = mdName;
	}
	public String getMobileNumber() {
		return mobileNumber;
	}
	public void setMobileNumber(String mobileNumber) {
		this.mobileNumber = mobileNumber;
	}
	public String getEmailId() {
		return emailId;
	}
	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}
	public String getPanNumber() {
		return panNumber;
	}
	public void setPanNumber(String panNumber) {
		this.panNumber = panNumber;
	}
	public String getGstNumber() {
		return gstNumber;
	}
	public void setGstNumber(String gstNumber) {
		this.gstNumber = gstNumber;
	}
	public String getWebSite() {
		return webSite;
	}
	public void setWebSite(String webSite) {
		this.webSite = webSite;
	}
	
	public Boolean getIsActive() {
		return isActive;
	}
	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}
	public String getCreatedDate() {
		return createdDate;
	}
	public void setCreatedDate(String createdDate) {
		this.createdDate = createdDate;
	}
	public String getModifiedDate() {
		return modifiedDate;
	}
	
	public void setModifiedDate(String modifiedDate) {
		this.modifiedDate = modifiedDate;
	}
public void CompanyModelExcel(String uuid,String companyname, String companytype,Integer established,String mdname,String mobilenumber,String emailid,String pannumber,String gstnumber,String website,Boolean isactive,String createddate,String modifieddate) {
	this.emailId = emailid;
	this.mdName = mdname;
	this.companyName = companyname;
	this.companyType = companytype;
	this.uuId = uuid;
	this.mobileNumber = mobilenumber;
	this.established = established;
	this.gstNumber = gstnumber;
	this.isActive = isactive;
	this.panNumber = pannumber;
	this.createdDate = createddate;
	this.modifiedDate = modifieddate;
	
	}
}
